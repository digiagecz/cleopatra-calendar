<?php
// CLEO home page template
// Author: Martin Krotil

require_once('./core/settings.php');
require_once('./core/cleo_users.php');

header('Content-Type: text/html; charset=utf-8');

$cUsers = new cleoUsers;

// Login box content
$loginBoxContent = '';
if ($cUsers->isUserLoggedIn()) {
  if( $picture = $cUsers->getUserInformation('picture') ) {
    $loginBoxContent = '<div class="cleo-user"><img src="' . $picture . '" class="cleo-user__img" alt="Profilový obrázek" /><span class="cleo-user__text">' . $cUsers->getUserInformation('name') . '</span></div>';
  }
  else {
    $loginBoxContent = '<div class="cleo-user"><span class="cleo-user__text">' . $cUsers->getUserInformation('name') . '</span></div>';
  }
}
else {
  $loginBoxContent = '<a href="' . GOOGLE_LOGIN . '" class="cleo-google js--notice"><span class="cleo-google__img"></span><span class="cleo-google__text">Sign in with Google</span></a>';
}

  getMainHeader("Cleopatra",0,$cUsers->isServiceLoggedIn('google'));

  require_once('./core/tmp/navigation.php');
?>

    <header class="masthead d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-12 mx-auto">
            <div class="cleo-header__text">
              <h2 class="font-weight-bold font-italic">Elektronický kalendář pro moderní ženu,<br />která chce žít v souladu se svými cykly.</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mx-auto">
            <div class="cleo-header__text is--logo">
              <svg
                 version="1.1"
                 viewBox="0 0 200 200"
                 height="160"
                 width="160">

                <g
                   transform="translate(0,-852.36216)"
                   id="layer1">
                  <ellipse
                     ry="59.239624"
                     rx="57.969704"
                     style="opacity:1;fill:#a600de;fill-opacity:1;stroke:none;stroke-width:10;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                     id="circle3349"
                     cx="57.970173"
                     cy="911.60181" />
                  <ellipse
                     ry="59.239624"
                     rx="57.969704"
                     style="opacity:1;fill:#fe0000;fill-opacity:1;stroke:none;stroke-width:10;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                     id="circle3351"
                     cx="142.02986"
                     cy="993.12256" />
                  <path
                     id="circle3353"
                     d="M 57.969706,933.8829 A 57.969704,59.239624 0 0 0 0,993.12252 57.969704,59.239624 0 0 0 57.969706,1052.3621 57.969704,59.239624 0 0 0 99.973954,1033.8463 57.969704,59.239624 0 0 1 84.060588,993.12252 57.969704,59.239624 0 0 1 100.02605,952.39875 57.969704,59.239624 0 0 0 57.969706,933.8829 Z"
                     style="opacity:1;fill:#007fff;fill-opacity:1;stroke:none;stroke-width:10;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                  <path
                     id="path3347"
                     d="m 142.03029,852.36247 a 57.969704,59.239624 0 0 0 -42.00311,18.51469 57.969704,59.239624 0 0 1 15.91223,40.72493 57.969704,59.239624 0 0 1 -15.966594,40.72377 57.969704,59.239624 0 0 0 42.057474,18.51586 A 57.969704,59.239624 0 0 0 200,911.60209 57.969704,59.239624 0 0 0 142.03029,852.36247 Z"
                     style="opacity:1;fill:#ffff00;fill-opacity:1;stroke:none;stroke-width:10;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                </g>
              </svg>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mx-auto">
            <div class="cleo-header__text is--with-ul">
              <ul class="cleo-util is--first-ul-style">
                <li>Cítíš se během měsíce proměnlivě? Vnímáš jakými fázemi procházíš?</li>
                <li>Díky kalendáři Cleopatra budeš mít o svých fázích přehled a poznáš správný čas pro své plány. Buď ve své práci efektivní a čerpej maximálně ze svého potenciálu!</li>
                <li>Využij individuální zobrazení jednotlivých fází na půl roku dopředu přímo ve svém kalendáři, který již používáš.</li>
                <li>Dopřej si měsíc zdarma na vyzkoušení.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Lze poznat správný čas pro moje plány?</h2>
            <hr class="dark my-4">
            <p class="mb-4">Víš, kdy je Tvůj nejlepší čas na důležitou schůzku? Kdy začít nový projekt? Kdy naplánovat oslavu a dobře se na ní bavit nebo naopak zůstat doma, pokud to jen trochu jde? Určitě ano. Už sis mnohokrát všimla, že jsou dny, kdy se cítíš skvěle a vše Ti jde od ruky a pak dny, kdy je lepší o věcech jen přemýšlet? Že jsou dny, kdy zvládneš opravdu velkou zátěž, a pak dny, kdy Tě rozhodí každá maličkost? Jistě.</p>
            <p class="mb-4"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice">Vyzkoušej měsíc ZDARMA</a></p>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 text-center">
            <h2 class="section-heading">Náhled kalendáře</h2>
            <?php if (!$cUsers->isUserLoggedIn()) { echo '<a href="' . GOOGLE_LOGIN . '" class="js--notice" title="Příhlásit se ke svému Google účtu">'; }?>
            <img src="./img/calendar.jpg" alt="Náhled Google kalendáře" class="cleo-thumbnail" />
            <?php if (!$cUsers->isUserLoggedIn()) { echo '</a>'; } ?>
          </div>
          <div class="col-lg-6 col-md-6 text-center">
            <h2 class="section-heading">Proměnlivá žena</h2>
            <iframe src="https://www.youtube.com/embed/w7wurtInopQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="cleo-thumbnail is--video"></iframe>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-primary">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Cyklus ovlivňuje nás <br />a&nbsp;my můžeme ovlivnit naše plány</h2>
            <hr>
            <p class="mb-4">Souvisí to s naším menstruačním a ovulačním cyklem, s tím jak nám stoupá a klesá hladina hormonů v průběhu měsíce. Máme přitom jedinečnou možnost využít jednotlivé fáze našeho cyklu tím nejlepším způsobem, který je nám přirozený. Každý měsíční cyklus má čtyři odlišné fáze. Ty nemusí být stejně dlouhé, ani pevně ohraničené, ale za to jsou jedinečné a nepostradatelné! Každá z nich má svůj význam, a také svůj potenciál. Můžeme je využít v osobním i v pracovním životě a pro plnění našich nápadů a projektů. Je ale těžké plánovat dopředu, najít nejvhodnější čas s předstihem a využít jej tak ve svůj prospěch. Zajímavé informace a podrobnosti k cyklům najdeš v knize Cyklická žena od Mirandy Gray.</p>
            <p class="mb-4"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice">Vyzkoušej měsíc ZDARMA</a></p>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading mb-5">Stručný popis jednotlivých fází cyklu:</h2>

            <div class="container">
              <div class="row">
                <div class="col-lg-3 mx-auto text-center">
                  <div class="portfolio-box">
                    <img class="img-fluid" src="img/cycles/cycle1.jpg" alt="" />
                    <div class="portfolio-box__circle is--yellow"></div>
                  </div>
                  <h4 class="mt-4 mb-3 font-weight-bold">Dynamická fáze</h4>
                  <p>Období po menstruaci a před ovulací, kdy je žena aktivní, extrovertní a energická. Je to čas na uskutečnění osobních změn, ke kterým v předchozích fázích došla. Myšlení má logické, objektivní a dynamické. Je to správný čas pro nové projekty, stanovení priorit a plánování postupných kroků k dosažení cíle. Čas vhodný k učení, zkoumání i k fyzické vytrvalosti.</p>
                </div>
                <div class="col-lg-3 mx-auto text-center">
                  <div class="portfolio-box">
                    <img class="img-fluid" src="img/cycles/cycle2.jpg" alt="" />
                    <div class="portfolio-box__circle is--red"></div>
                  </div>
                  <h4 class="mt-4 mb-3 font-weight-bold">Expresivní fáze</h4>
                  <p>Období kolem ovulace, kdy vajíčko dozrává a žena je připravena k oplodnění. Je velmi společenská a do popředí jejího zájmu se dostávají vztahy, emoce a city. Je komunikativní a empatická, což jí pomáhá pracovat v týmu a ostatní podporovat v jejich práci. Je to fáze, kdy působí nejvíce “žensky” a připadá si v této roli opravdu dobře a sebejistě.</p>
                </div>
                <div class="col-lg-3 mx-auto text-center">
                  <div class="portfolio-box">
                    <img class="img-fluid" src="img/cycles/cycle4.jpg" alt="" />
                    <div class="portfolio-box__circle is--blue"></div>
                  </div>
                  <h4 class="mt-4 mb-3 font-weight-bold">Kreativní fáze</h4>
                  <p>Období před menstruací, kdy hladina hormonů klesá a více se dostává do popředí intuice, nápady, odhalení problémů. Je to období, kdy je žena tvůrčí při řešení problémů a vyjasňování svého směru. Je to období nutkavé vnitřní aktivity, projevuje se emocemi a vášní i mentální aktivitou. Žena je v tuto dobu kritická ke všemu, co nefunguje, má výraznou představivost, intuici a vhled.</p>
                </div>
                <div class="col-lg-3 mx-auto text-center">
                  <div class="portfolio-box">
                    <img class="img-fluid" src="img/cycles/cycle3.jpg" alt="" />
                    <div class="portfolio-box__circle is--purple"></div>
                  </div>
                  <h4 class="mt-4 mb-3 font-weight-bold">Reflexivní fáze</h4>
                  <p>Období menstruace, kdy je žena introvertní a obrací se více ke svému vnitřnímu já i svému tělu. Má potřebu vzdálit se a zpomalit. Zpracovává v sobě události, které prožila a emoce které vypluly na povrch. Je to období pasivní, kdy má žena intenzivní potřebu odpočinku a potřebuje věnovat čas vlastní regeneraci. Každá běžná činnost ji stojí velké úsilí, ale jednoduše nalézá důležitou podstatu věcí a událostí.</p>
                </div>
              </div>
            </div>

            <p class="mb-4"><a href="<?php echo INFO_PAGE;?>" class="btn btn-info">Často kladené dotazy</a></p>

          </div>
        </div>
      </div>
    </section>

    <section class="bg-primary">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Vše v&nbsp;jednom kalendáři</h2>
            <hr class="dark my-4">
            <p class="mb-4">Už nemusíš používat žádné speciální aplikace ani speciální přihlašování, abys věděla, kdy je nejvhodnější doba k plánování těhotenství či schůzky. Osobní, rodinný a pracovní život se jen těžko dají oddělit a proto je důležité mít možnost podívat se jen do svého Google kalendáře a mít přehled o všech důležitých věcech, které potřebuješ pro své plánování. Diskrétně a přirozeně kdekoli a kdykoli.</p>
            <p class="mb-4"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice">Vyzkoušej měsíc ZDARMA</a></p>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Jak začít? Vyzkoušej Cleopatru</h2>
            <hr />

            <ul style="list-style-position: inside;">
              <li>Stačí přihlásit se přes svůj vlastní Google účet a tímto účtem mít chráněna i osobní data.</li>
              <li>Zadat minimálně 2 termíny svých posledních menstruací pro výpočet svých osobních cyklů a jejich fází.</li>
              <li>Ihned se Ti zobrazí osobní Cleopatra kalendář. Po aktualizaci se Ti zobrazí na všech zařízeních, kde se svým Google kalendářem pracuješ.</li>
              <li>Je potřeba se vracet a přidat aktuální termín menstruace, jinak by Ti kalendář zobrazoval zastaralé údaje a neodpovídající Tvým cyklům.</li>
              <li>Nepřihlásíš-li se, tak po 30ti dnech od poslední menstruace, tak Ti přijde e-mail s připomenutím potřebné aktualizace kalendáře.</li>
              <li>Pravidelná aktualizace Ti zabere maximálně minutku :).</li>
              <li><strong>Kalendář Cleopatra máš k dispozici 30 dní zdarma. Po té Ti přijde emailem nabídka, zda-li máš zájem využít roční předplatné za 250,-Kč a možnosti on-line úhrady. </strong></li>
              <li>Pokud nemáš zájem a neuhradíš předplatné, Tvůj účet bude zrušen a nemusíš se o něj více starat.</li>
              <li>Pokud nemáš Google účet, tak si jej můžeš založit <a href="https://accounts.google.com/SignUp?hl=cs" target="_blank" title="Založení Google účtu">ZDE</a>. </li>
              <li>Na stránce Informace najdeš řadu užitečných informací a častých dotazů a <a href="<?php echo TERMS_PAGE;?>" title="Všeobecné obchodní podmínky">ZDE</a> najdeš Všeobecné obchodní podmínky a Zásady ochrany osobních údajů online kalendáře Cleopatra.</li>
            </ul>

            <p class="mb-4"><a href="<?php echo INFO_PAGE;?>" class="btn btn-info">Často kladené dotazy</a></p>

          </div>
        </div>
      </div>
    </section>

    <section class="bg-primary">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Nejlepší čas je TEĎ!</h2>
            <hr>
            <p class="mb-4">Stále se musíme přizpůsobovat řadě věcí - plánům ostatních, rozvrhům, pracovní době, vhodným termínům a tak dále. Už teď ale můžeme vědět, jak se například připravit na schůzku i když není v tom nejlepším termínu. Získáváme tak možnost věci ovlivnit ve svůj prospěch a užít si každý okamžik na plno a zbytečně se nestresovat. Protože právě teď je ten nejlepší čas! Jen vědět na co. Právě teď je nejlepší čas vyzkoušet Cleopatru a plánovat si svůj život podle sebe!</p>
            <p class="mb-4"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice">Vyzkoušej měsíc ZDARMA</a></p>
          </div>
        </div>
      </div>
    </section>

<?php
  require_once('./core/tmp/footer.php');

  if (isset($_GET['after'])) {
    require_once('./core/cleo_dialog_modal.php');
  }

  getMainFooter(0,0);
?>
