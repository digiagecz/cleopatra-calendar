(function () {
  var cleoAgreements = {
    showModal: function() {
      $('#agreement-modal').modal('show');
    }
  }

  if (document.readyState!='loading') cleoAgreements.showModal();
  else if (document.addEventListener) document.addEventListener('DOMContentLoaded', cleoAgreements.showModal);
  else document.attachEvent('onreadystatechange', function() {
    if (document.readyState=='complete') cleoAgreements.showModal();
  });

})();
