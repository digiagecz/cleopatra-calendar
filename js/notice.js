var notice = {
  lang: 'cs',
  url: '',

  setCookie: function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'; // + ';path=/'
  },

  getCookie: function (cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  },

  changeHrefs: function () {
    var anchors = document.getElementsByClassName('js--notice');
    var char = '';

    if (notice.url.indexOf('?') === -1) {
      char = '?';
    } else {
      char = '&';
    }

    if ((notice.url.indexOf('?after=notice') === -1) && (notice.url.indexOf('&after=notice') === -1)) {
      for (var i = 0; i<anchors.length; i++) {
        anchors[i].href = notice.url + char + 'after=notice';
      }
    } else {
      for (var i = 0; i<anchors.length; i++) {
        anchors[i].href = notice.url;
      }
    }
  },

  init: function () {

    if (notice.getCookie('cleopatraVisit') === '') {
      notice.url = window.location.href;

      if ((notice.url.indexOf('?after=notice') > 0) || (notice.url.indexOf('&after=notice') > 0)) {
        notice.setCookie('cleopatraVisit',true,365);
      }

    notice.changeHrefs();
    }
  }
}




if (document.readyState!='loading') notice.init();
else if (document.addEventListener) document.addEventListener('DOMContentLoaded', notice.init);
else document.attachEvent('onreadystatechange', function() {
  if (document.readyState=='complete') notice.init();
});
