var env = {
  lang: 'cs',
  logOutTimeout: null,

  hasClass: function (el, className) {
    return el.classList ? el.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(el.className);
  },

  addClass: function (el, className) {
    if (el.classList) el.classList.add(className);
    else if (!env.hasClass(el, className)) el.className += ' ' + className;
  },

  removeClass: function (el, className) {
    if (el.classList) el.classList.remove(className);
    else el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
  },

  getText: function (code) {
    switch (env.lang) {
      case 'cs':
        switch (code) {
          case 'err_login': return 'Přístup zamítnut. Prosím, přihlašte se.'; break;
          case 'err_google_crt_clndr': return 'Nepodařilo se vygenerovat nový Google kalendář.'; break;
          case 'google_calendar_reset': return 'Byl vám vygenerován nový Google kalendář.'; break;
          default: return 'Chyba: hledaný obsah ve slovníku nenalezen.';
        }
      break;
    }
  },

  infoBoxes: {
    showing: false,
    infoBox: null,
    infoContent: null,
    timeout: null,
    startTimeout: null,
    opacityTimeout: null,
    validInit: false,
    lastType: null,

    hide: function() {
      env.addClass(env.infoBoxes.infoBox,'is--closing');
      opacityTimeout = setTimeout(function() {
        env.infoBoxes.infoContent.innerHTML = '';
        env.removeClass(env.infoBoxes.infoBox,'is--open');
        env.removeClass(env.infoBoxes.infoBox,'is--closing');
        env.removeClass(env.infoBoxes.infoBox,'is--' + env.infoBoxes.lastType);
        env.infoBoxes.showing = false;
      }, 1000);
    },

    show: function(content, type, speed) {
      type = type || 'info';
      speed = speed || 'slow';

      if (!env.infoBoxes.showing) {
        env.infoBoxes.lastType = type;

        if (env.loadingPhase.validInit) {
          env.loadingPhase.hide();
        }

        if (content && (content != '')) {

          env.infoBoxes.startTimeout = setTimeout(function() {
            env.infoBoxes.infoContent.innerHTML = content;
            env.addClass(env.infoBoxes.infoBox,'is--open');
            env.addClass(env.infoBoxes.infoBox,'is--' + type);

            var miliseconds = 4000;

            if (speed == 'quick') {
              var miliseconds = 2000;
            }

            env.infoBoxes.timeout = setTimeout(function() {
              env.infoBoxes.hide();
            }, miliseconds);
          }, 500);
        }

        env.infoBoxes.showing = true;
      }
    },

    init: function() {
      if (env.infoBoxes.validInit !== true) {
        env.infoBoxes.infoBox = document.getElementById('js-info-box');
        env.infoBoxes.infoContent = document.getElementById('js-info-content');
        env.infoBoxes.infoCloser = document.getElementById('js-info-closer');

        if ((env.infoBoxes.infoBox) && (env.infoBoxes.infoContent) && (env.infoBoxes.infoCloser)) {
          env.infoBoxes.infoCloser.addEventListener("click", function() {
            env.infoBoxes.hide();
          });
          env.infoBoxes.validInit = true;

          // if ?message
          var url = new URL(window.location.href);
          var message = url.searchParams.get('message');

          if (message) {
            // start with 'error'?
            if (message.substring(0,3) == 'err') {
              env.infoBoxes.show(env.getText(message), 'error');
            }
            else {
              env.infoBoxes.show(env.getText(message));
            }
          }
          return true;
        }
      }
      return false;
    }
  },

  loadingPhase: {
    validInit: false,
    isShowing: false,
    mainElement: null,
    textField: null,

    setText: function(text) {
      if (env.loadingPhase.validInit) {
        env.loadingPhase.textField.innerHTML = text;
      }
    },

    show: function() {
      if (env.loadingPhase.validInit) {
        env.loadingPhase.mainElement.style.display = 'block';
        env.loadingPhase.mainElement.className = 'cleo-loading is--showing';
      }
    },

    hide: function() {
      if (env.loadingPhase.validInit) {
        env.loadingPhase.mainElement.style.display = 'none';
        env.loadingPhase.mainElement.className = 'cleo-loading';
      }
    },

    init: function() {
      if (env.loadingPhase.validInit !== true) {
        env.loadingPhase.mainElement = document.getElementById('js-loading-phase');
        env.loadingPhase.textField = document.getElementById('js-loading-text');

        if ((env.loadingPhase.mainElement) && (env.loadingPhase.textField)) {
          env.loadingPhase.mainElement.style.display = 'none';

          // handlers
          var handlers = document.getElementsByClassName('js--show-loading');
          for (var i = 0; i<handlers.length; i++) {
            handlers[i].addEventListener('click', function() {
              var message = this.getAttribute('data-loading');
              if (message) {
                env.loadingPhase.setText(message);
              }
              env.loadingPhase.show();
            });
          }

          env.loadingPhase.validInit = true;
          return true;
        }
      }
      return false;
    }
  },

  idleTimer: {
    t: null,

    logout: function() {
      switch (window.location.hostname) {
        case 'localhost':
          window.location.href = 'http://' + window.location.hostname + '/cleopatra/app/passages/logout/';
          break;
        default:
          window.location.href = 'https://' + window.location.hostname + '/app/passages/logout/';
      }
    },

    reload: function() {
      window.location = self.location.href;
    },

    resetTimer: function() {
      clearTimeout(env.idleTimer.t);
      env.idleTimer.t = setTimeout(env.idleTimer.logout, 1800000);
      env.idleTimer.t = setTimeout(env.idleTimer.reload, 600000);
    },

    init: function() {
      window.onmousemove = env.idleTimer.resetTimer;
      window.onmousedown = env.idleTimer.resetTimer;
      window.onclick = env.idleTimer.resetTimer;
      window.onscroll = env.idleTimer.resetTimer;
      window.onkeypress = env.idleTimer.resetTimer;
      return true;
    }
  },

  init: function() {
    if (!env.loadingPhase.init()) { console.log('ERROR: loadingPhase initialization failed!'); }
    if (!env.infoBoxes.init()) { console.log('ERROR: infoBoxes initialization failed!'); }
    if (!env.idleTimer.init()) { console.log('ERROR: idleTimer initialization failed!'); }
  }
}

if (document.readyState!='loading') env.init();
else if (document.addEventListener) document.addEventListener('DOMContentLoaded', env.init);
else document.attachEvent('onreadystatechange', function() {
  if (document.readyState=='complete') env.init();
});
