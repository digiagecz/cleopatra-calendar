(function () {
  var appCore = {

    hasClass: function (el, className) {
      return el.classList ? el.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(el.className);
    },

    addClass: function (el, className) {
      if (el.classList) el.classList.add(className);
      else if (!appCore.hasClass(el, className)) el.className += ' ' + className;
    },

    removeClass: function (el, className) {
      if (el.classList) el.classList.remove(className);
      else el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
    },

    cleoStats: {
      url: null,
      validInit: false,

      getAjaxData: function(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onload = function() {
          var status = xhr.status;
          if (status == 200) {
            callback(null, xhr.responseText);
          } else {
            callback(status);
          }
        };

        xhr.send();
      },

      // VENDORS

      initVendorSlider: function (sliderID, fieldID, minValue, maxValue, disableSlider) {
        disableSlider = disableSlider || false;

        var handler = $(sliderID + '-handler'),
            field = $(fieldID),
            filedValue = field.val();

        $(sliderID).slider({
          create: function() {
            handler.text( $(this).slider('value'));
          },
          slide: function(event, ui) {
            var actualValue = ui.value;
            handler.text(actualValue);
            field.val(actualValue);
            filedValue = actualValue;
          },
          min: minValue,
          max: maxValue
        });


        $(sliderID).slider('option','value',filedValue);
        handler.text( filedValue );

        var isDisabled = $(sliderID).slider('option','disabled');
        if ((isDisabled) && (!disableSlider)) {
          $(sliderID).slider('option','disabled',false);
        }
        else if (disableSlider) {
          $(sliderID).slider('option','disabled',true);
        }
      },

      initPeriodSlider: function (disableSlider) {
        disableSlider = disableSlider || false;

        appCore.cleoStats.initVendorSlider('#js-manual-period-slider','#js-manual-period-field',3,8,disableSlider);
      },

      initCycleSlider: function (disableSlider) {
        disableSlider = disableSlider || false;

        appCore.cleoStats.initVendorSlider('#js-manual-cycle-slider','#js-manual-cycle-field',24,35,disableSlider);
      },

      // OWN PLAIN JS

      loadAndShowStats: function() {
        appCore.cleoStats.getAjaxData(appCore.cleoStats.url + '?action=get',  function(err, data) {
          if (err !== null) {
            console.error(err);
          }
          else {
            var dataObject = JSON.parse(data);
            appCore.cleoStats.period.innerHTML = dataObject[0]['length-period'];
            appCore.cleoStats.cycle.innerHTML = dataObject[0]['length-cycle'];

            appCore.cleoStats.cycleManual.value = dataObject[0]['length-cycle'];
            appCore.cleoStats.periodManual.value = dataObject[0]['length-period'];

            //if (dataObject[0]['pregnancy'] == 0) {
            //  appCore.cleoStats.pregnancyInfo.innerHTML = "Vypnuto";
            //}
            //else {
            //  appCore.cleoStats.pregnancyInfo.innerHTML = "Zapnuto";
            //}

            if (dataObject[0]['auto-cycle'] == 0) {
              appCore.cleoStats.cycleCheckbox.checked = false;
              appCore.cleoStats.cycleManual.disabled = false;
              appCore.cleoStats.initCycleSlider(false);
            }
            else {
              appCore.cleoStats.cycleCheckbox.checked = true;
              appCore.cleoStats.cycleManual.disabled = true;
              appCore.cleoStats.initCycleSlider(true);
            }
            if (dataObject[0]['auto-period'] == 0) {
              appCore.cleoStats.periodCheckbox.checked = false;
              appCore.cleoStats.periodManual.disabled = false;
              appCore.cleoStats.initPeriodSlider(false);
            }
            else {
              appCore.cleoStats.periodCheckbox.checked = true;
              appCore.cleoStats.periodManual.disabled = true;
              appCore.cleoStats.initPeriodSlider(true);
            }
            if (dataObject[0]['pregnancy'] == 0) {
              appCore.cleoStats.periodCheckbox.disabled = false;
              appCore.cleoStats.cycleCheckbox.disabled = false;
              appCore.cleoStats.pregnancyCheckbox.checked = false;
            }
            else {
              appCore.cleoStats.pregnancyCheckbox.checked = true;
            }
          }
        });
      },

      saveChanges: function() {
        var autoMensesCount = null,
            mensesLength = appCore.cleoStats.periodManual.value,
            autoCycleCount = null,
            cycleLength = appCore.cleoStats.cycleManual.value,
            pregnancy = null;

        if (appCore.cleoStats.periodCheckbox.checked == true) {
          autoMensesCount = 1;
        } else {
          autoMensesCount = 0;
        }

        if (appCore.cleoStats.cycleCheckbox.checked == true) {
          autoCycleCount = 1;
        } else {
          autoCycleCount = 0;
        }

        if (appCore.cleoStats.pregnancyCheckbox.checked == true) {
          pregnancy = 1;
        } else {
          pregnancy = 0;
        }


        var completeUrl = appCore.cleoStats.url + '?action=set&autoMensesCount=' + autoMensesCount + '&mensesLength=' + mensesLength + '&autoCycleCount=' + autoCycleCount + '&cycleLength=' + cycleLength + '&pregnancy=' + pregnancy;

        appCore.cleoStats.getAjaxData(completeUrl ,  function(err, data) {
          if (err !== null) {
            console.error(err);
          }
        });

        appCore.cleoStats.loadAndShowStats();
      },

      init: function() {
        if (appCore.cleoStats.validInit !== true) {
          // nastavení URL
          switch (window.location.hostname) {
            case 'localhost':
              appCore.cleoStats.url = 'http://' + window.location.hostname + '/cleopatra/app/common/stats/';
              break;
            default:
              appCore.cleoStats.url = 'https://' + window.location.hostname + '/app/common/stats/';
          }

          appCore.cleoStats.settingButton = document.getElementById('js-open-setting-button');
          appCore.cleoStats.period = document.getElementById('js-length-period');
          appCore.cleoStats.cycle = document.getElementById('js-length-cycle');
          appCore.cleoStats.cycleCheckbox = document.getElementById('js-automatic-cycle');
          appCore.cleoStats.periodCheckbox = document.getElementById('js-automatic-period');
          appCore.cleoStats.cycleManual = document.getElementById('js-manual-cycle-field');
          appCore.cleoStats.periodManual = document.getElementById('js-manual-period-field');
          appCore.cleoStats.saveButton = document.getElementById('js-stats-save-button');
          appCore.cleoStats.pregnancyCheckbox = document.getElementById('js-pregnancy-checkbox');
          //appCore.cleoStats.pregnancyInfo = document.getElementById('js-pregnancy-info');

          if (appCore.cleoStats.settingButton && appCore.cleoStats.period && appCore.cleoStats.cycle && appCore.cleoStats.cycleCheckbox && appCore.cleoStats.periodCheckbox && appCore.cleoStats.cycleManual && appCore.cleoStats.periodManual && appCore.cleoStats.saveButton && appCore.cleoStats.pregnancyCheckbox) {

            appCore.cleoStats.settingButton.addEventListener("click", function() {
              appCore.cleoStats.loadAndShowStats();
              $('#settings-modal').modal('show');
            });

            appCore.cleoStats.cycleCheckbox.addEventListener("change", function() {
              if (this.checked) {
                appCore.cleoStats.cycleManual.disabled = true;
                appCore.cleoStats.initCycleSlider(true);
              } else {
                appCore.cleoStats.cycleManual.disabled = false;
                appCore.cleoStats.initCycleSlider(false);
              }
            });

            appCore.cleoStats.periodCheckbox.addEventListener("change", function() {
              if (this.checked) {
                appCore.cleoStats.periodManual.disabled = true;
                appCore.cleoStats.initPeriodSlider(true);
              } else {
                appCore.cleoStats.periodManual.disabled = false;
                appCore.cleoStats.initPeriodSlider(false);
              }
            });

            appCore.cleoStats.pregnancyCheckbox.addEventListener("change", function() {
              if (this.checked) {
                appCore.cleoStats.cycleCheckbox.disabled = true;
                appCore.cleoStats.periodCheckbox.disabled = true;
                appCore.cleoStats.periodManual.disabled = true;
                appCore.cleoStats.initPeriodSlider(true);
                appCore.cleoStats.cycleManual.disabled = true;
                appCore.cleoStats.initCycleSlider(true);
              }
              else {
                appCore.cleoStats.cycleCheckbox.disabled = false;
                appCore.cleoStats.periodCheckbox.disabled = false;

                if (appCore.cleoStats.cycleCheckbox.checked) {
                  appCore.cleoStats.cycleManual.disabled = true;
                  appCore.cleoStats.initCycleSlider(true);
                }
                else {
                  appCore.cleoStats.cycleManual.disabled = false;
                  appCore.cleoStats.initCycleSlider(false);
                }
                if (appCore.cleoStats.periodCheckbox.checked) {
                  appCore.cleoStats.periodManual.disabled = true;
                  appCore.cleoStats.initPeriodSlider(true);
                }
                else {
                  appCore.cleoStats.periodManual.disabled = false;
                  appCore.cleoStats.initPeriodSlider(false);
                }
              }
            });

            appCore.cleoStats.saveButton.addEventListener("click", function() {
              appCore.cleoStats.saveChanges();
            });

            appCore.cleoStats.loadAndShowStats();

            appCore.cleoStats.validInit = true;
            return true;
          }
        }
        return false;
      }
    },

    cleoEvents: {
      url: null,
      googleGenerateUrl: null,
      validInit: false,
      eventWrapper: null,
      form: null,
      eventIdInput: null,
      startInput: null,
      lengthInput: null,
      action: null,
      showingItemsCount: 5,
      actualGroupWrapper: null,
      showingGroupsCount: 1,
      groupClassName: 'cleo-event__group js-event-group',
      hiddenGroupClassName: 'cleo-event__group js-event-group js-is-hidden',
      canBeUpdated: false,
      minCyclesCountsToUpdate: 3,

      showDate: function(date) {
        if (date) {
          var arr = date.split('-');
          var pole = arr[2] + '.' + arr[1] + '.' + arr[0];
          return pole;
        }
        return false;
      },
      showDayMonth: function(date) {
        if (date) {
          var arr = date.split('-');
          var pole = arr[2] + '.' + arr[1] + '.';
          return pole;
        }
        return false;
      },
      showYear: function(date) {
        if (date) {
          var arr = date.split('-');
          var pole = arr[0];
          return pole;
        }
        return false;
      },

      getAjaxData: function(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onload = function() {
          var status = xhr.status;
          if (status == 200) {
            callback(null, xhr.responseText);
          } else {
            callback(status);
          }
        };

        xhr.send();
      },

      // VENDORS

      showVendorModal: function() {
        $('#event-modal').modal('show');
      },

      initVendorDatepicker: function(disableDatepicker) {
        disableDatepicker = disableDatepicker || false;

        var originFiledValue = $('#js-input-start').val();

        $('#js-event-datepicker').datepicker({
          altField: '#js-input-start',
          altFormat: 'yy-mm-dd',
          maxDate: '+0D'
        });
        $('#js-event-datepicker').datepicker('option', $.datepicker.regional["cs"]);

        if ($('#js-event-datepicker').datepicker('isDisabled') && (!disableDatepicker)) {
          $('#js-event-datepicker').datepicker('option', 'disabled', false);
        }
        else if (disableDatepicker) {
          $('#js-event-datepicker').datepicker('option', 'disabled', true);
        }

        date = new Date(originFiledValue);
        $('#js-event-datepicker').datepicker('setDate', date);
      },

      initVendorSlider: function(disableSlider) {
        disableSlider = disableSlider || false;

        var handler = $('#js-slider-handler'),
            field = $('#js-input-length'),
            filedValue = field.val();

        $('#js-slider').slider({
          create: function() {
            handler.text( $(this).slider('value'));
          },
          slide: function(event, ui) {
            var actualValue = ui.value;
            handler.text(actualValue);
            field.val(actualValue);
            filedValue = actualValue;
          },
          min: 3,
          max: 8
        });


        $('#js-slider').slider('option','value',filedValue);
        handler.text( filedValue );

        var isDisabled = $('#js-slider').slider('option','disabled');
        if ((isDisabled) && (!disableSlider)) {
          $('#js-slider').slider('option','disabled',false);
        }
        else if (disableSlider) {
          $('#js-slider').slider('option','disabled',true);
        }
      },

      // OWN PLAIN JS

      doSomthingWithEvent: function(whatToDo) {
        var eventID = appCore.cleoEvents.eventIdInput.value,
            eventStart = appCore.cleoEvents.startInput.value,
            eventLength = appCore.cleoEvents.lengthInput.value,
            parameters = '?action=' + whatToDo + '&ID=' + eventID + '&start=' + eventStart + '&length=' + eventLength;

        appCore.cleoEvents.getAjaxData(appCore.cleoEvents.url + parameters,  function(err, data) {
          if (err !== null) {
            if (env) {
              env.infoBoxes.show('Chyba s úpravou události. Prosím, kontaktujte administrátora.', type = 'error');
            }
          }
          else {
            if ((whatToDo == 'create') || (whatToDo == 'delete')) {
              appCore.cleoEvents.showingGroupsCount = 1;
              appCore.cleoEvents.showMoreButton.style.display = 'none';
              appCore.cleoEvents.showFewerButton.style.display = 'none';
              appCore.cleoEvents.showEvents(false);
              if (whatToDo == 'create') {
                if (env) {
                  env.infoBoxes.show('Událost byla vytvořena.', 'info', 'quick');
                }
                appCore.cleoEvents.updateCalendars();
              }
              else {
                if (env) {
                  env.infoBoxes.show('Událost byla úspěšně smazána.', 'info', 'quick');
                }
                appCore.cleoEvents.updateCalendars();
              }
            }
            else if (whatToDo == 'update') {
              appCore.cleoEvents.showEvents(true);
              if (env) {
                env.infoBoxes.show('Událost byla upravena.', 'info', 'quick');
              }
              appCore.cleoEvents.updateCalendars();
            }

            appCore.cleoStats.loadAndShowStats();
          }
        });
      },

      startCreatingEvent: function () {
        appCore.cleoEvents.setFormState('create');
        var today = new Date(),
            dd = today.getDate(),
            mm = today.getMonth() + 1,
            yyyy = today.getFullYear();

        if (dd < 10) { dd = '0' + dd ;};
        if (mm < 10) { mm = '0' + mm ;};
        today = yyyy + '-' + mm + '-' + dd;

        // inputs values
        appCore.cleoEvents.eventIdInput.value = 0;
        appCore.cleoEvents.startInput.value = today;
        appCore.cleoEvents.lengthInput.value = 5;

        appCore.cleoEvents.modalLabel.innerHTML = "Vytvoření termínu menstruace";
        appCore.cleoEvents.showVendorModal();
        appCore.cleoEvents.initVendorDatepicker();
        appCore.cleoEvents.initVendorSlider();
      },

      startEditingEvent: function(eventID, eventStart, eventLength) {
        appCore.cleoEvents.setFormState('edit');
        appCore.cleoEvents.eventIdInput.value = eventID;
        appCore.cleoEvents.startInput.value = eventStart;
        appCore.cleoEvents.lengthInput.value = eventLength;

        appCore.cleoEvents.modalLabel.innerHTML = "Změna termínu menstruace";
        appCore.cleoEvents.showVendorModal();
        appCore.cleoEvents.initVendorDatepicker();
        appCore.cleoEvents.initVendorSlider();
      },

      startDeletingEvent: function(eventID, eventStart, eventLength) {
        appCore.cleoEvents.setFormState('remove');
        appCore.cleoEvents.eventIdInput.value = eventID;
        appCore.cleoEvents.startInput.value = eventStart;
        appCore.cleoEvents.lengthInput.value = eventLength;

        appCore.cleoEvents.modalLabel.innerHTML = "Smazání termínu menstruace";
        appCore.cleoEvents.showVendorModal();
        appCore.cleoEvents.initVendorDatepicker(true);
        appCore.cleoEvents.initVendorSlider(true);
      },

      showGroups: function() {
        var groups = document.getElementsByClassName('js-event-group');
        var countOfGroups = groups.length;

        for (var i = 0; i < countOfGroups; i++) {
          if (i < appCore.cleoEvents.showingGroupsCount) {
            groups[i].className = appCore.cleoEvents.groupClassName;
          }
          else {
            groups[i].className = appCore.cleoEvents.hiddenGroupClassName;
          }
        }

        if (appCore.cleoEvents.showingGroupsCount == countOfGroups) {
          appCore.cleoEvents.showMoreButton.style.display = 'none';
        }

        if (appCore.cleoEvents.showingGroupsCount == 1) {
          appCore.cleoEvents.showFewerButton.style.display = 'none';
          appCore.cleoEvents.showMoreButton.style.display = 'inline-block';
        }
        else {
          appCore.cleoEvents.showFewerButton.style.display = 'inline-block';
        }
      },

      showMoreGroups: function() {
        appCore.cleoEvents.showingGroupsCount++;
        appCore.cleoEvents.showGroups();
      },

      showFewerGroups: function() {
        if (appCore.cleoEvents.showingGroupsCount > 1) {
          appCore.cleoEvents.showingGroupsCount--;
          appCore.cleoEvents.showGroups();
        }
      },

      createHTMLEventItem: function(eventID, eventStart, eventEnd, eventLength, eventDays, i, count) {
        var eventDiv = document.createElement('div'),

            eventHeader = document.createElement('div'),

            eventDivNum = document.createElement('div'),
            eventDivNumSpan = document.createElement('span'),
            eventDivDate1 = document.createElement('div'),
            eventDivDate1Year = document.createElement('span'),
            eventDivDateDash = document.createElement('div'),
            eventDivDate2 = document.createElement('div'),
            eventDivDate2Year = document.createElement('span'),
            eventDivDays = document.createElement('div'),
            eventDivDaysSpan = document.createElement('span'),
            eventDivEdit = document.createElement('div'),
            eventDivEditDiv = document.createElement('div'),
            eventDivDelete = document.createElement('div'),

            eventDivEditIco = document.createElement('a'),
            eventDivDeleteIco = document.createElement('a'),

            textNum = document.createTextNode(i+1),
            textFrom = document.createTextNode(this.showDayMonth(eventStart)),
            textFromYear = document.createTextNode(this.showYear(eventStart)),
            textTo = document.createTextNode(this.showDayMonth(eventEnd)),
            textToYear = document.createTextNode(this.showYear(eventEnd)),
            textDash = document.createTextNode('-'),
            textDays = document.createTextNode(eventDays),
            textDaysSpan = document.createTextNode(' dní');

        eventHeader.innerHTML= '<div class="box-table__header"><div class="box-table__num"></div><div class="box-table__date">ZAČÁTEK</div><div class="box-table__arrow"></div><div class="box-table__date">KONEC</div><div class="box-table__cycle-len">DÉLKA CYKLU</div><div class="box-table__edit"></div><div class="box-table__delete"></div></div>';

        eventDivNumSpan.appendChild(textNum);
        eventDivNum.appendChild(eventDivNumSpan);
        eventDivDate1.appendChild(textFrom);
        eventDivDate1Year.appendChild(textFromYear);
        eventDivDate1.appendChild(eventDivDate1Year);

        eventDivDateDash.appendChild(textDash);

        eventDivDate2.appendChild(textTo);
        eventDivDate2Year.appendChild(textToYear);
        eventDivDate2.appendChild(eventDivDate2Year);

        eventDivDays.appendChild(textDays);
        eventDivDaysSpan.appendChild(textDaysSpan)
        if(eventDays != '-') eventDivDays.appendChild(eventDivDaysSpan);

        eventDivEditDiv.appendChild(eventDivEditIco);       
        eventDivEdit.appendChild(eventDivEditDiv);

        eventDivDelete.appendChild(eventDivDeleteIco);

        eventDiv.className = 'box-table';   //'cleo-event__item';
        //eventDivDetailWrapper.className = 'cleo-event__detail-wrapper';
        eventDivNum.className = 'box-table__num';
        eventDivDate1.className = 'box-table__date'; //'cleo-event__detail is--date';
        eventDivDateDash.className = 'box-table__arrow';
        eventDivDate2.className = 'box-table__date';
        eventDivDays.className = 'box-table__cycle-len'; //'cleo-event__detail is--days';
        eventDivEdit.className = 'box-table__edit';//'cleo-event__edit';
        eventDivEditDiv.className = 'cleo-util is--pointer';
        eventDivDelete.className = 'box-table__delete cleo-util is--pointer';//'cleo-event__delete';

        eventDivEditIco.className = 'cleo-icon is--pencil';//'cleo-icon is--pencil cleo-util is--pointer';
        eventDivDeleteIco.className = 'cleo-icon is--bin';//'cleo-icon is--bin cleo-util is--pointer';
        eventDivEditIco.title = 'edituj';
        eventDivDeleteIco.title = 'smaž';

        // event detail wrapper
        eventDiv.appendChild(eventDivNum);
        eventDiv.appendChild(eventDivDate1);//eventDivDetailWrapper.appendChild(eventDivDate);
        eventDiv.appendChild(eventDivDateDash);
        eventDiv.appendChild(eventDivDate2);
        eventDiv.appendChild(eventDivDays);//eventDivDetailWrapper.appendChild(eventDivDays);

        // event div
        //eventDiv.appendChild(eventDivDetailWrapper);
        eventDiv.appendChild(eventDivEdit);
        eventDiv.appendChild(eventDivDelete);

        eventDiv.setAttribute('data-event-id', eventID);
        eventDiv.setAttribute('data-event-start', eventStart);
        eventDiv.setAttribute('data-event-length', eventLength);
        eventDiv.setAttribute('data-event-days', eventDays);

        // grouping & adding to markup
        if ((i % appCore.cleoEvents.showingItemsCount) == 0) {
          appCore.cleoEvents.actualGroupWrapper = document.createElement('div');
          if ((i / appCore.cleoEvents.showingItemsCount) < appCore.cleoEvents.showingGroupsCount) {
            appCore.cleoEvents.actualGroupWrapper.className = appCore.cleoEvents.groupClassName;
          }
          else {
            appCore.cleoEvents.actualGroupWrapper.className = appCore.cleoEvents.hiddenGroupClassName;
          }

          appCore.cleoEvents.eventWrapper.appendChild(appCore.cleoEvents.actualGroupWrapper);
          appCore.cleoEvents.actualGroupWrapper.append(eventHeader);
          appCore.cleoEvents.actualGroupWrapper.append(eventDiv);
        }
        else {
          appCore.cleoEvents.actualGroupWrapper.appendChild(eventDiv);
        }


        // Bindings events
        eventDivEdit.addEventListener("click", function() {
          var eventDiv = this,
              attempts = 0;

          while (!eventDiv.getAttribute('data-event-id') && (attempts < 4)) {
            eventDiv = eventDiv.parentElement;
            attempts++;
          }

          var eventID = eventDiv.getAttribute('data-event-id'),
              eventStart = eventDiv.getAttribute('data-event-start'),
              eventLength = eventDiv.getAttribute('data-event-length');

          appCore.cleoEvents.startEditingEvent(eventID, eventStart, eventLength);
        });

        eventDivDelete.addEventListener("click", function() {
          var eventDiv = this,
              attempts = 0;

          while (!eventDiv.getAttribute('data-event-id') && (attempts < 4)) {
            eventDiv = eventDiv.parentElement;
            attempts++;
          }

          var eventID = eventDiv.getAttribute('data-event-id'),
              eventStart = eventDiv.getAttribute('data-event-start'),
              eventLength = eventDiv.getAttribute('data-event-length');

          appCore.cleoEvents.startDeletingEvent(eventID, eventStart, eventLength);
        });
      },

      isServiceLoggedIn: function(service) {
        var el = document.getElementsByTagName('body')[0];
        if (appCore.hasClass(el, 'js--' + service)) {
          return true;
        }
        return false;
      },

      mustUpdate: function() {
        return true;
      },

      showEvents: function(lastActionIsEdit) {
        lastActionIsEdit = lastActionIsEdit || false;

        appCore.cleoEvents.getAjaxData(appCore.cleoEvents.url + '?action=get',  function(err, data) {
          if (err !== null) {
            console.error(err);
          }
          else {
            // clearing wrapper
            appCore.cleoEvents.eventWrapper.innerHTML = '';
            var dataObject = JSON.parse(data);
            var count = dataObject.length;

            // Can be update the calendar?
            if (( count > 1 ) && (appCore.cleoEvents.mustUpdate())) {
              appCore.cleoEvents.canBeUpdated = true;
            }
            else {
              appCore.cleoEvents.canBeUpdated = false;
            }

            var updateCalendarsHandlers = document.getElementsByClassName('js-update-calendars');
            for (var i = 0; i < updateCalendarsHandlers.length; i++) {
              if (appCore.cleoEvents.canBeUpdated) {
                appCore.addClass(updateCalendarsHandlers[i], 'js--can-be-updated');
                appCore.removeClass(updateCalendarsHandlers[i], 'js--cant-be-updated');
              }
              else {
                appCore.addClass(updateCalendarsHandlers[i], 'js--cant-be-updated');
                appCore.removeClass(updateCalendarsHandlers[i], 'js--can-be-updated');
              }
            }

            if (count > 0) {
              if ((count > appCore.cleoEvents.showingItemsCount) && (!lastActionIsEdit)) {
                appCore.cleoEvents.showMoreButton.style.display = 'inline-block';
              }
              else if (!lastActionIsEdit) {
                appCore.cleoEvents.showMoreButton.style.display = 'none';
              }
              for (var i = 0; i < count; i++) {
                if (i == 0) {
                  dataObject[i]['days'] = "-";
                }
                appCore.cleoEvents.createHTMLEventItem(dataObject[i]['id'], dataObject[i]['start'], dataObject[i]['end'], dataObject[i]['length'], dataObject[i]['days'], i, count);
              }
            }
            else {
              appCore.cleoEvents.eventWrapper.innerHTML = '<div class="pl-2">Uživatel zatím nemá vytvořenou žádnou událost! <br><a href="#jak-zacit" class="btn btn-info btn-smaller mb-1 mt-1">Jak začít?</a></div>';
            }
            if (count < appCore.cleoEvents.minCyclesCountsToUpdate) {                                //show message how much cycles left to generate calendar events 
              var cyclesLeft = document.createElement('span');
              cyclesLeft.className = 'italic-pink d-block pl-2';
              var cycleLeftCount = appCore.cleoEvents.minCyclesCountsToUpdate - count;
              
              var cycleText = "1 cyklus. ";                                         //if one cycle left show one, else if two....
              if(cycleLeftCount > 1) cycleText = cycleLeftCount+" cykly. ";
              
              var textSpan = 'Pro vytvoření tvého kalendáře zbývá zadat ještě '+ cycleText;
              var createSpanText = document.createTextNode(textSpan);

              var createA = document.createElement('a');
              var createAText = document.createTextNode('Proč to potřebujeme?');
              createA.setAttribute('href', '/app/pages/info?q=3');
              createA.appendChild(createAText);
              cyclesLeft.appendChild(createSpanText);
              cyclesLeft.setAttribute('style', 'padding-left:18px');
              cyclesLeft.appendChild(createA);

              appCore.cleoEvents.eventWrapper.appendChild(cyclesLeft);// = 'Uživatel zatím nemá vytvořenou žádnou událost!';

              appCore.cleoEvents.mujKalendar.style.display = 'none';          // show button Muj Kalendar only if the calendar was created
            } else appCore.cleoEvents.mujKalendar.style.display = 'table';    // ...
          }
        });
      },

      setFormState: function(arrangement) {
        switch (arrangement) {
          case 'default':
            appCore.cleoEvents.eventIdInput.type = 'hidden';
            appCore.cleoEvents.createButton.style.display = 'none';
            appCore.cleoEvents.updateButton.style.display = 'none';
            appCore.cleoEvents.deleteButton.style.display = 'none';
          break;
          case 'create':
            // disabling inputs
            appCore.cleoEvents.startInput.disabled = false;
            appCore.cleoEvents.lengthInput.disabled = false;

            // buttons visibility
            appCore.cleoEvents.createButton.style.display = 'inline-block';
            appCore.cleoEvents.updateButton.style.display = 'none';
            appCore.cleoEvents.deleteButton.style.display = 'none';
          break;
          case 'edit':
            // disabling inputs
            appCore.cleoEvents.startInput.disabled = false;
            appCore.cleoEvents.lengthInput.disabled = false;

            // buttons visibility
            appCore.cleoEvents.createButton.style.display = 'none';
            appCore.cleoEvents.updateButton.style.display = 'inline-block';
            appCore.cleoEvents.deleteButton.style.display = 'none';
          break;

          case 'remove':
            // disabling inputs
            appCore.cleoEvents.startInput.disabled = true;
            appCore.cleoEvents.lengthInput.disabled = true;

            // buttons visibility
            appCore.cleoEvents.createButton.style.display = 'none';
            appCore.cleoEvents.updateButton.style.display = 'none';
            appCore.cleoEvents.deleteButton.style.display = 'inline-block';
          break;
        }
      },

      updateCalendars: function() {
        if (appCore.cleoEvents.canBeUpdated) {

          // GOOGLE PART
          if (env) {
            env.loadingPhase.setText('<span>Generují se události v Google kalendáři.<br />Prosím, nezavírejte okno prohlížeče.</span>');
            env.loadingPhase.show();
          }

          if (appCore.cleoEvents.isServiceLoggedIn('google')) {
            appCore.cleoEvents.getAjaxData(appCore.cleoEvents.googleGenerateUrl,  function(err, data) {
              if (err !== null) {
                if (env) {
                  env.infoBoxes.show('Bohužel se nepodařilo vygenerovat události ve Vašem Google kalendáři.','error');
                }
              }
              else {
                var dataObject = JSON.parse(data);

                if (dataObject[0]['status'] == "OK") {
                  if (env) {
                    env.infoBoxes.show(dataObject[0]['error'],'info');
                  }
                }
                else {
                  if (env) {
                    env.infoBoxes.show(dataObject[0]['error'],'error');
                  }
                }
                if (env) {
                  env.loadingPhase.hide();
                }
              }
            });
          } else {
            console.log('Google services is not available!');
            if (env) {
              env.loadingPhase.hide();
            }
          }
          // END OF GOOGLE PART
        }
        else {
          if (env) {
            env.infoBoxes.show('Nelze generovat budoucí události.','error','quick');
          }
        }
      },

      init: function() {
        if (appCore.cleoEvents.validInit !== true) {
          // nastavení URL
          switch (window.location.hostname) {
            case 'localhost':
              appCore.cleoEvents.url = 'http://' + window.location.hostname + '/cleopatra/app/common/event/';
              appCore.cleoEvents.googleGenerateUrl = 'http://' + window.location.hostname + '/cleopatra/app/google/generate/';
              break;
            default:
              appCore.cleoEvents.url = 'https://' + window.location.hostname + '/app/common/event/';
              appCore.cleoEvents.googleGenerateUrl = 'https://' + window.location.hostname + '/app/google/generate/';
          }

          appCore.cleoEvents.eventWrapper = document.getElementById('js-event-wrapper');
          appCore.cleoEvents.form = document.getElementById('js-event-form');
          appCore.cleoEvents.eventIdInput = document.getElementById('js-input-id');
          appCore.cleoEvents.startInput = document.getElementById('js-input-start');
          appCore.cleoEvents.lengthInput = document.getElementById('js-input-length');
          appCore.cleoEvents.createButton = document.getElementById('js-button-create-new');
          appCore.cleoEvents.updateButton = document.getElementById('js-button-update');
          appCore.cleoEvents.deleteButton = document.getElementById('js-button-delete');
          appCore.cleoEvents.createStarter = document.getElementById('js-create-new');
          appCore.cleoEvents.showMoreButton = document.getElementById('js-show-more-groups');
          appCore.cleoEvents.showFewerButton = document.getElementById('js-show-fewer-groups');
          appCore.cleoEvents.modalLabel = document.getElementById('js-event-modalLabel');
          appCore.cleoEvents.mujKalendar = document.getElementById('muj-kalendar');

          if (appCore.cleoEvents.eventWrapper && appCore.cleoEvents.form && appCore.cleoEvents.eventIdInput && appCore.cleoEvents.startInput && appCore.cleoEvents.lengthInput && appCore.cleoEvents.createButton && appCore.cleoEvents.updateButton && appCore.cleoEvents.deleteButton && appCore.cleoEvents.createStarter && appCore.cleoEvents.showMoreButton && appCore.cleoEvents.showFewerButton && appCore.cleoEvents.modalLabel) {
            appCore.cleoEvents.setFormState('default');
            appCore.cleoEvents.showMoreButton.style.display = 'none';
            appCore.cleoEvents.showFewerButton.style.display = 'none';
            appCore.cleoEvents.showEvents();

            appCore.cleoEvents.createButton.addEventListener("click", function() {
              appCore.cleoEvents.doSomthingWithEvent('create');
            });

            appCore.cleoEvents.updateButton.addEventListener("click", function() {
              appCore.cleoEvents.doSomthingWithEvent('update');
            });

            appCore.cleoEvents.deleteButton.addEventListener("click", function() {
              appCore.cleoEvents.doSomthingWithEvent('delete');
            });

            appCore.cleoEvents.createStarter.addEventListener("click", function() {
              appCore.cleoEvents.startCreatingEvent();
            });

            appCore.cleoEvents.showMoreButton.addEventListener("click", function() {
              appCore.cleoEvents.showMoreGroups();
            });

            appCore.cleoEvents.showFewerButton.addEventListener("click", function() {
              appCore.cleoEvents.showFewerGroups();
            });

            var updateCalendarsHandlers = document.getElementsByClassName('js-update-calendars');
            for (var i = 0; i < updateCalendarsHandlers.length; i++) {
              updateCalendarsHandlers[i].addEventListener("click", function() {
                appCore.cleoEvents.updateCalendars();
              });
            }

            appCore.cleoEvents.validInit = true;
            return true;
          }
          else {
            if (appCore.cleoEvents.eventWrapper) {
              appCore.cleoEvents.eventWrapper.innerHTML = 'Error: cleoEvent init fail!';
            }
            else {
              console.log('Error: cleoEvent init fail!');
            }
          }
        }
        return false;
      }
    },

    options: {
      validInit: false,

      init: function() {
        if (appCore.options.validInit !== true) {
          appCore.options.showConfirmButton = document.getElementById('js-show-confirm');
          appCore.options.hideConfirmButton = document.getElementById('js-hide-confirm');
          appCore.options.confirmBox = document.getElementById('js-confirm');

          if (appCore.options.showConfirmButton && appCore.options.hideConfirmButton && appCore.options.confirmBox) {
            appCore.options.showConfirmButton.style.display = "inline-block";
            appCore.options.confirmBox.style.display = "none";

            appCore.options.showConfirmButton.addEventListener("click", function() {
              appCore.options.showConfirmButton.style.display = "none";
              appCore.options.confirmBox.style.display = "block";
            });

            appCore.options.hideConfirmButton.addEventListener("click", function() {
              appCore.options.showConfirmButton.style.display = "inline-block";
              appCore.options.confirmBox.style.display = "none";
            });

            appCore.options.validInit = true;
            return true;
          }
        }
        return false;
      }
    },

    init: function() {
      if (!appCore.cleoStats.init()) { console.log('ERROR: Cleo-stats initialization failed!'); }
      if (!appCore.cleoEvents.init()) { console.log('ERROR: Cleo-stats initialization failed!'); }
      if (!appCore.options.init()) { console.log('ERROR: Options initialization failed!'); }
    }
  }

  if (document.readyState!='loading') appCore.init();
  else if (document.addEventListener) document.addEventListener('DOMContentLoaded', appCore.init);
  else document.attachEvent('onreadystatechange', function() {
    if (document.readyState=='complete') appCore.init();
  });
})();
