<?php
// CLEO home page template
// Author: Martin Krotil
// Edit: Honza Kouba @ digiage.cz

require_once('./core/settings.php');
require_once('./core/cleo_users.php');

header('Content-Type: text/html; charset=utf-8');

$cUsers = new cleoUsers;

// Login box content
$loginBoxContent = '';
if ($cUsers->isUserLoggedIn()) {
  if( $picture = $cUsers->getUserInformation('picture') ) {
    $loginBoxContent = '<div class="cleo-user"><img src="' . $picture . '" class="cleo-user__img" alt="Profilový obrázek" /><span class="cleo-user__text">' . $cUsers->getUserInformation('name') . '</span></div>';
  }
  else {
    $loginBoxContent = '<div class="cleo-user"><span class="cleo-user__text">' . $cUsers->getUserInformation('name') . '</span></div>';
  }
}
else {
  $loginBoxContent = '<a href="' . GOOGLE_LOGIN . '" class="cleo-google js--notice"><span class="cleo-google__img"></span><span class="cleo-google__text">Sign in with Google</span></a>';
}

  getMainHeader("Cleopatra - Cyklický kalendář online",0,$cUsers->isServiceLoggedIn('google'));

  require_once('./core/tmp/navigation.php');
?>

    <header class="masthead d-flex">
      <img class="hero-left" src="img/hero-left-circle.svg" alt="">
      <img class="hero-back" src="img/hero-back-circle.svg" alt="">
      <picture>
        <source srcset="img/hero-lady.avif" media="(min-width: 768px)" type="image/avif">
        <source srcset="img/hero-lady.webp" media="(min-width: 768px)" type="image/webp">
        <source srcset="img/hero-lady.png" media="(min-width: 768px)" type="image/png">
        <source type="image/gif" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="> <!-- nebude se stahovat na malých zařízeních -->
        <img src="img/hero-lady.png" class="hero-front" alt="Žena, která na mobilním telefonu otevírá kalendář Cleopatra a dívá se dopředu">
      </picture>
      <div class="container my-auto hero-text">
        <div class="row">
          <div class="col-12 col-lg-7">
            <div class="cleo-header__text">
              <h1 class="font-weight-bold">Kalendář každé <span>cyklické ženy</span><!-- <br>Plánuj Předem! --></h1> 
              <p>Google kalendář pro ženy co plánují <br> s lehkostí a přehledem.</p>
              <ul class="arrow cleo-util">
                <li>Chceš plánovat v souladu se svými cykly?</li>
                <li>Chceš využít své proměnlivosti naplno?</li>
                <li>Používáš Google kalendář?</li>
              </ul>
              <div class="hero-links">
                <a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-main btn-main-shine js--notice">Vyzkoušej ZDARMA</a> &nbsp; &nbsp; 
                <a href="#jak-to-funguje" class="btn btn-main-outline">Více info</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-xl-10 mx-auto text-center">
            <h2 id="jak-to-funguje" class="section-heading link">Jak kalendář Cleopatra funguje?</h2>
            <div class="subheading">Získej svůj osobní cyklický kalendář ve třech snadných krocích</div>
            <!-- <hr class="dark my-4"> -->
            <a href="<?php echo NAVOD_VIDEO; ?>" target="_blank" class="btn btn-info-outline btn-smaller">
              <span class="cleo-icon is--play"></span> Video návod
            </a>
            <div class="row mt-5 mt-md-7 mb-8 kroky">
              <div class="col-12 col-md-4 box">
                <div><strong><a href="<?php echo GOOGLE_LOGIN; ?>">Registruj</a> se osobním Google účtem</strong> Pokud ještě Google účet nemáš, můžeš si jej snadno <a href="https://accounts.google.com/" target="_blank">založit</a>. Díky Google účtu máš kalendář vždy aktuální a tvá data jsou chráněna.</div>
              </div>
              <div class="col-12 col-md-4 box">
                <div><strong>Zadej <?php echo MIN_CYCLES_TO_CALCULATE; ?> termíny začátku posledních menstruací</strong> Tím se ti ve tvém Google kalendáři vytvoří osobní kalendář s fázemi cyklů a názvem Cleopatra. </div>
              </div>
              <div class="col-12 col-md-4 box">
                <div><strong>Každý měsíc přidáš aktuální termín menstruace</strong> Není to samozřejmě podmínka, ale čím novější data máme, tím je výpočet cyklů a fází přesnější.</div>
                <span class="vizualni-zena">
                  <span>Jsi spíše vizuální žena?</span> Máme k dispozici i podrobný <a href="<?php echo NAVOD_VIDEO; ?>" target="_blank">videonávod</a>
                </span>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-lg-7 col-xxl-6 text-justify">
                <h3>Ten správný čas pro tvé plány</h3>
                <p>Štve Tě, že se stále musíš přizpůsobovat plánům ostatních, rozvrhům dětí, pracovní době, termínům akcí a podobně?</p>
                <p>Získej možnost ovlivnit věci ve svůj prospěch, užít si každý okamžik naplno a zbytečně se nestresovat. Protože právě <strong>teď je ten nejlepší čas!</strong> Jen vědět na co. Věřím, že právě s tím Ti kalendář Cleopatra pomůže. Právě teď je nejlepší čas vyzkoušet Cleopatru a plánovat si svůj život podle sebe!</p>
                <p>Na <strong>první měsíc zdarma</strong> a pokud se ti kalendář zalíbí, můžeš si jej předplatit na rok nebo i víc.</p>
                <p class="mt-3"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice mb-1">Vyzkoušej měsíc ZDARMA</a> &nbsp; <a href="#vyhody" class="btn btn-info-outline mb-1">Výhody Cleopatry</a></p>
              </div>
              <div class="col-12 col-lg-5 col-xxl-6">

                <div class="calendar-graphics perspective">
                  <img src="img/calendar.svg" alt="Grafika kalendáře s cykly" loading="lazy">
                </div>
              </div>
            </div>
           <!--  <p class="mb-4">Víš, kdy je Tvůj nejlepší čas na důležitou schůzku? Kdy začít nový projekt? Kdy naplánovat oslavu a dobře se na ní bavit nebo naopak zůstat doma, pokud to jen trochu jde? Určitě ano. Už sis mnohokrát všimla, že jsou dny, kdy se cítíš skvěle a vše Ti jde od ruky a pak dny, kdy je lepší o věcech jen přemýšlet? Že jsou dny, kdy zvládneš opravdu velkou zátěž, a pak dny, kdy Tě rozhodí každá maličkost? Jistě.</p>
            --> 
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8 col-xl-6 mx-auto text-center">
            <h2 class="section-heading link" id="vyhody">Výhody Cleopatry</h2>
            <div class="subheading">Získej svůj osobní cyklický kalendář ve třech snadných krocích</div>
            <div class="text-justify">
              <ul class="arrow">
                <li><strong>30 dní zdarma</strong> na vyzkoušení funkčnosti!</li>
                <li><strong>Plán</strong> osobních cyklů a jeho fází <strong>na 6 měsíců dopředu</strong>. Čím víc termínu menstruací zadáš, tím přesnější plány se ti zobrazí.</li>
                <li><strong>Cena</strong> předplatného pouze <?php echo PAYMENT_NORMAL; ?>,- Kč na celý rok (+měsíc zdarma)</li>
                <li><strong>Jednoduchá platba</strong> on-line na rok i více dopředu, okamžité prodloužení platnosti.</li>
                <li><strong>Slevový kupón</strong> od některého z našich <a href="#partneri">Partnerů</a> ti zajistí výraznou slevu předplatného</li>
                <li><strong>Cleopatru máš na mobilu</strong> i na všech zařízeních, co používáš - notebook, počítač, tablet. Zkrátka všude tam, kde používáš svůj Google kalendář.</li>
                <li><strong>Vše v jednom kalendáři</strong>. Žádné speciální aplikace, speciální přihlašování, abys věděla, kdy je nejvhodnější doba k plánování těhotenství či pracovní schůzky.</li>
                <li><strong>Osobní, rodinný a pracovní život</strong> se dá jen těžko oddělit a proto je důležité mít možnost podívat se na jedno místo a mít přehled o všech důležitých věcech, které potřebuješ pro své plánování.</li>
                <li><strong>Cleopatra bude jeden ze tvých Google kalendářů</strong>, takže jej můžeš stejně tak používat. Zobrazit, skrýt, nechat si připomenout, nebo třeba sdílet s partnerem. </li>
                <li><strong>Přehledná evidence cyklů</strong> díky pravidelnému zadávání termínu své menstruace. To se ti může hodit při návštěvě gynekologie :).</li>
                <li>Pokud tě kalendář Cleopatra neosloví, můžeš jej kdykoliv jednoduše smazat.</li>
              </ul>
              <p class="mb-3 mt-3">Na stránce <a href="<?php echo INFO_PAGE;?>"><strong>Často kladené otázky</strong></a> najdeš řadu užitečných doporučení a odpovědí na nejčastější dotazy. Ve <a href="<?php echo TERMS_PAGE;?>">Všeobecných obchodních podmínkách</a> najdeš veškeré podrobnosti</p>
              <p class="mt-5 text-center"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice mb-1">Vyzkoušej Cleopatru</a> &nbsp; <a href="#faze-cyklu" class="btn btn-info-outline mb-1">Jaké jsou fáze cyklu</a> &nbsp; </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-primary">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8 col-xl-6 mx-auto">
            <h2 class="section-heading link" id="cyklicka-zena">Cyklická žena je proměnlivá</h2>
            <div class="subheading">Cyklus ovlivňuje nás a my můžeme ovlivnit naše plány</div>
            <hr class="dark mb-2 ml-0">
            <div class="text-justify">
              <p>Jednotlivé fáze souvisí s naším menstruačním a ovulačním cyklem, s tím jak nám stoupá a klesá hladina hormonů v průběhu měsíce. Máme přitom jedinečnou možnost využít jednotlivé fáze svého cyklu tím nejlepším způsobem, který je nám přirozený.</p>
              <p>Každý měsíční cyklus má čtyři odlišné fáze. Ty nemusí být stejně dlouhé, ani pevně ohraničené, ale zato jsou jedinečné a nepostradatelné! Každá z nich má svůj význam, a také svůj potenciál. Můžeme je využít v osobním i pracovním životě a pro plnění svých nápadů a projektů. Je ale těžké předem najít nejvhodnější čas a využít je ve svůj prospěch.</p>
              <p>Zajímavé informace a podrobnosti k cyklům najdeš v knize Cyklická žena od Mirandy Gray a na webu <a href="https://www.cyklickazena.cz/" target="_blank">Cyklická žena</a>. </p>
            </div>
            <div>
            <p class="mt-3"><a href="#faze-cyklu" class="btn btn-info">Jaké jsou fáze cyklu</a></p>
            </div>
          </div>
          <div class="col-12 col-xl-6 d-flex align-items-center perspective">
            <iframe src="<?php echo PROMENLINA_ZENA_VIDEO_EMBED;?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="cleo-thumbnail is--video" loading="lazy"></iframe>
          </div>
        </div>
        <h3 class="mt-4 text-center link" id="faze-cyklu">Stručný popis jednotlivých fází cyklu</h3>
        <div class="row mt-2 mb-5">
          <div class="col-lg-9 text-justify box">
            <div class="portfolio-box">
              <div class="faze-foto">
                <picture>
                  <source srcset="img/cycles/cycle1.avif" type="image/avif">
                  <source srcset="img/cycles/cycle1.jpg" type="image/jpeg">
                  <img src="img/cycles/cycle1.jpg" class="img-fluid is--yellow" alt="Usměvavá žena v dynamické fázi" loading="lazy">
                </picture>
                <!-- <div class="portfolio-box__circle is--yellow"></div> -->
              </div>
              <div class="faze-text">
                <h4 class="font-weight-bold">Dynamická fáze</h4>
                Období po menstruaci a před ovulací, kdy je žena aktivní, extrovertní a energická. Je to čas na uskutečnění osobních změn, ke kterým v předchozích fázích došla. Myšlení má logické, objektivní a dynamické. Je to správný čas pro nové projekty, stanovení priorit a plánování postupných kroků k dosažení cíle. Čas vhodný k učení, zkoumání i k fyzické vytrvalosti.
              </div>
            </div>
          </div>
          <div class="col-lg-9 ml-auto faze-right text-justify box">
            <div class="portfolio-box">
              <div class="faze-text">
                <h4 class="font-weight-bold">Expresivní fáze</h4>
                Období kolem ovulace, kdy vajíčko dozrává a žena je připravena k oplodnění. Je velmi společenská a do popředí jejího zájmu se dostávají vztahy, emoce a city. Je komunikativní a empatická, což jí pomáhá pracovat v týmu a ostatní podporovat v jejich práci. Je to fáze, kdy působí nejvíce „žensky“ a připadá si v této roli opravdu dobře a sebejistě.
              </div>
              <div class="faze-foto">
                <picture>
                  <source srcset="img/cycles/cycle2.avif" type="image/avif">
                  <source srcset="img/cycles/cycle2.jpg" type="image/jpeg">
                  <img src="img/cycles/cycle2.jpg" class="img-fluid is--red" alt="Žena s mobilním telefonem v expresivní fázi" loading="lazy">
                </picture>
                <!-- <div class="portfolio-box__circle is--yellow"></div> -->
              </div>
            </div>
          </div>
          <div class="col-lg-9 text-justify box">
            <div class="portfolio-box">
              <div class="faze-foto">
                <picture>
                  <source srcset="img/cycles/cycle4.avif" type="image/avif">
                  <source srcset="img/cycles/cycle4.jpg" type="image/jpeg">
                  <img src="img/cycles/cycle4.jpg" class="img-fluid is--blue" alt="Žena se zápisníkem a drdolem v kreativní fázi" loading="lazy">
                </picture>
                <!-- <div class="portfolio-box__circle is--yellow"></div> -->
              </div>
              <div class="faze-text">
                <h4 class="font-weight-bold">Kreativní fáze</h4>
                Období před menstruací, kdy hladina hormonů klesá a více se dostává do popředí intuice, nápady, odhalení problémů. Je to období, kdy je žena tvůrčí při řešení problémů a vyjasňování svého směru. Je to období nutkavé vnitřní aktivity, projevuje se emocemi a vášní i mentální aktivitou. Žena je v tuto dobu kritická ke všemu, co nefunguje, má výraznou představivost, intuici a vhled.
              </div>
            </div>
          </div>
          <div class="col-lg-9 ml-auto faze-right text-justify box">
            <div class="portfolio-box">
              <div class="faze-text">
                <h4 class="font-weight-bold">Reflektivní fáze</h4>
                Období menstruace, kdy je žena introvertní a obrací se více ke svému vnitřnímu já i svému tělu. Má potřebu vzdálit se a zpomalit. Zpracovává v sobě události, které prožila a emoce které vypluly na povrch. Je to období pasivní, kdy má žena intenzivní potřebu odpočinku a potřebuje věnovat čas vlastní regeneraci. Každá běžná činnost ji stojí velké úsilí, ale jednoduše nalézá důležitou podstatu věcí a událostí.
              </div>
              <div class="faze-foto">
                <picture>
                  <source srcset="img/cycles/cycle3.avif" type="image/avif">
                  <source srcset="img/cycles/cycle3.jpg" type="image/jpeg">
                  <img src="img/cycles/cycle3.jpg" class="img-fluid is--purple" alt="Žena v reflektivní fázi s polštářem před obličejem" loading="lazy">
                </picture>
                <!-- <div class="portfolio-box__circle is--yellow"></div> -->
              </div>
            </div>
          </div>
        </div>
        <p class="text-center"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice mb-1">Vyzkoušej Cleopatru</a> &nbsp; <a href="<?php echo INFO_PAGE;?>" class="btn btn-info-outline mb-1">Často kladené otázky</a> &nbsp; </p>
      </div>  
<!-- 
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Cyklus ovlivňuje nás <br />a&nbsp;my můžeme ovlivnit naše plány</h2>
            <hr>
            <p class="mb-4">Souvisí to s naším menstruačním a ovulačním cyklem, s tím jak nám stoupá a klesá hladina hormonů v průběhu měsíce. Máme přitom jedinečnou možnost využít jednotlivé fáze našeho cyklu tím nejlepším způsobem, který je nám přirozený. Každý měsíční cyklus má čtyři odlišné fáze. Ty nemusí být stejně dlouhé, ani pevně ohraničené, ale za to jsou jedinečné a nepostradatelné! Každá z nich má svůj význam, a také svůj potenciál. Můžeme je využít v osobním i v pracovním životě a pro plnění našich nápadů a projektů. Je ale těžké plánovat dopředu, najít nejvhodnější čas s předstihem a využít jej tak ve svůj prospěch. Zajímavé informace a podrobnosti k cyklům najdeš v knize Cyklická žena od Mirandy Gray.</p>
            <p class="mb-4"><a href="<?php echo GOOGLE_LOGIN;?>" class="btn btn-info js--notice">Vyzkoušej měsíc ZDARMA</a></p>
          </div>
        </div>
      </div> -->
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8 mx-auto text-center">
            <h2 class="section-heading link" id="partneri">Partneři kalendáře Cleopatra</h2>
            <div class="subheading">Konceptem cyklické ženy se zabývá mnoho žen a firem, které s námi spolupracují. Mohou Ti dát osobní referenci či dokonce <strong>slevový kupón na roční předplatné</strong> kalendáře Cleopatra.</div>
            <hr class="dark mb-3">
          </div>
          <div class="col-12 col-xl-10 col-xxl-8 mx-auto text-justify box">
            <div class="portfolio-box">
              <div class="partneri-foto">
              <a href="https://fotohavlin.cz" target="_blank">
                <picture>
                  <source srcset="img/partneri/zuzana_havlinova.avif" type="image/avif">
                  <source srcset="img/partneri/zuzana_havlinova.png" type="image/png">
                  <img src="img/partneri/zuzana_havlinova.png" class="img-fluid" alt="logo Zuzany Havlínové" loading="lazy">
                </picture>
              </a>
              </div>
              <div class="partneri-text">
                <h4 class="font-weight-bold">Fotky a Video - Zuzana Havlínová</h4>
                Profesionální fotografka a videotvůrkyně, která vyfotí a natočí, co si budete přát – nejraději fotí a natáčí lidi, akce, reportáže, emoce, ale nebojí se žádné výzvy! <br>
                Podívejte se na ukázky její práce na <a href="https://fotohavlin.cz" target="_blank">https://fotohavlin.cz</a>

              </div>
            </div>
          </div>
          <div class="col-12 col-xl-10 col-xxl-8 mx-auto text-justify box">
            <div class="portfolio-box">
              <div class="partneri-foto">
              <a href="https://fotohavlin.cz" target="_blank">
                <picture>
                  <source srcset="img/partneri/michaela_vancatova.avif" type="image/avif">
                  <source srcset="img/partneri/michaela_vancatova.png" type="image/png">
                  <img src="img/partneri/michaela_vancatova.png" class="img-fluid" alt="logo Zuzany Havlínové" loading="lazy">
                </picture>
              </a>
              </div>
              <div class="partneri-text">
                <h4 class="font-weight-bold">MUDr. Michaela Vančatová</h4>
                Maminka-lékařka se zkušeností s neplodností, jejíž vášní se stalo zdraví bez "chemie": Provede Vás plánováním miminka, připraví na porod a naučí pečovat o zdraví miminka (kojení, podpora pohybového vývoje, bezpečné očkování, příkrmy, vyléčení atopického ekzému). Vyberte si s čím Vám pomůže na <a href="https://michaelavancatova.cz" target="_blank">https://michaelavancatova.cz</a>

              </div>
            </div>
          </div>
          <div class="col-12 col-xl-10 col-xxl-8 mx-auto text-justify box">
            <div class="portfolio-box">
              <div class="partneri-foto">
              <a href="https://www.vercajandova.cz" target="_blank">
                <picture>
                  <source srcset="img/partneri/VJ-Logo.avif" type="image/avif">
                  <source srcset="img/partneri/VJ-Logo.jpg" type="image/jpeg">
                  <img src="img/partneri/VJ-Logo.jpg" class="img-fluid" alt="logo Ing. Veroniky Jandové" loading="lazy">
                </picture>
              </a>
              </div>
              <div class="partneri-text">
                <h4 class="font-weight-bold">Ing. Veronika Jandová</h4>
                Osobní trenér a výživový kouč <a href="https://www.vercajandova.cz" target="_blank">www.vercajandova.cz</a>, tel: <a href="tel:739691235">739 691 235</a>.
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<?php
  require_once('./core/tmp/footer.php');

  if (isset($_GET['after'])) {
    require_once('./core/cleo_dialog_modal.php');
  }

  getMainFooter(0,0);
?>
