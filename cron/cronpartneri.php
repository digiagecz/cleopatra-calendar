<?php 
  require_once('../core/settings.php');
  require_once('../core/cleo_statistics.php');
  //require_once('../core/cleo_users.php');
  //require_once('../core/cleo_cron.php');
  $cStats = new cleoStatistics;

  $dayToSend = 1;

  if(date('j') == $dayToSend){
    $lastMonth = date("F", strtotime("first day of previous month"));
    $table = $cStats->getLastMonthPartnersCouponsTotals();

    $sent = $cStats->sendEmail(EMAIL_INFO,'Pravidelné měsíční statistiky Cleopatra kuponů za '.$lastMonth, $cStats->formatEmailTable($table));     //.', '.EMAIL_INFO
    
    if($sent){
      $cStats->log(0,'success','statistiky partnerů odeslány emailem');
      echo "statistiky partnerů odeslány emailem";
    } 
    else{
      $cStats->log(0,'error','odeslání statistik partnerů se nezdařilo');
      echo "odeslání statistik partnerů se nezdařilo";
    } 
  } 
  else {
    //$cStats->log(0,'error','někdo se pokusil spustit ');  //cron se spousti kazdy den... takze by to v logach dalo bordel
    echo "dnes není ten správný den k odesílání statistik partnerů. je teprve ".date('j').".ho a statistiky se odesílají ".$dayToSend,".ho";
  }

?>