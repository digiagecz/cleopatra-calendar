<?php

 // Request configuration, please fill these
 $config = array(
   'base_domain' => 'cleopatra-calendar.com', // webhosting domain
   'request_domain' => 'cleopatra-calendar.com', // domain or subdomain name (also web directory name)
   'request_scheme' => 'http', // request protocol - http or https
   'script_name' => '/cron/cron-partneri.php', // target script name, including path (when in subdirectory)
   'query_string' => '', // request variables
);

 // Request execution, nothing needs to be changed down here
 $_SERVER['SCRIPT_NAME'] = $config['script_name'];
 $_SERVER['REQUEST_URI'] = $config['query_string'] != '' ? $config['script_name'].'?'.$config['query_string'] : $config['script_name'];
 $_SERVER['QUERY_STRING'] = $config['query_string'];
 $_SERVER['REQUEST_METHOD'] = 'GET';
 $_SERVER['REMOTE_PORT'] = 10000;
 $_SERVER['SCRIPT_FILENAME'] = '/home/www/'.$config['base_domain'].'/www/'.$config['request_domain'].$config['script_name'];
 $_SERVER['CONTEXT_DOCUMENT_ROOT'] = '/home/www/'.$config['base_domain'].'/www/'.$config['request_domain'].'/';
 $_SERVER['DOCUMENT_ROOT'] = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
 $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
 $_SERVER['SERVER_ADDR'] = '127.0.0.1';
 $_SERVER['SERVER_NAME'] = $config['request_domain'];
 $_SERVER['HTTP_HOST'] = $config['request_domain'];
 $_SERVER['SERVER_SOFTWARE'] = 'Apache/2.4.10 (Debian)';
 $_SERVER['PHP_SELF'] = $config['script_name'];
 $_SERVER['REQUEST_SCHEME'] = $config['request_scheme'];

 foreach (explode('&', $config['query_string']) as $query_item) {
  list($key, $value) = explode('=', $query_item);
  $_GET[$key] = $value;
 }

 chdir(dirname($_SERVER['SCRIPT_FILENAME']));
 require $_SERVER['SCRIPT_FILENAME'];
?>
