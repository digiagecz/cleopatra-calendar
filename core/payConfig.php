<?php

///Require TpMerchantConfig
require_once ('../../../vendor/thepay/TpMerchantConfig.php');

///Class with our thepay configuration, it extends TpMerchantConfig
class PayConfig extends TpMerchantConfig {

	///Our id
	public $merchantId = PAY_MERCHANT_ID;

	///Our account id
	public $accountId = PAY_ACCOUNT_ID;

	///Our password
	public $password = PAY_PASSWORD;

	///Url of thepay gate
	public $gateUrl = PAY_GATE_URL;

}
