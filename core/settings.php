<?php
define('DEMO_IS_ON' , true, true);
define('DEMO_LENGTH' , 30, true);

define('MIN_MENSES_LENGTH', 3, true);
define('MAX_MENSES_LENGTH', 8, true);
define('MIN_CYCLE_LENGTH', 24, true);
define('MAX_CYCLE_LENGTH', 35, true);

define('MIN_CYCLES_TO_CALCULATE', 3, true);

define('HOST', strtolower($_SERVER['HTTP_HOST']), true);

if (HOST == "localhost") { // EDIT
  define('HTTPMODE', 'http', true);
  define('HOME', HTTPMODE . '://' . HOST . '/cleopatra-calendar-test', true);
}
else if (HOST == "cleopatra-calendar.eu") { // TEST
  define('HTTPMODE', 'https', true);
  define('HOME', HTTPMODE . '://' . HOST, true);
}
else { // PRODUCTION
  define('HTTPMODE', 'https', true);
  define('HOME', HTTPMODE . '://' . HOST, true);
}

define('APPHOME', HOME . '/app/', true);

// CORE
define('CORE', HOME . '/core', true);

// PAGES
define('AGREEMENT_PAGE', HOME . '/app/pages/agreement', true);
define('LICENSING_PAGE', HOME . '/app/pages/licensing', true);
define('INFO_PAGE', HOME . '/app/pages/info', true);
define('TERMS_PAGE', HOME . '/app/pages/terms', true);
define('ADMIN_PAGE', HOME . '/admin/overview', true);

// SUBPAGES
define('PARTNERI_SUBPAGE', '/#partneri', true);
define('VYHODY_SUBPAGE', '/#vyhody', true);

// VIDEOS
define('PROMENLINA_VIDEO_LINK','w7wurtInopQ',true);
define('PROMENLINA_ZENA_VIDEO', 'https://www.youtube.com/watch?v='.PROMENLINA_VIDEO_LINK, true);
define('PROMENLINA_ZENA_VIDEO_EMBED', 'https://www.youtube.com/embed/'.PROMENLINA_VIDEO_LINK, true);     //embedded
define('NAVOD_VIDEO', 'https://www.youtube.com/watch?v=LvoQ_z_teF0', true);

// SOCIAL
define('FACEBOOK', 'https://www.facebook.com/Cleopatra.kalendar', true);
define('INSTAGRAM', 'https://www.instagram.com/cleopatracalendar/', true);

// FEEDBACK
define('FEEDBACK_FORM','https://docs.google.com/forms/d/e/1FAIpQLScxz5BssvGjxuGDDgF8orWmxkR5y1WL_W0wUsjaLccWwZ243g/viewform', true);


// PASSAGES
define('LOGOUT', HOME . '/app/passages/logout/', true);
define('REMOVE_ALL_PAGE', HOME . '/app/passages/remove', true);
define('PAYMENT', HOME . '/app/passages/payment', true);

// GOOGLE SETTINGS
define('GOOGLE_CREATE', HOME . '/app/google/create/', true);
define('GOOGLE_LOGIN', HOME . '/app/google/login/', true);
define('GOOGLE_ADD', HOME . '/app/google/add/', true);
define('GOOGLE_CALENDAR', HOME . '/app/google/calendar/', true);
define('GOOGLE_SCOPE', 'https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/userinfo.email');

//define('EMAIL_ADMIN', HOME . 'martin@krotil.info', true);
define('EMAIL_ADMIN', 'info@digiage.cz', true);
define('EMAIL_INFO', 'info@cleopatra-calendar.com', true);
define('EMAIL_LIDA', 'hobzova@cleopatra-calendar.com', true);

if (HOST == "localhost") { // EDIT
  define('ENVIRONMENT', 'localhost');
  define('DB_SERVER', 'localhost');
  define('DB_NAME', 'cleopatra_calendar', true);
  define('DB_USER', 'cleopatra_calendar', true);
  define('DB_PASSWORD', 'cleopsswd', true);
  define('DEBUGGING', true, true);
  define('GOOGLE_CLIENT_SECRET_FILENAME', 'client_secret_test_eu.json', true);

  // PAY CONFIG
  define('PAY_MERCHANT_ID', 1, true);
  define('PAY_ACCOUNT_ID', 1, true);
  define('PAY_PASSWORD', 'my$up3rsecr3tp4$$word', true);
  define('PAY_GATE_URL', "https://www.thepay.cz/demo-gate/", true);
}
else if (HOST == "cleopatra-calendar.eu") { // TEST
  define('ENVIRONMENT', 'test');
  define('DB_SERVER', 'md110.wedos.net');
  define('DB_NAME', 'd275388_cletest', true);
  define('DB_USER', 'w275388_cletest', true);
  define('DB_PASSWORD', 'P1!e50biE-N', true);
  define('DEBUGGING', true, true);
  define('GOOGLE_CLIENT_SECRET_FILENAME', 'client_secret_test_eu.json', true);

  // PAY CONFIG
  define('PAY_MERCHANT_ID', 1, true);
  define('PAY_ACCOUNT_ID', 1, true);
  define('PAY_PASSWORD', 'my$up3rsecr3tp4$$word', true);
  define('PAY_GATE_URL', "https://www.thepay.cz/demo-gate/", true);
}
else { // PRODUCTION
  define('ENVIRONMENT', 'production');
  define('DB_SERVER', '127.0.0.1');
  define('DB_NAME', 'cleopatra-calendarcom', true);
  define('DB_USER', 'cleopatra-cal001', true);
  define('DB_PASSWORD', 'CnxFk8aG', true);
  define('DEBUGGING', false, true);
  define('GOOGLE_CLIENT_SECRET_FILENAME', 'client_secret_production.json', true);

  // PAY CONFIG
  define('PAY_MERCHANT_ID', 2875, true);
  define('PAY_ACCOUNT_ID', 3797, true);
  define('PAY_PASSWORD', 'fe6d3dfc85c64', true);
  define('PAY_GATE_URL', "https://www.thepay.cz/gate/", true);
}

//price
define('PAYMENT_NORMAL', 800, true);
//define('PAYMENT_SALE', 500, true);
define('PAYMENT_SALE_YEARS', array(0,10,15,0,0,0,0,0,0,0), true);		// for more years (zeros), so it will not fail if the PAYMENT_QUANTITY_MAX gets higher
define('PAYMENT_SALE_COUPON', array(37.5,30,30,0,0,0,0,0,0), true);		// ...

define('PAYMENT_QUANTITY_MIN', 1, true);
define('PAYMENT_QUANTITY_MAX', 3, true);
define('PAYMENT_DAYS_PAID', 365, true);



if ( DEBUGGING ) {
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
}

function cleoRedirect($destination, $messageNumber = null, $message = null) {
  if ( $messageNumber !== null ) {
    if (strpos($destination, '?' ) === false) {
      $destination = $destination . '?message=' . $messageNumber;
    }
    else {
      $destination = $destination . '&message=' . $messageNumber;
    }

    if ( DEBUGGING ) {
      header('Content-Type: text/html; charset=utf-8');
      echo '<p><strong>Chyba:</strong> ' . $message . '<br /><strong>Přesměrovávám:</strong> <a href="' . $destination . '">' . $destination . '</a></p>';
    }
    else {
      header('Location: ' . filter_var($destination, FILTER_SANITIZE_URL));
    }
  }
  else {
    header('Location: ' . filter_var($destination, FILTER_SANITIZE_URL));
  }
}

function getMainHeader($title = 'Cleopatra - Cyklický kalendář online', $depth = 0, $google) {
  switch ($depth) {
    case 0:
      $depthPrefix = '.';
    break;
    case 1:
      $depthPrefix = '..';
    break;
    case 2:
      $depthPrefix = '../..';
    break;
    case 3:
      $depthPrefix = '../../..';
    break;
  }
  ?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Google kalendář pro ženy co plánují s lehkostí a přehledem. Chceš plánovat v souladu se svými cykly? Plánuj s Cleopatrou - cyklickým kalendářem online.">

    <title><?php echo $title;?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $depthPrefix;?>/creative/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery UI -->
    <link href="<?php echo $depthPrefix;?>/css/jquery-ui.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo $depthPrefix;?>/creative/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,700' rel='stylesheet' type='text/css'>
<!--     <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->

    <!-- Plugin CSS -->
    <link href="<?php echo $depthPrefix;?>/creative/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $depthPrefix;?>/css/creative.min.css?v=2.0" rel="stylesheet">

    <link rel="icon" href="<?php echo $depthPrefix;?>/img/favicon.png">

    <?php
    if (ENVIRONMENT == 'production') {?>
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-5QH5H77');</script>
      <!-- End Google Tag Manager -->
    <?php
    }
    ?>

  </head>

  <body id="page-top" class="js--services<?php if ($google) { echo ' js--google';} ?>">
<?php
}

function getMainFooter($depth = 0, $appLibs) {
  switch ($depth) {
    case 0:
      $depthPrefix = '.';
    break;
    case 1:
      $depthPrefix = '..';
    break;
    case 2:
      $depthPrefix = '../..';
    break;
    case 3:
      $depthPrefix = '../../..';
    break;
  }
?>
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $depthPrefix;?>/creative/jquery/jquery.min.js"></script>
    <script src="<?php echo $depthPrefix;?>/creative/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $depthPrefix;?>/creative/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo $depthPrefix;?>/creative/scrollreveal/scrollreveal.min.js"></script>
    <script src="<?php echo $depthPrefix;?>/creative/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $depthPrefix;?>/js/creative.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo $depthPrefix;?>/js/jquery-ui.js"></script>

    <script src="<?php echo $depthPrefix;?>/js/env.js"></script>
    <script src="<?php echo $depthPrefix;?>/js/notice.js"></script>
    <?php if ($appLibs) { ?>
    <!-- Cleo lib -->
    <script src="<?php echo $depthPrefix;?>/js/appCore.js"></script>
    <?php } ?>

    <!-- Burger menu -->

    
<script>
  $( document ).ready(function() {


    //menu jako kopie normalniho
    $('.js-clone-nav').each(function() {
      var $this = $(this);
      $this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
    });

    //kliknutí na hamburger
    $('body').on('click', '.js-menu-toggle', function(e) {
      var $this = $(this);
      e.preventDefault();

      if ( $('body').hasClass('offcanvas-menu') ) {
        $('body').removeClass('offcanvas-menu');
        $('body').find('.js-menu-toggle').removeClass('active');
      } else {
        $('body').addClass('offcanvas-menu');
        $('body').find('.js-menu-toggle').addClass('active');
      }
    }); 

    //kliknutí na položku menu
    $('.site-mobile-menu a').click(function(){
      $('body').removeClass('offcanvas-menu');
      $('body').find('.js-menu-toggle').removeClass('active');
    });

    // kliknutí mimo mobilní menu
    $(document).mouseup(function(e) {
    var container = $(".site-mobile-menu");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      if ( $('body').hasClass('offcanvas-menu') ) {
          $('body').removeClass('offcanvas-menu');
          $('body').find('.js-menu-toggle').removeClass('active');
        }
    }
    });


    });
</script>


    </body>
</html>
<?php
}
?>
