<?php

if ( ($cUsers->isUserLoggedIn()) && ($cUsers->isConsentGiven()) ) {
  $isUserLoggedIn = true;
} else {
  $isUserLoggedIn = false;
}

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$actual_link = explode('?', $actual_link)[0];
//echo "aktuální: " . $actual_link . ", hodnota HOME: " . HOME . ", hodnota APPHOME: " . APPHOME;
//echo "posledni znak: " . $actual_link[strlen($actual_link)-1];

?>

<!-- Navigation -->


    <?php
    $lAnchClass = "";
    $lAnchHref = "";

    if ($isUserLoggedIn) { // USER IS LOGGED IN
      if ($actual_link == HOME) {
        $lAnchClass = " js-scroll-trigger";
        $lAnchHref = "#page-top";
      } else {
        // ANCHOR TO HOME
        $lAnchClass = "";
        $lAnchHref = HOME;
      }
    } else { // USER IS NOT LOGGED IN
      if ($actual_link == HOME) {
        $lAnchClass = " js-scroll-trigger";
        $lAnchHref = "#page-top";
      } else {
        // ANCHOR TO HOME
        $lAnchClass = "";
        $lAnchHref = HOME;
      }
    } ?>


<!---------------------- NAVIGACE ------------------->
    
<div class="site-mobile-menu site-navbar-target">
  <div class="site-mobile-menu-header">
	<div class="site-mobile-menu-close">
	  <span class="icofont-close js-menu-toggle">x</span>
	</div>
  </div>
  <div class="site-mobile-menu-body"></div>
</div>
<div class="site-navbar js-sticky-header site-navbar-target" role="banner" id="shrinkable">
  <nav class="site-navigation position-relative" role="navigation" id="mainNav">
    <div class="container">
      <div class="row align-items-center">
      <div class="col-10 col-xl-2">
        <a class="navbar-brand<?php echo $lAnchClass;?>" href="<?php echo $lAnchHref;?>">
          <img src="/img/logo.svg" class="logotyp" alt="Logotyp Cleopatra kalendáře" />
          <img src="/img/cleopatra.svg" class="logo-text" alt="Logo text Cleopatra kalendáře" />
        </a>
      </div>
      <div class="col-xl-10 d-none d-xl-block text-right">
        
        <ul class="cleo-menu site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
          <li class="cleo-menu__item ">
           <a href="/#jak-to-funguje" class="js--notice">Jak to funguje</a>
          </li>
          <li class="cleo-menu__item ">
            <a href="<?php echo INFO_PAGE;?>" class="js--notice">Časté otázky</a>
          </li>
          <li class="cleo-menu__item ">
            <a href="/#cyklicka-zena" class="js--notice">Cyklická žena</a>
          </li>
          <li class="cleo-menu__item ">
            <a href="/#partneri" class="js--notice">Partneři</a>
          </li>

        <?php if ($isUserLoggedIn) { 

        if ($actual_link != LICENSING_PAGE . '/') { ?>
          <li class="cleo-menu__item">
            <a href="<?php echo LICENSING_PAGE;?>" data-tooltip="true" data-tooltip-content="Prodloužit platnost Cleopatry"><span class="cleo-icon is--cart"></span>&nbsp;&nbsp;Zakoupit</a>
          </li>
        <?php } ?>
          <li class="cleo-menu__item">
            <a href="<?php echo LOGOUT;?>" data-tooltip="true" data-tooltip-content="Odhlásit se"><span class="cleo-icon is--exit"></span>&nbsp;&nbsp;Odhlásit</a>
          </li>
        <?php  if ($actual_link != APPHOME) { ?>
          <li class="cleo-menu__item pb-0 pt-0">
          <a href="<?php echo APPHOME; ?>" class="js--notice btn btn-main" data-tooltip-content="Uživatelský účet" data-tooltip="true"><span class="cleo-icon is--user"></span>&nbsp;&nbsp;Tvůj účet</a>
          </li>
        <?php } 

        }else{ ?>

          <li class="cleo-menu__item">
          <a href="<?php echo GOOGLE_LOGIN; ?>" class="js--notice">Přihlásit se</a>
          </li>
          <li class="cleo-menu__item pb-0 pt-0">
          <a href="<?php echo GOOGLE_LOGIN; ?>" class="js--notice btn btn-main btn-main-shine">Vyzkoušet</a>
          </li>

        <?php } ?>

        </ul>
        
        
      </div>
      <div class="col-2 d-inline-block d-xl-none ml-md-0 py-3 menu-burger text-right">
        <a href="#" class="burger site-menu-toggle js-menu-toggle" data-toggle="collapse" aria-label="menu" data-target="#main-navbar">
        <span></span>
        </a>
      </div>
      </div>
    </div>
  </nav>
</div>





<?php //echo "actual_link:".$actual_link."APPHOME:".APPHOME; ?>
<script type="text/javascript">
  // TOOLTIPS
  function cleoTooltips() {
    $( document ).tooltip({
      items: "[data-tooltip]",
      track: true,
      content: function() {
        return $( this ).attr( "data-tooltip-content" )
      }
    });
  }

  if (document.readyState!='loading') cleoTooltips();
  else if (document.addEventListener) document.addEventListener('DOMContentLoaded', cleoTooltips);
  else document.attachEvent('onreadystatechange', function() {
    if (document.readyState=='complete') cleoTooltips();
  });
</script>
