  <section class="cleo-section is--main">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">

          <div class="container">
            <div class="row">
              <div class="col-lg-6">

                <ul class="list-unstyled">
                  <li><a href="<?php echo FEEDBACK_FORM; ?>" title="Odkaz na Cleopatra help" target="_blank">Zpětná vazba</a></li>
                  <li><a href="<?php echo TERMS_PAGE;?>" title="Odkaz na Obchodní podmínky">Obchodní podmínky</a></li>
                  <li><a href="<?php echo INFO_PAGE;?>" title="Odkaz na Často kladené otázky">Často kladené otázky</a></li>
                </ul>

              </div>
              <div class="col-lg-6">


                <p class="text-right social">
                  <a href="<?php echo FACEBOOK; ?>" title="Cleopatra na facebooku" target="_blank">
                    <img src="<?php echo HOME; ?>/img/fb.svg" alt="facebook ikona Cleopatry">
                  </a>
                  <a href="<?php echo INSTAGRAM; ?>" title="Cleopatra na Instagramu" target="_blank">
                    <img src="<?php echo HOME; ?>/img/ig.svg" alt="instagram ikona Cleopatry">
                  </a>
                  <br><br>
                  <a href="mailto:&#105;&#110;&#102;&#111;&#64;&#99;&#108;&#101;&#111;&#112;&#97;&#116;&#114;&#97;&#45;&#99;&#97;&#108;&#101;&#110;&#100;&#97;&#114;&#46;&#99;&#111;&#109;">&#105;&#110;&#102;&#111;&#64;&#99;&#108;&#101;&#111;&#112;&#97;&#116;&#114;&#97;&#45;&#99;&#97;&#108;&#101;&#110;&#100;&#97;&#114;&#46;&#99;&#111;&#109;</a>
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <a href="https://www.thepay.cz/" target="_blank"><img class="img-fluid" src="https://www.thepay.cz/gate/banner/?merchantId=2875&accountId=3797&skin=Horizontal&transparent=0&signature=8e1b2012404243f561d70693bd815a70" alt="ThePay – Platba kartou, Platba24, MojePlatba, eKonto, mPeníze, MONETA, ČSOB, Fio Banka, Equa Bank, superCASH, Sberbank, QR platba, Bitcoin" /></a>
        </div>
      </div>
      <div class="row mt-2">
        <div class="col-lg-8 mx-auto text-center">
          © 2018 - <?php echo date("Y"); ?> Cleopatra Calendar. <a href="https://digiage.cz/tvorba-webovych-stranek" target="_blank" rel="noopener">Webové stránky</a> vytvořilo a spravuje studio <a href="https://digiage.cz" target="_blank" rel="noopener">Digi Age</a>
        </div>
      </div>
    </div>
  </section>

  <!-- info boxes -->
  <div id="js-info-box" class="cleo-info">
    <div class="cleo-info__box">
      <span id="js-info-closer" class="cleo-icon is--cross cleo-info__closer"></span>
      <p id="js-info-content" class="cleo-info__content">

      </p>
    </div>
  </div>

  <!-- loading phase -->
  <div id="js-loading-phase" class="cleo-loading">
    <div class="cleo-loading__spinner">
      <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div>
    <div class="cleo-loading__textfield pulse">
      <p id="js-loading-text" class="cleo-loading__content"></p>
    </div>
  </div>
