<?php
require_once(__DIR__ . '/cleo_users.php');

class cleoStatistics extends cleoUsers {
  public function getUsersActivity() {
    if ($this->openDB()) {

      $sql = "SELECT TUsers.ID AS userID,TUsers.Email AS userEmail, TLogs.Time AS logTime
              FROM TUsers
              INNER JOIN TLogs ON TUsers.ID = TLogs.Owner
              WHERE TLogs.Type = 'state' AND TLogs.Description = 'events are generated'
              ORDER BY TUsers.ID, TLogs.Time";

      $result = $this->dbConn->query($sql);

      if ($result->num_rows > 0) {
        $usersDetails = [];
        $lastID = null;
        $lastLogTime = null;

        while ($row = mysqli_fetch_array($result)) {
          if ($lastID!=$row['userID']) {
            $usersDetails[$row['userID']]['id'] = $row['userID'];
            $usersDetails[$row['userID']]['email'] = $row['userEmail'];
            $usersDetails[$row['userID']]['num'] = 0;
            $usersDetails[$row['userID']]['total'] = 0;
            $lastID = $row['userID'];
            $lastLogTime = null;
          }

          if ($lastLogTime === null) {
            $usersDetails[$row['userID']]['num']++;
            $lastLogTime = $row['logTime'];
          } else {
            $dateTimeStart = new DateTime($lastLogTime);
            $dateTimeEnd = new DateTime($row['logTime']);
            $dateTimeDiff = $dateTimeStart->diff($dateTimeEnd);

            if ($dateTimeDiff->format('%d') > 0) {
              $usersDetails[$row['userID']]['num']++;
            }

            $lastLogTime = $row['logTime'];
          }
          $usersDetails[$row['userID']]['total']++;
        }


        return $usersDetails;
      }
    }
    return null;
  }

  public function getBasicStatistics() {
    if ($this->openDB()) {

      $sql = "SELECT TUsers.ID AS userID, TUsers.Email AS userEmail, TUsers.LatestEvent AS userLastEventDate, TSettings.Expirate AS userLicenceExpirateDate
              FROM TUsers
              INNER JOIN TSettings ON TUsers.ID = TSettings.Owner
              ORDER BY TUsers.ID";

      $result = $this->dbConn->query($sql);

      if ($result->num_rows > 0) {
        $basicStatistics = [];
        $basicStatistics['numberOfUsers'] = 0;
        $basicStatistics['hasValidLicense'] = 0;
        $basicStatistics['30daysActive'] = 0;

        while ($row = mysqli_fetch_array($result)) {
          $basicStatistics['numberOfUsers']++;

          $diff = floor((strtotime($row['userLicenceExpirateDate']) - strtotime("NOW")) / (60*60*24));
          if ($diff > 0) {
            $basicStatistics['hasValidLicense']++;
          }

          if ($row['userLastEventDate'] != NULL) {
            $diff = floor((strtotime("NOW") - strtotime($row['userLastEventDate'])) / (60*60*24));

            if ($diff < 31) {
              $basicStatistics['30daysActive']++;
            }
          }
        }
        
        return $basicStatistics;
      }
    }
    //return null;
  }
  public function getPaymentStatistics($orderBy) {
    if ($this->openDB()) {

      $sql = "SELECT TUsers.ID AS userID, TUsers.Email AS userEmail, IFNULL(TUsers.LatestEvent,'---') AS userLastEventDate, TUsers.Name AS userName, IFNULL(TPayments.PaymentId,'---') AS paymentId, IFNULL(TPayments.Time,'---') AS paymentTime, TSettings.Expirate AS userLicenseExpirateDate, IFNULL(TPayments.coupon,'---') AS paymentCoupon
              FROM TUsers
              LEFT JOIN TSettings ON TUsers.ID = TSettings.Owner
              LEFT JOIN TPayments ON TUsers.ID = TPayments.Owner
              ORDER BY ".$orderBy;

      $result = $this->dbConn->query($sql);

      if ($result->num_rows > 0) {
/*         $paymentStatistics = [];

        while ($row = mysqli_fetch_array($result)) {

          $diff = floor((strtotime($row['userLicenceExpirateDate']) - strtotime("NOW")) / (60*60*24));
          if ($diff > 0) {
            $paymentStatistics['hasValidLicense']++;
          }

          if ($row['userLastEventDate'] != NULL) {
            $diff = floor((strtotime("NOW") - strtotime($row['userLastEventDate'])) / (60*60*24));

            if ($diff < 31) {
              $paymentStatistics['30daysActive']++;
            }
          }
        } */
        
        return $result;
      }
    }
    //return null;
  }

  public function getLastMonthPartnersCouponsTotals() {
    if ($this->openDB()) {
      //na strance statistik a do emailu každej měsíc - cron
      //vybrat předchozí měsíc z TPayments -> Time
      $sql = 'SELECT MONTH(CURRENT_DATE() - INTERVAL 1 MONTH) as mm, TPayments.coupon, TPartners.name, TPartners.email, TPartners.account, COUNT(*) 
              FROM TPayments 
              LEFT JOIN TPartners ON TPayments.coupon = TPartners.coupon 
              WHERE MONTH(TPayments.Time) = MONTH(CURRENT_DATE()  - INTERVAL 1 MONTH)
              GROUP BY TPayments.coupon';

      $result = $this->dbConn->query($sql);

      if ($result && $result->num_rows >= 1) {
        $lastMonth = date("F", strtotime("first day of previous month"));
        $withCoupon = 0;
        $noCoupon = 0;
        
        $table = '<table><tr><th>Za měsíc</th><th>Použitý kód</th><th>Jméno Partnera</th><th>Email</th><th>Č. účtu</th><th>Počet použití</th></tr>';

        while($row = $result->fetch_assoc()){
          if($row['coupon'] != ''){
            $table .= '<tr><td>'.$row['mm'].'</td><td>'.$row['coupon'].'</td><td>'.$row['name'].'</td><td>'.$row['email'].'</td><td>'.$row['account'].'</td><td>'.$row['COUNT(*)'].'</td></tr>';
            $withCoupon += $row['COUNT(*)'];
          }else $noCoupon += $row['COUNT(*)'];
        }
        $table .= '</table>';

        $heading = '<h1>Použití kuponů za měsíc '.$lastMonth.'</h1>';
        $heading .= 'Dobré zprávy. Za minulý měsíc proběhlo <strong>'.($withCoupon + $noCoupon).'</strong> plateb. <br> Z toho <strong>'.$withCoupon.'</strong> krát s použitím kuponu a <strong>'.$noCoupon.'</strong> krát bez.<br><br>';

        return $heading.$table;     //mozna return table, kdyz uz bude vytvorena
      }
    }
    return false;
  }
}

?>
