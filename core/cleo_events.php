<?php
class cleoEvents {
  public $dbConn = null;

  public function __construct() {
      $this->dbConn = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_NAME);
      if ($this->dbConn->connect_error) {
        echo '<p class="error">Connection failed: ' . $this->dbConn->connect_error . '</p>';
      }
      mysqli_set_charset($this->dbConn,'utf8');
  }

  public function isSendedDataValid($start, $length) {
    $today = new DateTime('NOW');
    $startTime = DateTime::createFromFormat('Y-m-d', $start);

    if ($startTime > $today) {
      return false;
    }

    if (($length < 3) || ($length > 8)) {
      return false;
    }

    return true;
  }

  public function updateLatestEvent($id) {
    if ( ($this->dbConn !== null) &&  isset($id) ) {
      // latest event
      $sql = 'SELECT TEvents.Start
              FROM TEvents
              WHERE Owner="' . $id . '"
              ORDER BY TEvents.Start DESC
              LIMIT 1';

      $result = $this->dbConn->query($sql);
      $row = $result->fetch_assoc();
      $latestEventDate = $row['Start'];

      $sql = 'UPDATE TUsers SET
              LatestEvent 	="' . $latestEventDate . '"
              WHERE ID="' . $id . '";';

      $this->dbConn->query($sql);
    }
  }

  public function doSomethingWithEvent($ownerID, $sql) {
    if ($this->dbConn !== null && (isset($sql))) {
      if ( ($this->dbConn->query($sql) === true) ) {
        $this->updateLatestEvent($ownerID);
        $this->countStats($ownerID);
        return true;
      }

      if (DEBUGGING) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
      }
    }
    return false;
  }

  public function newEvent($ownerID, $start, $length) {
    if ( (isset($ownerID, $start, $length)) && ($this->isSendedDataValid($start, $length)) ) {
      $sql = 'INSERT INTO TEvents (Owner, Start, Length)
      VALUES ("' . $ownerID . '",
              "' . $start . '",
              "' . $length . '");';
      return $this->doSomethingWithEvent($ownerID, $sql);
    }
    return false;
  }

  public function deleteEvent($ownerID, $eventID) {
    if ( isset($ownerID, $eventID) ) {
      $sql = 'DELETE FROM TEvents WHERE ID="'. mysqli_real_escape_string($this->dbConn,$eventID) . '" AND Owner="' . mysqli_real_escape_string($this->dbConn,$ownerID) . '"';
      return $this->doSomethingWithEvent($ownerID, $sql);
    }
    return false;
  }

  public function updateEvent($ownerID, $eventID, $start, $length) {
    if ( (isset($ownerID, $eventID, $start, $length)) && ($this->isSendedDataValid($start, $length)) ) {
      $sql = 'UPDATE TEvents
              SET Start="' . mysqli_real_escape_string($this->dbConn,$start) . '", Length="' . mysqli_real_escape_string($this->dbConn,$length) .'"
              WHERE ID="' . $eventID . '" AND Owner="' . $ownerID . '"';
      return $this->doSomethingWithEvent($ownerID, $sql);
    }
    return false;
  }

  public function dateDiff($from, $to) {
    $datediff = strtotime($from) - strtotime($to);
    return abs(round($datediff / (60 * 60 * 24)));
  }

  public function getEvents($ownerID) {
    if ( ($this->dbConn !== null) &&  isset($ownerID) ) {
      $sql = 'SELECT * FROM TEvents WHERE Owner="' . $ownerID . '" ORDER BY Start DESC'; // ORDER BY IMPORTANT
      $result = $this->dbConn->query($sql);

      if ( $result ) {
        if ( $result->num_rows > 0 ) {
          return $result;
        }
      }
    }
    return false;
  }

  public function getLastEventStart($ownerID) {
    if ( ($this->dbConn !== null) &&  isset($ownerID) ) {
      $sql = 'SELECT Start FROM TEvents WHERE Owner="' . $ownerID . '" ORDER BY Start DESC'; // ORDER BY IMPORTANT
      $result = $this->dbConn->query($sql);

      if ( $result->num_rows > 0 ) {
        $row = $result->fetch_assoc();
        return $row['Start'];
      }
    }
    return false;
  }

  public function deleteAllEvents($ownerID) {
    if ( isset($ownerID) ) {
      $sql = 'DELETE FROM TEvents WHERE Owner="'. mysqli_real_escape_string($this->dbConn,$ownerID) . '"';
      return $this->doSomethingWithEvent($ownerID, $sql);
    }
    return false;
  }

  public function deleteAllFutureEvents($ownerID) {
    if ( isset($ownerID) ) {
      $sql = 'DELETE FROM TFutureEvents WHERE Owner="'. mysqli_real_escape_string($this->dbConn,$ownerID) . '"';
      return $this->doSomethingWithEvent($ownerID, $sql);
    }
    return false;
  }

  // STATS PART

  public function isPregnant($ownerID) {
    if ( ($this->dbConn !== null) &&  isset($ownerID) ) {
      $sql = 'SELECT Pregnancy FROM TSettings WHERE Owner="' . $ownerID . '"';
      $result = $this->dbConn->query($sql);

      if ( ($result) && ($result->num_rows > 0) ) {
        $row = $result->fetch_assoc();
        if ($row['Pregnancy'] == 1) {
          return true;
        }
      }
    }
    return false;
  }

  public function getDefaultStat($stat) {
    switch ($stat) {
      case 'cycle':
        return 28;
      break;
      case 'period':
        return 4;
      break;
    }
  }

  public function getStats($ownerID) {
    if ( ($this->dbConn !== null) &&  isset($ownerID) ) {

      $sql = 'SELECT * FROM TSettings WHERE Owner="' . $ownerID . '"';
      $result = $this->dbConn->query($sql);

      if ( $result ) {
        if ( $result->num_rows > 0 ) {
          return $result;
        }
      }
    }
    return false;
  }

  public function setStats($ownerID, $autoMensesCount, $mensesLength, $autoCycleCount, $cycleLength, $pregnancy) {
    if ( ($this->dbConn !== null) &&  isset($ownerID, $autoMensesCount, $mensesLength, $autoCycleCount, $cycleLength, $pregnancy) ) {

      if ( (($autoMensesCount == 1) || ($autoMensesCount == 0)) && (($autoCycleCount == 1) || ($autoCycleCount == 0)) && (($pregnancy == 1) || ($pregnancy == 0)) ) {
        if ($mensesLength < MIN_MENSES_LENGTH) { $mensesLength = MIN_MENSES_LENGTH; }
        elseif ($mensesLength > MAX_MENSES_LENGTH) { $mensesLength = MAX_MENSES_LENGTH; }

        if ($cycleLength < MIN_CYCLE_LENGTH) { $cycleLength = MIN_CYCLE_LENGTH; }
        elseif ($cycleLength > MAX_CYCLE_LENGTH) { $cycleLength = MAX_CYCLE_LENGTH; }

        if (($mensesLength >= MIN_MENSES_LENGTH) && ($mensesLength <= MAX_MENSES_LENGTH) && ($cycleLength >= MIN_CYCLE_LENGTH) && ($cycleLength <= MAX_CYCLE_LENGTH)) {

          // Nastavují se validní hodnoty
          $sql = 'UPDATE TSettings SET
                  AutoPeriod="' . $autoMensesCount . '",
                  LengthPeriod 	="' . $mensesLength . '",
                  AutoCycle="' . $autoCycleCount . '",
                  LengthCycle="' . $cycleLength .'",
                  Pregnancy="' . $pregnancy . '"
                  WHERE Owner="' . $ownerID . '";';

          if ( ($this->dbConn->query($sql) === true) ) {
            return true;
          }
          elseif ( DEBUGGING ) {
            echo '<p class="error">SQL exeption: ' . $this->dbConn->error . ',<br /> SQL: ' . $sql . '</p>';
          }

        }
      }
    }
    return false;
  }

  public function countStats($ownerID) {
    if ( ($this->dbConn !== null) &&  isset($ownerID) ) {
      $skipCountingStatistic = false;

      // Nejdříve se podíváme, zda nemá uživatel ručně nastavené statistiky
      $sql = 'SELECT * FROM TSettings WHERE Owner="' . $ownerID . '"';
      $result = $this->dbConn->query($sql);
      //if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $autoPeriod = $row['AutoPeriod'];
        $lengthPeriod = $row['LengthPeriod'];
        $autoCycle = $row['AutoCycle'];
        $lengthCycle = $row['LengthCycle'];

        if (($autoPeriod == 0) && ($autoCycle == 0)) {
          $skipCountingStatistic = true;
        }
      //}



      if ($skipCountingStatistic === false) {
        $sql = 'SELECT *  FROM TEvents WHERE Owner="' . $ownerID . '" ORDER BY Start DESC';  // ORDER BY IMPORTANT - seřazeno podle data startu sestupně
        $result = $this->dbConn->query($sql);

        $count = 0;
        $events = [];

        while ($row = $result->fetch_assoc()) {
          $events['start'][$count] = $row['Start'];
          $events['length'][$count] = $row['Length'];
          $count++;
        }

        if ($count > 0) {

          $cycleSUM = 0;
          $periodSUM = 0;

          // Provedeme součet dní všech period a cyklů (u cyklu vyjma nejnovějšího cyklu)
          for ($i = 0; $i < $count; $i++) {
            if ($i > 0) {
              // Do součtu dní cyklů ignorujeme ten nejnovější
              $cycleSUM = $cycleSUM + $this->dateDiff($events['start'][$i], $events['start'][$i - 1]);
            }
            $periodSUM = $periodSUM + $events['length'][$i];
          }

          if ($count == 1) {
            // Uživatel má pouze jednu událost, pak průmernou délku cyklu nastavuji na default
            $cycleAverageNumber = $this->getDefaultStat('cycle');
          }
          else {
            // Uživatel má více jak dvě a více událostí, vypočítám průměrnou délku cyklu (nejnovější událost se do výpočtu nezapočítává)
            $cycleAverageNumber = round(($cycleSUM / ($count - 1)));
          }

          // Uživatel má jednu a více událostí, vypočítávám délku periody
          $periodAverageNumber = round($periodSUM / $count);
        }
        else {
          // Uživtel v DB nemá žádnou událost, nastavuji default obou hodnot
          $cycleAverageNumber = $this->getDefaultStat('cycle');
          $periodAverageNumber = $this->getDefaultStat('period');
        }

      }

      if ($autoPeriod == 0) {
        $periodAverageNumber = $lengthPeriod;
      }

      if ($autoCycle == 0) {
        $cycleAverageNumber = $lengthCycle;
      }

      // direktivní nastavení maximálních a minimálních hodnot aplikace
      if ($periodAverageNumber < MIN_MENSES_LENGTH) { $periodAverageNumber = MIN_MENSES_LENGTH; }
      else if ($periodAverageNumber > MAX_MENSES_LENGTH) { $periodAverageNumber = MAX_MENSES_LENGTH; }
      if ($cycleAverageNumber < MIN_CYCLE_LENGTH) { $cycleAverageNumber = MIN_CYCLE_LENGTH; }
      else if ($cycleAverageNumber > MAX_CYCLE_LENGTH) { $cycleAverageNumber = MAX_CYCLE_LENGTH; }

      // Uložíme hodnoty do nastavení
      if ( isset($cycleAverageNumber, $periodAverageNumber) ) {
        $sql = 'UPDATE TSettings SET
                LengthPeriod 	="' . $periodAverageNumber . '",
                LengthCycle="' . $cycleAverageNumber .'"
                WHERE Owner="' . $ownerID . '";';

        if ( ($this->dbConn->query($sql) === true) ) {
          return true;
        }
        elseif ( DEBUGGING ) {
          echo '<p class="error">SQL exeption: ' . $this->dbConn->error . ',<br /> SQL: ' . $sql . '</p>';
        }
      }
    }
    return false;
  }

  // FUTURE EVENTS PART

  public function addDaysToDate($date, $days) {
    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);
  }

  public function createFutureEvent($ownerID, $type, $start, $end) {
    $sql = 'INSERT INTO TFutureEvents (Owner, Type, Start, End)
    VALUES ("' . $ownerID . '",
            "' . mysqli_real_escape_string($this->dbConn,$type) . '",
            "' . $start . '",
            "' . $end . '");';
    return $this->doSomethingWithEvent($ownerID, $sql);
  }

  public function getIntervalsLengths($mensesLength, $cycleLength) {

    //echo "mensesLength: " . $mensesLength . ", cycleLength " . $cycleLength . "<br />";

    if (($mensesLength > 2) && ($mensesLength < 9) && ($cycleLength > 23) && ($cycleLength < 36)) {
      $dataArray[24] = [$mensesLength,11 - $mensesLength,7,6];
      $dataArray[25] = [$mensesLength,11 - $mensesLength,7,7];
      $dataArray[26] = [$mensesLength,12 - $mensesLength,7,7];
      $dataArray[27] = [$mensesLength,12 - $mensesLength,8,8];
      $dataArray[28] = [$mensesLength,12 - $mensesLength,8,8];
      $dataArray[29] = [$mensesLength,13 - $mensesLength,8,8];
      $dataArray[30] = [$mensesLength,14 - $mensesLength,8,8];
      $dataArray[31] = [$mensesLength,15 - $mensesLength,8,8];
      $dataArray[32] = [$mensesLength,15 - $mensesLength,8,8];
      $dataArray[33] = [$mensesLength,16 - $mensesLength,8,9];
      $dataArray[34] = [$mensesLength,16 - $mensesLength,8,10];
      $dataArray[35] = [$mensesLength,16 - $mensesLength,8,11];

      return $dataArray[$cycleLength];
    }

    return false;
  }

  public function isDateAfterExpiration($actualDate) {                        //zjištění překročení data expirace k určitému datu (ne ke dnešku)
    $stampdiff = strtotime($actualDate) - strtotime($_SESSION['expirate']);
    if($stampdiff >= 0){
      return true;
    }
    return false;
  }


  public function updateFutureEvents($ownerID) {
    // Pro generování budoucích eventů je potřeba alespoň jeden záznam o periodě a vypnutá hodnota Pregnancy

    if ( $this->countStats($ownerID) ) {
      $lastCycleStart = $this->getLastEventStart($ownerID);
      $statsData = $this->getStats($ownerID);

      $stats = $statsData->fetch_assoc();
      $avgLengthCycle = $stats['LengthCycle'];
      $avgLengthPeriod = $stats['LengthPeriod'];
      $pregnancy = $stats['Pregnancy'];


      if (( $lastCycleStart ) && ($pregnancy == 0)) {

        $sql = 'DELETE FROM TFutureEvents WHERE Owner=' . $ownerID;
        if ( ($this->doSomethingWithEvent($ownerID, $sql)) && ($avgLengthPeriod > 0) && ($avgLengthCycle > 0) ) {

          $intervalsLengths = $this->getIntervalsLengths($avgLengthPeriod, $avgLengthCycle);

          if ($intervalsLengths) {

            $newCycleStart = $this->addDaysToDate($lastCycleStart, 0);
            $newCycleEnd = $this->addDaysToDate($lastCycleStart, $avgLengthCycle);

            $intervals = [];
            
            $numberOfCyclesGenerated = 7; //počet generovaných cyklů
            $nextForCycleEndValue = $numberOfCyclesGenerated;  // (tady byla chybně 0) info pro další FOR kdy má skončit. Generuje se to na 6 měsíců ale max do data Expirace

            for ($i = 0; $i < $numberOfCyclesGenerated; $i++) {        //sedmička tady znamená na 6 měsíců dopředu? (7*28/30). dole je i druhý for cyklus

              $a = $newCycleStart;
              $b = $this->addDaysToDate($newCycleStart, $intervalsLengths[0]);
              $c = $this->addDaysToDate($newCycleStart, ($intervalsLengths[0] + $intervalsLengths[1]));
              $d = $this->addDaysToDate($newCycleStart, ($intervalsLengths[0] + $intervalsLengths[1] + $intervalsLengths[2]));
              $e = $this->addDaysToDate($newCycleEnd, 1);

              $intervals[$i]['start'] = $a;
              $intervals[$i]['end'] = $e;

              $intervals[$i]['reflective-start'] = $a;
              $intervals[$i]['reflective-end'] = $b;
              $intervals[$i]['dynamic-start'] = $b;
              $intervals[$i]['dynamic-end'] = $c;
              $intervals[$i]['expressive-start'] = $c;
              $intervals[$i]['expressive-end'] = $d;
              $intervals[$i]['creative-start'] = $d;
              $intervals[$i]['creative-end'] = $e;

              //echo 'Cyklus začíná: ' . $newCycleStart . ', a končí: ' . $newCycleEnd . '<br />';
              //echo 'Reflektivní fáze začíná: ' . $intervals[$i]['reflective-start'] . ' a končí: ' . $intervals[$i]['reflective-end'] . '<br />';
              //echo 'Dynamická fáze začíná: ' . $intervals[$i]['dynamic-start'] . ' a končí: ' . $intervals[$i]['dynamic-end'] . '<br />';
              //echo 'Expresivní fáze začíná: ' . $intervals[$i]['expressive-start'] . ' a končí: ' . $intervals[$i]['expressive-end'] . '<br />';
              //echo 'Creativní fáze začíná: ' . $intervals[$i]['creative-start'] . ' a končí: ' . $intervals[$i]['creative-end'] . '<br />';



              if($this->isDateAfterExpiration($newCycleEnd)){        //pokud je překročena doba expirace - ukončit tady generování/ jinak na těch 7 cyklů...
                $nextForCycleEndValue = $i+1;                        //... funkce je před novým $newCycleEnd aby byly vygenerovány ještě události nejméně do data expirace
                break;
              }

              $newCycleStart = $this->addDaysToDate($newCycleEnd, 1);
              $newCycleEnd = $this->addDaysToDate($newCycleEnd, $avgLengthCycle);
            }

            // Creating events in DB
            for ($i = 0; $i < $nextForCycleEndValue; $i++) {
              $this->createFutureEvent($ownerID, 'reflective', $intervals[$i]['reflective-start'], $intervals[$i]['reflective-end']);
              $this->createFutureEvent($ownerID, 'dynamic', $intervals[$i]['dynamic-start'], $intervals[$i]['dynamic-end']);
              $this->createFutureEvent($ownerID, 'expressive', $intervals[$i]['expressive-start'], $intervals[$i]['expressive-end']);
              $this->createFutureEvent($ownerID, 'creative', $intervals[$i]['creative-start'], $intervals[$i]['creative-end']);
            }

            return true;
          }
        }
      }
    }
    return false;
  }

  public function getFutureEvents($ownerID) {
    if ( ($this->dbConn !== null) &&  isset($ownerID) ) {
      $sql = 'SELECT * FROM TFutureEvents WHERE Owner="' . $ownerID . '"';
      $result = $this->dbConn->query($sql);

      if (( $result ) && ($result->num_rows > 0)) {
        return $result;
      }
    }
    return false;
  }
}


?>
