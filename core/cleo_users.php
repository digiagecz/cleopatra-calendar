<?php
class cleoUsers{
  public $dbConn = null;

  /* SESSION PART */

  public function __construct() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
  }

  public function destroyAll() {
    session_unset();
    session_destroy();
  }

  public function isConsentGiven() {
    if ( (isset($_SESSION['agreement'])) && ($_SESSION['agreement'] == 1) ) { return true; } return false;
  }

  public function hasValidLicense() {
    if (isset($_SESSION['expirate'])) {
      if (strtotime("-1day") < strtotime($_SESSION['expirate'])) {
        return true;
      }
    }
    return false;
  }

  public function isUserLoggedIn() {
    if (isset($_SESSION['login']) && ($_SESSION['login'] == true)) {
      if (isset($_SESSION['logged_in_time']) && (time() - $_SESSION['logged_in_time'] > 1800)) {
        $this->destroyAll();
        return false;
      }
      else {
        return true;
      }
    }
    return false;
  }

  public function whenLicenseExpires() {
    if (isset($_SESSION['expirate'])) {
      return date('j.n.Y', strtotime($_SESSION['expirate']));
    }
    return false;
  }

  public function isServiceLoggedIn($service) {
    if (($this->isUserLoggedIn()) && isset($_SESSION['services']) && (in_array($service, $_SESSION['services']))) { return true; } return false;
  }

  public function logOut($service) {
    if (($this->isUserLoggedIn()) && ($this->isServiceLoggedIn($service))) {
      $index = array_search($service, $_SESSION['services']);
      array_splice($_SESSION['services'], $index, 1);
      if (empty($_SESSION['services'])) {
        $this->destroyAll();
      }
      return true;
    }
    return false;
  }

  public function setUserInformation($key, $value) {
    if (isset($key) && (isset($value))) {
      $_SESSION[$key] = $value;
      return true;
    }
    return false;
  }

  public function getUserInformation($key) {
    if (isset($_SESSION[$key])) { return $_SESSION[$key]; }
    return false;
  }

  public function getRandomString($length) {
    $characters = '23456789abcdefghijkmnprstuvwxyz';        //nepoužívat ty které se pletou
    $char_len = strlen($characters);
    $string = '';    

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, $char_len-1)];
    }
    return $string; 

  }

/*   public function getCouponCode($length){
    $code = $this->getRandomString(5);    // XXXXX             /////coupon XXXXX000
    $partner = str_pad($partnerID, 3, "0", STR_PAD_LEFT);
    return $code.$partner; 
    return bin2hex(random_bytes(round($length/2)));
  }*/

  public function getTableStyles(){
    return '<style>tr{background-color: #ffe7e7;}tr:first-child{background-color: #b9d0f3;}th{padding:10px 10px;}td{padding:5px 10px;}tr:nth-child(2n){background-color:#fffade;}td:last-of-type{text-align:center}</style>';
  }
  
  public function formatEmailTable($content){
    return '<!DOCTYPE html><html lang="cs"><head><meta charset="UTF-8">'.$this->getTableStyles().'</head><body>'.$content.'</body></html>';
  }

  /* DATABASE PART */

  public function openDB() {
    if ($this->dbConn === null) {
      $this->dbConn = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_NAME);
      if ($this->dbConn->connect_error) {
        if ( DEBUGGING ) {
          echo '<p class="error">Connection failed: ' . $this->dbConn->connect_error . '</p>';
        }
        else {
          return false;
        }
      }

      mysqli_set_charset($this->dbConn,'utf8');
    }
    return true;
  }

  public function giveConsent($ID) {
    if ($this->openDB() && (isset($ID))) {
      $sql = 'UPDATE TSettings SET Agreement=1 WHERE Owner=' . $ID .';';

      if ($this->dbConn->query($sql) === true) {
        $_SESSION['agreement'] = 1;
        return true;
      }

      if (DEBUGGING) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
      }
    }
    return false;
  }


  public function getPayConfigFile($userID){
    //return require_once('../../../core/test_payConfig.php');
    //require_once('settings.php');
    //$this->log($userID,'test',CORE.'/test_payConfig.php');
    if($userID == '5' || $userID == '106'){                     // sem vložit všechny admin účty, které budou mít test bránu
      return require_once('../../../core/test_payConfig.php');         //testovací platební brána pro adminy
    }else{
      return require_once('../../../core/payConfig.php');              //pro kohokoliv jiného standardní brána
    }
  }

/***************** emails  *********************************/

  public function getEmailHeaders(){
      $headers  = "From: Cleopatra Calendar <".EMAIL_INFO."> \r\n";
      //$headers .= "Cc: ".$email." \r\n"; 
      //$headers .= "X-Sender: ".$vas_web." < ".$vas_email." >\r\n";
      //$headers .= "Reply-To: ".$vas_email." \r\n";				
      $headers .= "X-Mailer: PHP/" . phpversion();
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      $headers .= "Return-Path: ".EMAIL_INFO."\r\n"; // Return path for errors
      $headers .= "MIME-Version: 1.0\r\n"; 
      $headers .= "Content-Type: text/html; charset=utf-8\r\n";

      return $headers;
  }

  public function sendEmail($address,$subject,$content) {
    if ( (isset($address)) && (mail($address,$subject,$content,$this->getEmailHeaders())) ) {
      return true;
    }
    return false;
  }

  public function getEmailStyle() {
    $content = "<!doctype html><html>";
    $content .= "<head><meta charset='UTF-8'><meta content='width=device-width, initial-scale=1' name='viewport'><meta name='x-apple-disable-message-reformatting'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta content='telephone=no' name='format-detection'></head>";
    $content .= "<body style='background-color:#fcf2fc; text-align:center; width:100%; font-family: Arial;'><div style='width:700px; margin:20px auto; background-color:#fff; text-align:left;'>\r\n";
    $content .= "<div><img src='".HOME."/img/email-header-cleopatra.jpg' alt='Cleopatra Clanedar email header' style='width:100%;margin-bottom:0;'></div>\r\n";
    $content .= "<div style='padding:30px;'>\r\n";
    return $content;
    
  }



  public function sendPaymentConfirmationEmail($expiration,$name,$email){

      $subject = "Potvrzení o uhrazení předplatného kalendáře Cleopatra";

      $content = $this ->getEmailStyle();
      $content .= "<h1>Děkuji,</h1>\n"; //<strong>" . $name . "</strong>
      $content .= "<p>za uhrazení ročního předplatného kalendáře Cleopatra. Nyní máš předplaceno do ".date('j.n.Y', strtotime($expiration)).". Věřím, že Ti usnadní plánování v souladu s jednotlivými fázemi Tvého cyklu a pozorování, zda jsi s nimi v souladu.</p>\n";
      $content .= "<p>Pokud potřebuješ doklad o zaplacení, napiš mi prosím na email: <a href='mailto:".EMAIL_INFO."'>".EMAIL_INFO."</a>, obratem Ti jej zašlu.</p>\n";
      $content .= "<br><p>Nezapomeň se s dalším <strong>termínem své menstruace</strong> <a href='".GOOGLE_LOGIN."' title='Odkaz na Cleopatru'>přihlásit</a> a během minutky <strong>zadat aktualizaci kalendáře</strong>, abys jej měla aktuální a načetly se ti i cykly s jednotlivými fázemi na dalších 6 měsíců dopředu.</p>\n";
      $content .= "<p>Neboj, pokud zapomeneš, pošleme Ti po 30ti dnech připomenutí emailem :) </p>\n";
      $content .= "<p>Je krásné být <a href='".PROMENLINA_ZENA_VIDEO."'>ženou ve všech svých proměnách</a> a věřím, že Ti kalendář pomůže tuto proměnlivost využívat naplno!</p>\n";
      $content .= "<p>Tvoje Cleopatra</p>\n";

      $content .= "</div></body></html>\n";

      $this->sendEmail($email,$subject,$content);
  }

  public function giveLicense($ID, $days) {
    if ($this->openDB() && (isset($ID)) && ($days > 1)) {

      //$expirateDate = date('Y-m-d', strtotime(date('Y-m-d').' +' . $days . ' days'));

      $sql = 'SELECT (Expirate) FROM TSettings WHERE Owner = "' . $ID . '"';
      $result = $this->dbConn->query($sql);
      if ($result->num_rows == 1) {
        $detail = $result->fetch_assoc();
        $datum_expirace = $detail['Expirate'];
        $date1 = date_create($datum_expirace);
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date2, $date1);              //datum_expirace - dnesni_datum
        if(number_format($diff->format("%r%a")) > 0){                                  // = expirace ještě nenastala -> prodloužit o rok od expirace
          $sql2 = 'UPDATE TSettings SET Expirate=DATE_ADD(Expirate, INTERVAL "' . $days . '" DAY) WHERE Owner=' . $ID .';';
        }else{                                          // = už je po expiraci -> prodloužit o rok od dneška
          $sql2 = 'UPDATE TSettings SET Expirate=DATE_ADD(CURDATE(), INTERVAL "' . $days . '" DAY) WHERE Owner=' . $ID .';';
        } 
        if ($this->dbConn->query($sql2) === true) {
          //$sql = 'SELECT (Expirate) FROM TSettings WHERE Owner = "' . $ID . '"';
          $sql = 'SELECT * FROM TSettings INNER JOIN TUsers ON TSettings.Owner = TUsers.ID WHERE ID ="' . $ID . '"';
          $result2 = $this->dbConn->query($sql);
          if ($result2->num_rows == 1) {
            $detail2 = $result2->fetch_assoc();
            $this->setUserInformation('expirate', $detail2['Expirate']);
            $this->sendPaymentConfirmationEmail($detail2['Expirate'],$detail2['Name'],$detail2['Email']);
            return true;
          }
        }
      }
    }
    return false;
  }

  public function createNewSettings($ID) {
    if (DEMO_IS_ON && (DEMO_LENGTH > 0)) {
      $expirateDate = date('Y-m-d', strtotime(date('Y-m-d').' +' . DEMO_LENGTH . ' days'));
    } else {
      $expirateDate = date('Y-m-d');
    }

    if ($this->openDB() && (isset($ID)) && (isset($expirateDate))) {
      $sql = 'INSERT INTO TSettings (Owner, Agreement, Expirate, AutoPeriod, LengthPeriod, AutoCycle, LengthCycle, Pregnancy)
      VALUES ("' . $ID . '", 0, "' . $expirateDate . '", 1, 0, 1, 0, 0);';

      if ($this->dbConn->query($sql) === true) {
        return true;
      }

      if (DEBUGGING) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
      }
    }
    return false;
  }

  public function createNewAccount($name, $gender, $email) {
    /*  Metoda pro vytváření uživatelů v TUsers
        Vstup: Jméno, pohlaví, email
        Při úspěšném vytvoření metoda vrací databází předělené ID,
        jinak vrací false
    */

    if ($this->openDB() && (isset($name, $gender, $email))) {
      $sql = 'INSERT INTO TUsers (Name, Gender, Email)
      VALUES ("' . mysqli_real_escape_string($this->dbConn,$name) . '",
              "' . mysqli_real_escape_string($this->dbConn,$gender) . '",
              "' . mysqli_real_escape_string($this->dbConn,$email) . '");';

      if ($this->dbConn->query($sql) === true) {
        // Úspěšné vložení + vrátí databází přidělené IDs
        $userID = $this->dbConn->insert_id;

        if ($this->createNewSettings($userID)) {
          return $userID;
        }
      }

      if (DEBUGGING) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
      }
    }
    return false;
  }

  //-------------- PARTNERS ---------------------

  public function createNewPartner($name, $email, $account) {
    /*  Metoda pro vytváření partnerů v TPartners
        Vstup: Jméno, email
        Při úspěšném vytvoření metoda vrací true
    */
    if ($this->openDB() && (isset($name, $email, $account))) {
      //$coupon = $this->getCouponCode(6);
      $coupon = $this->getRandomString(6);
      $sql = 'INSERT INTO TPartners (name, email, account, coupon, from_date)
      VALUES ("' . mysqli_real_escape_string($this->dbConn,$name) . '",
              "' . mysqli_real_escape_string($this->dbConn,$email) . '",
              "' . mysqli_real_escape_string($this->dbConn,$account) . '",
              "' . mysqli_real_escape_string($this->dbConn,$coupon) . '",
              "'.date('Y-m-d').'");';


      //poslat email s html kodem pro partnera ajs s obrazekm   
      $this->sendCouponToAdmin($name, $email, $coupon);     

      if ($this->dbConn->query($sql) === true) {
        return true;
      }

      if (DEBUGGING) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
      }
    }
    return false;
  }

  public function sendCouponToAdmin($name, $email_partner, $coupon){

    $couponWrapper2 = '<div id="cleopatraWrapper" style="position: relative; width:1000px;">
  <img src="'.HOME.'/img/banner-cleopatra.jpg" style="width:100%" alt="slevový kupón kalendáře Cleopatra až na 45 procent">
  <div style="position:absolute;bottom: 18%;left: 46%;z-index:1000;font-size: 25px;font-weight: bold;">'.$coupon.'</div>
</div>';
    $couponWrapper = htmlentities($couponWrapper2);

    $subject = "Kupón pro partnera: ".$name ;

    $content = $this->getEmailStyle();
    $content .= "<p><strong>Přeji krásný den </strong>a děkuji za vyplnění údajů v dotazníku a uzavření Dohody o spolupráci.</p><p>Váš <strong>Slevový kupón je: <span style='color:crimson;'>".$coupon."</span></strong>. V příloze najdete váš Slevový kupón také jako obrázek, který můžete svým klientkám vytisknout či zaslat.</p><p><strong>Propagační banner Cleoptary </strong>(kód najdete níže v tomto emailu) si můžete umístit na vlastní stránky, abyste vašim klientkám usnadnila přístup ke Cleopatře.</p>\r\n<p><strong>Vaši reklamu </strong>ráda umístím do sekce <a href='".HOME.PARTNERI_SUBPAGE."'>Partneři</a>&nbsp;na webu Cleopatry. Stačí mi poslat logo či fotku a text (pokud jste ho již nevyplnila do dotazníku).</p>\r\n<p><strong>K samotné aplikaci </strong>vřele doporučuji vyzkoušet si jí na<strong>&nbsp;měsíc zdarma</strong>. Jak založit a používat kalendář, je dobře vidět na <a href='".NAVOD_VIDEO."'>videonávodu</a>. Mohlo by se vám hodit, že na webu je i speciální sekce <a href='".HOME.VYHODY_SUBPAGE."'>Výhody Cleopatry</a>&nbsp;a věřím, že řadu odpovědí najdete také na stránce <a href='".INFO_PAGE."'>Časté otázky</a>. </p>\r\n<p><strong>K doporučení <a href='".HOME."'>Cleopatry</a></strong>&nbsp;neváhejte využít nejen naše skvělé propagační <a href='".PROMENLINA_ZENA_VIDEO."'><strong>VIDEO</strong></a>, ale i odkaz na <a href='".FACEBOOK."'>Facebook</a>&nbsp;či <a href='".INSTAGRAM."'>Instagram</a>&nbsp;Cleoptary.</p>\r\n<p><strong>Provizi a počet klientek</strong>, které si Cleopatru zakoupili Vám pošlu na konci měsíce spolu s informací o uhrazení celkové částky na váš účet. Neozvu se v případě, že by si za uplynulý měsíc nikdo Cleopatru s vaším Slevovým kupónem neobjednal.</p>\r\n<p>Zároveň budu velmi ráda za Vaši <strong>zpětnou vazbu </strong>(i vašich klientek), případně nápady na zlepšení jak aplikace, tak spolupráce či podpory.</p><p><strong>V případě jakýchkoliv dotazů se na mě neváhejte obrátit.</strong></p><p>Těším se na spolupráci</p><p>Ludmila Hobzová<br>mobil: 605 146 860<br><a href='mailto:".EMAIL_LIDA."'>".EMAIL_LIDA."</a><br><a href='".HOME."'>".HOME."</a></p>\r\n";
    $content .= "<h2>Údaje partnera</h2>\n";
    $content .= "jméno: <strong>".$name."</strong><br>\n";
    $content .= "email: <strong>".$email_partner."</strong><br>\n";
    $content .= "kupon: <strong>".$coupon."</strong><br>\n";
    $content .= "<h2>Kód pro vložení do internetové stránky</h2>\n";
    $content .= "<pre><code style='width:100%; max-width:600px;'>".$couponWrapper."</code></pre>\n";
    $content .= "<h2>Banner v \"obrazové\" formě</h2>\n";
    $content .= $couponWrapper2."\n";

    $content .= "</div></body></html>\n";

    $this->sendEmail(EMAIL_LIDA.",".EMAIL_ADMIN ,$subject,$content); //EMAIL_ADMIN  EMAIL_INFO.",".
}


  public function getAllActivePartners() {
    if ($this->openDB()) {

      //$sql = 'SELECT * FROM TPartners WHERE TPartners.active=1 ORDER BY from_date';

      $sql = 'SELECT TPartners.id, TPartners.name, TPartners.email, TPartners.from_date, TPartners.account, TPartners.coupon, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, total
      FROM TPartners
      LEFT JOIN (SELECT TPayments.coupon,
                    SUM(CASE WHEN MONTH(Time) = 1 THEN 1 ELSE 0 END) as m1, 
                    SUM(CASE WHEN MONTH(Time) = 2 THEN 1 ELSE 0 END) as m2, 
                    SUM(CASE WHEN MONTH(Time) = 3 THEN 1 ELSE 0 END) as m3, 
                    SUM(CASE WHEN MONTH(Time) = 4 THEN 1 ELSE 0 END) as m4, 
                    SUM(CASE WHEN MONTH(Time) = 5 THEN 1 ELSE 0 END) as m5, 
                    SUM(CASE WHEN MONTH(Time) = 6 THEN 1 ELSE 0 END) as m6, 
                    SUM(CASE WHEN MONTH(Time) = 7 THEN 1 ELSE 0 END) as m7, 
                    SUM(CASE WHEN MONTH(Time) = 8 THEN 1 ELSE 0 END) as m8, 
                    SUM(CASE WHEN MONTH(Time) = 9 THEN 1 ELSE 0 END) as m9, 
                    SUM(CASE WHEN MONTH(Time) = 10 THEN 1 ELSE 0 END) as m10,   
                    SUM(CASE WHEN MONTH(Time) = 11 THEN 1 ELSE 0 END) as m11, 
                    SUM(CASE WHEN MONTH(Time) = 12 THEN 1 ELSE 0 END) as m12,
                    COUNT(TPayments.coupon) as total
                    FROM TPayments
                    WHERE YEAR(TPayments.Time)=YEAR(CURRENT_DATE())
                    GROUP BY TPayments.coupon) as pay
                    
                    ON TPartners.coupon = pay.coupon
                    WHERE TPartners.active=1 
                    ORDER BY TPartners.id';
//(SELECT TPayments.Coupon FROM TPayments GROUP BY TPayments.coupon)
      $result = $this->dbConn->query($sql);

      if ($result->num_rows >= 1) {
        return $result;
      }
    }
    return false;
  }

  public function deactivatePartner($ID) {
    if ($this->openDB() && isset($ID) && ctype_digit($ID)) {              //if the id is numeric string

      //$sql = 'DELETE FROM TPartners WHERE ID = "' . $ID . '"';            //delete
      $sql = 'UPDATE TPartners SET active=0 WHERE id = "' . $ID . '"';      //deactivate

      if ($this->dbConn->query($sql)) {
        //return true;
        cleoRedirect(ADMIN_PAGE);             //redirect to hide ?delPartner URL parameter
      }
    }
    return false;
  }

  public function checkCouponValidity($coupon){
    if ($this->openDB()) {
      $sql = 'SELECT id FROM TPartners WHERE coupon="'.mysqli_real_escape_string($this->dbConn,$coupon).'" AND active=1';   //coupon must be in database and partner activated
      $result = $this->dbConn->query($sql);

      if ($result->num_rows >= 1) {
        return true;
      }
    }
    return false;
  }




  public function deleteAccount($ID) {
    if ($this->openDB() && isset($ID)) {
      $sql = 'DELETE FROM TUsers WHERE ID = "' . $ID . '"';
      if ($this->dbConn->query($sql)) {
        return true;
      }
    }
    return false;
  }

  public function logUserIn($ID, $service = false) {
    if ($this->openDB() && (isset($ID))) {

      $sql = 'SELECT * FROM TUsers INNER JOIN TSettings ON ID = Owner WHERE ID = "' . $ID . '"';
      $result = $this->dbConn->query($sql);

      //echo $result->num_rows;

      if ($result->num_rows == 1) {
        $_SESSION['login'] = true;
        $_SESSION['id'] = $ID;
        $detail = $result->fetch_assoc();
        $_SESSION['name'] = $detail['Name'];
        $_SESSION['gender'] = $detail['Gender'];
        $_SESSION['email'] = $detail['Email'];
        $_SESSION['agreement'] = $detail['Agreement'];
        $_SESSION['expirate'] = $detail['Expirate'];

        if ($service) {
          $_SESSION['services'] = [];
          array_push($_SESSION['services'], $service);
        }

        $_SESSION['logged_in_time'] = time();

        return true;
      }
    }
    return false;
  }


  public function addPayment($merchantData,$pID,$coupon,$price) {          //merchantData = ID z původního; merchantData ->  "owner"
    if ($this->openDB() && (isset($merchantData, $pID, $coupon,$price))) {
      $sql = 'SELECT (Owner) FROM TPayments WHERE PaymentID = "' . $pID . '"';  //zjištění, jestli záznam už existuje
      $result = $this->dbConn->query($sql);

      if ($result->num_rows == 0) {         
        $sql = 'INSERT INTO TPayments (Owner, Coupon, Price, PaymentId)
        VALUES ("' . mysqli_real_escape_string($this->dbConn,$merchantData) . '",
                "' . mysqli_real_escape_string($this->dbConn,$coupon) . '",
                "' . mysqli_real_escape_string($this->dbConn,$price) . '",
                "' . mysqli_real_escape_string($this->dbConn,$pID) . '");';

        if ($this->dbConn->query($sql) === true) {
          return true;
        }

        if (DEBUGGING) {
          echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
        }
      }
    }
    return false;
  }


  public function log($ID,$type,$description) {
    if ($this->openDB() && (isset($ID, $type, $description))) {
      $sql = 'INSERT INTO TLogs (Owner, Type, Description)
      VALUES ("' . $ID . '",
              "' . mysqli_real_escape_string($this->dbConn,$type) . '",
              "' . mysqli_real_escape_string($this->dbConn,$description) . '");';

      if ($this->dbConn->query($sql) === true) {
        return true;
      }

      if (DEBUGGING) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . '</p>';
      }
    }
    return false;
  }
}
?>
