<?php
///This is a dummy product database
class ProductDb {

	///Our top products are here in array
	private $products = array (
		'1' => array(
			'id' => 1,
			'price' => 250,
			'name' => 'Cleopatra na 1 rok'
		)
	);

	///Get particular product by its id
	public function getProduct($id){
		if (array_key_exists($id,$this->products)){
			return $this->products[$id];
		} else {
			return null;
		}
	}

}
