<?php

include('cleo_users.php');
class cleoCron {
  public $dbConn = null;

  public function __construct() {
      $this->dbConn = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_NAME);
      if ($this->dbConn->connect_error) {
        echo '<p class="error">Connection failed: ' . $this->dbConn->connect_error . '</p>';
      }
      mysqli_set_charset($this->dbConn,'utf8');
  }

  public function log($UserID,$type,$description) {
    if (isset($UserID, $type, $description)) {
      $sql = 'INSERT INTO TLogs (Owner, Type, Description)
      VALUES ("' . $UserID . '",
              "' . mysqli_real_escape_string($this->dbConn,$type) . '",
              "' . mysqli_real_escape_string($this->dbConn,$description) . '");';

      $result = $this->dbConn->query($sql);
    }
  }

  public function getHeaders($type) {
    // hlavičky
    // TODO diakritika
    switch ($type) {
      default:
        $headers = "From: ".EMAIL_INFO."\n";
        //$headers .= "Reply-to: ".EMAIL_INFO."\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/html; charset=utf-8\n";
        $headers .= "Content-Transfer-Encoding: 8bit\n";

    }

    return $headers;
  }

  public function getSubject($type) {
    // obsah zprávy
    // TODO diakritika
    switch ($type) {
      case "expirate":
        $subject = "Předplatné kalendáře Cleopatra";
        break;
      case "request_for_update":
        $subject = "Připomenutí aktualizace kalendáře Cleopatra";
        break;
      case "request_for_payment":
        $subject = "Předplatné kalendáře Cleopatra";
        break;
      case "farewell":
        $subject = "Konec předplatného kalendáře Cleopatra";
        break;
      default:
        $subject = "Informace od Cleopatry";
    }

    return $subject;
  }

  public function getContent($name,$type) {
    // obsah zprávy
    // TODO diakritika

    $cUsers = new cleoUsers;


    $content = $cUsers->getEmailStyle(); //"<html><body>\n";

   /*  if (!isset($name) || ($name == 'none') ) {
      $content .= "<h1>Jsem  moc ráda,</h1>\n";
    } else {
      $content .= "<h1>Jsem ráda <strong>" . $name . "</strong>,</h1>\n";
    } */

    $content .= "<h1>Jsem moc ráda,</h1>\n";

    switch ($type) {
      case "expirate":
        $content .= "<p>že jsi vyzkoušela Cleopatru a věřím, že Tě inspirovala ke sledování jednotlivých fází Tvého cyklu a pozorování zda jsi s nimi v souladu. Pokud máš i nadále zájem o kalendář Cleopatra, tak není nic jednoduššího, než se <a href='".GOOGLE_LOGIN."' title='Přihlášení do Cleopatry'><strong>přihlásit</strong></a> a vybrat si <a href='".LICENSING_PAGE."'>platební metodu</a> pro zakoupení <strong>ročního předplatného, které je ".PAYMENT_NORMAL.",-Kč</strong>. Tím si zajistíš plán svých cyklů <strong>vždy na 6 měsíců dopředu</strong> a bezproblémové fungování aplikace. Samozřejmě můžeš využít Slevového kupónu od některých z našich <a href='".PARTNERI_SUBPAGE."'>Partnerů</a>.</p>\n";
        $content .= "<p>Pokud bys zapomněla zadat aktuální termín menstruace, přijde Ti po 30ti dnech připomenutí.</p>";
        $content .= "<p>V případě <strong>neuhrazení předplatného bude Tvůj kalendář vymazán</strong>. Kdykoli se můžeš vrátit na <a href='".HOME."' title='Odkaz na Cleopatru'>".HOME."</a>, znovu si jej předplatit a vytvořit nový kalendář.</p>\n";
        $content .= "<p>Je krásné být <a href='".PROMENLINA_ZENA_VIDEO."'>ženou ve všech svých proměnách</a> a věřím, že Ti kalendář pomůže tuto proměnlivost využívat naplno!</p>\n";
        $content .= "<p>Tvoje Cleopatra</p>\n";
        break;

      case "request_for_update":
        $content .= "<p>že používáš kalendář Cleopatra a dovoluji si Tě upozornit, že je to dnes " . date("j. n. Y") . " již 30 dní, kdy <strong>nedošlo k zadání předpokládaného termínu další menstruace</strong> a tím i k aktualizaci Tvého kalendáře Cleopatra. Je možné, že ses jen zapomněla přihlásit ke svému účtu na <a href='".HOME."' title='Odkaz na Cleopatru'>".HOME."</a> a cyklus zadat. Pokud je to tak, <a href='".GOOGLE_LOGIN."' title='Přihlášení do Cleopatry'>přihlaš se</a> a během minutky <strong>zadej aktualizaci kalendáře</strong>, abys měla aktuální přehled o svých fázích cyklu.</p>\n";
        $content .= "<p>Je krásné být <a href='".PROMENLINA_ZENA_VIDEO."'>ženou ve všech svých proměnách</a> a věřím, že Ti kalendář pomůže tuto proměnlivost využívat naplno!</p>\n";
        $content .= "<p>Tvoje Cleopatra</p>\n";
        break;

      case "request_for_payment":
        $content .= "<p>že používáš Cleopatru a věřím, že jsi s ní spokojena a inspirovala Tě ke sledování jednotlivých fází Tvého cyklu a pozorování zda jsi s nimi v souladu. Pokud máš i nadále zájem o kalendář Cleopatra, tak není nic jednoduššího, než se <a href='".GOOGLE_LOGIN."' title='Přihlášení do Cleopatry'>přihlásit</a> a vybrat si <a href='".LICENSING_PAGE."'>platební metodu</a> pro zakoupení <strong>ročního předplatného, které je ".PAYMENT_NORMAL.",-Kč</strong>. Samozřejmě můžeš využít Slevového kupónu od některých z našich <a href='".PARTNERI_SUBPAGE."'>Partnerů</a>. Uhrazením předplatného si zajistíš bezproblémové fungování aplikace na další rok, který bude jistě nádherný!</p>\n";
        $content .= "<p>Je krásné být <a href='".PROMENLINA_ZENA_VIDEO."'>ženou ve všech svých proměnách</a> a věřím, že Ti kalendář pomůže tuto proměnlivost využívat naplno!</p>\n";
        $content .= "<p>Tvoje Cleopatra</p>\n";
        break;

      case "farewell":
        $content .= "<p>žes používala Cleopatru a věřím, že jsi s ní byla spokojená. Protože sis ji zatím znovu na další rok nepředplatila, prodloužila jsem Ti tuto možnost o 10 dní. <strong>Pokud máš i nadále zájem o kalendář Cleopatra</strong>, tak je třeba se <a href='".GOOGLE_LOGIN."' title='Přihlášení do Cleopatry'>přihlásit</a>přihlásit</a> a vybrat si <a href='".LICENSING_PAGE."'>platební metodu</a> k uhrazení <strong>ročního předplatného, které je ".PAYMENT_NORMAL.",-Kč</strong>. Samozřejmě můžeš využít Slevového kupónu od některých z našich <a href='".PARTNERI_SUBPAGE."'>Partnerů</a>.</p>\n";
        $content .= "<p>V případě neuhrazení poplatku bude <strong> Tvůj kalendář i všechny údaje vymazány</strong> ze systému Cleopatra.</p>\n";
        $content .= "<p>Kdykoli se můžeš vrátit na <a href='".HOME."' title='Odkaz na Cleopatru'>".HOME."</a>, znovu si jej <strong>předplatit a vytvořit si tak nový</strong> kalendář.</p>\n";
        $content .= "<p>Je krásné být <a href='".PROMENLINA_ZENA_VIDEO."'>ženou ve všech svých proměnách</a> a věřím, že Ti kalendář pomůže tuto proměnlivost využívat naplno!</p>\n";
        $content .= "<p>Tvoje Cleopatra</p>\n";
        break;

      default:
        $content .= "<p>že používáš či se podílíš na kalendáři Cleopatra..</p>\n";
        $content .= "<p>Nicméně tento defaultní email Ti neměl nikdy přijít. Něco je špatně..</p>\n";
        $content .= "<p>Tvoje Cleopatra</p>\n";
    }

    $content .= "</divS></body></html>\n";

    return $content;
  }

  public function sendSimpleEmail($address,$name,$type) {
    if ( (isset($address)) && (mail($address,$this->getSubject($type),$this->getContent($name,$type),$this->getHeaders($type))) ) {
      return true;
    }
    return false;
  }

  public function countRemainingDays($to) {
    $datediff = strtotime($to) - strtotime("now");
    return round($datediff / (60 * 60 * 24));
  }

  public function checkAllExpiredLicenses() {
    $count = 0;

    $sql = 'SELECT TUsers.ID, TUsers.Email, TUsers.Name, TSettings.Expirate
            FROM TUsers AS TUsers
            INNER JOIN TSettings AS TSettings
            ON TUsers.ID = TSettings.Owner';

    $result = $this->dbConn->query($sql);

    while ($row = $result->fetch_assoc()) {

      $diff = floor((strtotime($row['Expirate']) - strtotime("NOW")) / (60*60*24));
      if ($diff < 0) {
        // uživatel nemá validní licenci
        $this->sendSimpleEmail($row['Email'],$row['Name'],'expirate');
        $this->log($row['ID'],'email','expirate');
        $count++;
      }
    }

    return $count;
  }

  public function checkAfterDemoMails() {
    $expirateDate = date('Y-m-d');
    $count = 0;

    $sql = 'SELECT TUsers.ID, TUsers.Email, TUsers.Name
            FROM TUsers AS TUsers
            INNER JOIN TSettings AS TSettings
            ON TUsers.ID = TSettings.Owner
            WHERE TSettings.Expirate="' . $expirateDate . '"';

    $result = $this->dbConn->query($sql);

    while ($row = $result->fetch_assoc()) {

      $sql2 = 'SELECT TPayments.Owner
              FROM TPayments AS TPayments
              WHERE TPayments.Owner="' . $row['ID'] . '"';
      $result2 = $this->dbConn->query($sql2);

      if ($result2->num_rows == 0) {
        $this->sendSimpleEmail($row['Email'],$row['Name'],'expirate');
        $this->log($row['ID'],'email','expirate');
        $count++;
      }
    }

    return $count;
  }

  public function checkEventsUpdateMails() {
    $count = 0;
    $beforeThirtyDays = date('Y-m-d', strtotime("-30 days"));

    $sql = 'SELECT ID, Email, Name
            FROM TUsers
            WHERE LatestEvent="' . $beforeThirtyDays . '"';

    $result = $this->dbConn->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->sendSimpleEmail($row['Email'],$row['Name'],'request_for_update');
      $this->log($row['ID'],'email','request_for_update');
      $count++;
    }

    return $count;
  }

  public function checkExpireLicenseType($date,$type) {
    $count = 0;
    $sql = 'SELECT TUsers.ID, TUsers.Email, TUsers.Name, TSettings.Expirate
            FROM TUsers AS TUsers
            INNER JOIN TSettings AS TSettings
            ON TUsers.ID = TSettings.Owner
            WHERE TSettings.Expirate="' . $date . '"';

    $result = $this->dbConn->query($sql);

    while ($row = $result->fetch_assoc()) {
      $sql2 = 'SELECT TPayments.ID
      FROM TPayments AS TPayments
      WHERE TPayments.Owner = "' . $row['ID'] . '"';

      $result2 = $this->dbConn->query($sql2);

      if ($result2->num_rows > 0) {
        $this->sendSimpleEmail($row['Email'],$row['Name'],$type);
        $this->log($row['ID'],'email',$type);
        $count++;
      }
    }
    return $count;
  }

  public function checkExpireLicenseMails() {
    $count = 0;
    $beforeExpirateDate = date('Y-m-d', strtotime("+3 days"));
    $afterExpirateDate = date('Y-m-d', strtotime("-18 days"));

    $count += $this->checkExpireLicenseType($beforeExpirateDate,'request_for_payment');
    $count += $this->checkExpireLicenseType($afterExpirateDate,'farewell');

    return $count;
  }

  public function checkEmails() {
    $count = $this->checkAfterDemoMails();
    $count2 = $this->checkExpireLicenseMails();
    $count3 = $this->checkEventsUpdateMails();

    if (date("Y-m-d") == "2019-10-03") {
      $count2 += $this->checkAllExpiredLicenses();
    }

    $subject = "Cleopatra mail report " . date("ymd");
    $reportMailContent = "<html><body>\n";
    $reportMailContent .= "<h1>Report ze dne " . date("d.m.Y") . "</h1>\n";
    $reportMailContent .= "<p>Počet dnes odeslaných emailů po ukončení dema: " . $count . ".</p>\n";
    $reportMailContent .= "<p>Počet dnes odeslaných emailů po vypršení licence: " . $count2 . ".</p>\n";
    $reportMailContent .= "<p>Počet dnes odeslaných emailů s výzvou k aktualizaci kalendáře: " . $count3 . ".</p>\n";
    $reportMailContent .= "<p>S přáním hezkého dne.</p>\n";
    $reportMailContent .= "<p>Cleopatra</p>\n";
    $reportMailContent .= "</body></html>\n";

    //if ( mail('TODO EMAIL ADRESS',$subject,$reportMailContent,$this->getHeaders("default")) ) {
      return true;
    //}
    //return false;
  }

  // USER PART

  private function deleteAccount($ID) {
    if ($this->openDB() && isset($ID)) {
      $sql = 'DELETE FROM TUsers WHERE ID = "' . $ID . '"';
      if ($this->dbConn->query($sql)) {
        return true;
      }
    }
    return false;
  }

  private function checkInnactiveAccounts() {
    $count = 0;
    $beforeMoreThenYear = date('Y-m-d', strtotime("-400 days"));

    $sql = 'SELECT ID, Email, Name
            FROM TUsers
            WHERE LatestEvent="' . $beforeMoreThenYear . '"';

    $result = $this->dbConn->query($sql);

    while ($row = $result->fetch_assoc()) {
      if ($this->deleteAccount($row['ID'])) {
        $this->log($row['ID'],'cron','delete');
        $count++;
      } else {
        $this->log($row['ID'],'error','Cron delete account');
      }
    }

    return $count;
  }

  public function checkUsers() {
    $count = $this->checkInnactiveAccounts();

    $subject = "Cleopatra users report " . date("ymd");
    $reportMailContent = "<html><body>\n";
    $reportMailContent .= "<h1>Report ze dne " . date("d.m.Y") . "</h1>\n";
    $reportMailContent .= "<p>Počet dnes smazaných uživatelů: " . $count . ".</p>\n";
    $reportMailContent .= "<p>S přáním hezkého dne.</p>\n";
    $reportMailContent .= "<p>Cleopatra</p>\n";
    $reportMailContent .= "</body></html>\n";

    //if ( mail('TODO EMAIL ADRESS',$subject,$reportMailContent,$this->getHeaders("default")) ) {
      return true;
    //}
    //return false;
  }

}
?>
