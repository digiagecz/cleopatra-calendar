<?php
class googleAuth {
  public $dbConn = null;

  // DATABASE PART
  // *************

  public function openDB() {
    if ($this->dbConn === null) {
      $this->dbConn = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_NAME);
      if ($this->dbConn->connect_error) {
        if ( DEBUGGING ) {
          echo '<p class="error">Connection failed: ' . $this->dbConn->connect_error . '</p>';
        }
        else {
          return false;
        }
      }

      mysqli_set_charset($this->dbConn,'utf8');
    }
    return true;
  }

  public function doSomethingWithDB($sql) {
    if ($this->openDB() && (isset($sql))) {
      if ( $this->dbConn->query($sql) === true ) {
        return true;
      }

      if ( DEBUGGING ) {
        echo '<p class="error">SQL exeption: ' . $this->dbConn->error . ',<br /> SQL: ' . $sql . '</p>';
      }
    }
    return false;
  }

  // TODO jen jeden Google účet
  public function getSomethingFromDB($sql, $return_value) {
    if ($this->openDB() && (isset($sql))) {
      $result = $this->dbConn->query($sql);

      if ( $result->num_rows == 1 ) {
        $row = $result->fetch_assoc();
        return $row[$return_value];
      }
    }
    return false;
  }

  public function getOwnerID($googleID) {
    if (($this->openDB()) && (isset($googleID))) {
      $safeGoogleID = mysqli_real_escape_string($this->dbConn,$googleID);
      $sql = 'SELECT Owner FROM TGoogleAccounts WHERE ID="' . $safeGoogleID . '"';
      return $this->getSomethingFromDB($sql, "Owner");
    }
    return false;
  }

  public function getGoogleID($OwnerID) {
    if (($this->openDB()) && (isset($OwnerID))) {
      $sql = 'SELECT ID FROM TGoogleAccounts WHERE Owner="' . $OwnerID . '"';
      return $this->getSomethingFromDB($sql, "ID");
    }
    return false;
  }

  public function getOwnerToken($OwnerID) {
    if (($this->openDB()) && (isset($OwnerID))) {
      $sql = 'SELECT Token FROM TGoogleAccounts WHERE Owner="' . $OwnerID . '"';
      return $this->getSomethingFromDB($sql, "Token");
    }
    return false;
  }

  public function getOwnerCalendarID($OwnerID) {
    if (($this->openDB()) && (isset($OwnerID))) {
      $sql = 'SELECT CalendarID FROM TGoogleAccounts WHERE Owner="' . $OwnerID . '"';
      $calendar = $this->getSomethingFromDB($sql, "CalendarID");

      if (($calendar) && ($calendar != 'none')) {
        return $calendar;
      }
    }
    return false;
  }

  public function updateCalendarID($ownerID, $calendarID) {
    if ( ($this->openDB()) && (isset($ownerID, $calendarID)) ) {
      $sql = 'UPDATE TGoogleAccounts
              SET CalendarID="' . $calendarID . '"
              WHERE Owner="' . $ownerID . '"';
      return $this->doSomethingWithDB($sql);
    }
    return false;
  }

  public function recordUpdate($googleID, $ownerID, $accessToken) {
    if ( ($this->openDB()) && isset($ownerID, $accessToken )) {
      // TODO jen jeden Google účet
      $sql = 'UPDATE TGoogleAccounts
              SET Token="' . $accessToken['access_token'] . '"
              WHERE Owner="' . $ownerID . '"';
      return $this->doSomethingWithDB($sql);
    }
    return false;
  }

  public function createNewRecord($googleID, $ownerID, $accessToken) {
    if ( ($this->openDB()) && isset($googleID, $ownerID, $accessToken) ) {
      $sql = 'INSERT INTO TGoogleAccounts (ID, Owner, Token)
      VALUES ("' . mysqli_real_escape_string($this->dbConn,$googleID) . '",
              "' . mysqli_real_escape_string($this->dbConn,$ownerID) . '",
              "' . mysqli_real_escape_string($this->dbConn,$accessToken['access_token']) . '")';
      return $this->doSomethingWithDB($sql);
    }
    return false;
  }

  // GOOGLE EVENTS PART
  public function getGoogleEventsIDs($ownerID) {
    if ( ($this->openDB()) && isset($ownerID) ) {
      $sql = 'SELECT EventID FROM TGoogleEvents WHERE Owner="' . $ownerID . '"';
      $result = $this->dbConn->query($sql);

      if ($result) {
        return $result;
      }
    }
    return false;
  }

  public function clearGoogleEventsInDB($ownerID) {
    if ( ($this->openDB()) && isset($ownerID) ) {
      $sql = 'DELETE FROM TGoogleEvents WHERE Owner="' . $ownerID . '"';
      return $this->doSomethingWithDB($sql);
    }
    return false;
  }

  public function saveGoogleEventId($ownerID, $eventID) {
    if ( ($this->openDB()) && isset($ownerID, $eventID) ) {
      $sql = 'INSERT INTO TGoogleEvents (Owner, EventID)
      VALUES ("' . $ownerID . '",
              "' . $eventID . '")';
      return $this->doSomethingWithDB($sql);
    }
    return false;
  }

  //public function deleteAccount($ownerID) {
  //  if ($this->openDB() && isset($ownerID)) {
  //    $sql1 = 'DELETE FROM TGoogleEvents WHERE Owner = "' . $ownerID . '"';
  //    $sql2 = 'DELETE FROM TGoogleAccounts WHERE Owner = "' . $ownerID . '"';

  //    if ( ($this->dbConn->query($sql1)) && ($this->dbConn->query($sql2)) ) {
  //      return true;
  //    }
  //  }
  //  return false;
  //}
}
