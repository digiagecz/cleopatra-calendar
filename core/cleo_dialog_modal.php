<?php
switch ($_GET['after']) {
  case 'delete':
    $dialogClass = 'modal fade';
    $dialogHeader = "Účet byl úspěšně smazán";
    $dialogContent = '<p class="text-center">Veškeré Vaše údaje byly z Cleopatry<br /> (ze serveru ' . HOME . ')<br /> úspěšně smazány.</p>' . PHP_EOL;
    $dialogContent .= '<p class="text-center">Přejeme Vám přijemný den.</p>';
    $dialogButton = '<span class="btn btn-success" data-dismiss="modal">OK</span>';
    break;
  case 'notice':
    $dialogClass = 'modal fade cleo-util is--modal-80';
    $dialogHeader = "Úvodní informace";

    $dialogContent .= '<div class="container"><div class="row"><div class="col-2 text-center"><img class="cleo-util is--max-w-100" src="' . HOME . '/img/cleosmall.jpg" alt="Obrázek Cleopatry" /></div><div class="col-8">';
    $dialogContent .= '<p class="text-justify font-italic mb-0"><strong>Jsem moc ráda, že ses rozhodla vyzkoušet Cleopatru - kalendář každé cyklické ženy! Děkuji za důvěru a věřím, že budeš spokojená. Neváhej mi dát <a href="'.FEEDBACK_FORM.'" title="Odkaz na Cleopatra help" target="_blank">zpětnou vazbu</a></strong> (odkaz najdeš také v patičce webu).</p>' . PHP_EOL;
    $dialogContent .= '<p class="text-right font-italic"><strong>Ludmila Hobzová</strong></p>';
    $dialogContent .= '</div><div class="col-2">';
    $dialogContent .= '<img class="cleo-util is--max-w-100" src="' . HOME . '/img/lidule.jpg" alt="Ludmila Hobzová" />';
    $dialogContent .= '</div></div></div>';
    $dialogContent .= '<div class="container"><div class="row"><div class="col">';
    $dialogContent .= '<p class="text-justify font-italic">Vzhledem k tomu, že kalendář Cleopatra využívá veřejně dostupného prostředí GOOGLE APLIKACE, tak se Ti při dalším kroku zobrazí <strong>žádost o přístup k Tvému Google kalendáři.</strong></p>';
    $dialogContent .= '<p class="text-justify font-italic">Tento přístup je nutný k tomu, aby se Ti ihned mohl kalendář Cleopatra zobrazit přímo ve Tvém Google kalendáři (viz foto níže). K ničemu jinému tento přístup nepoužiju v souladu s <a href="'.TERMS_PAGE.'">Obchodními podmínkami a zásadami ochrany osobních údajů</a>.</p>';
    $dialogContent .= '</div></div></div>';
    $dialogContent .= '<div class="container"><div class="row"><div class="col">';
    $dialogContent .= '<p class="mt-2 mb-4 text-center"><a href="' . GOOGLE_LOGIN . '" class="btn btn-main">Jdeme na to!</a></p>';
    $dialogContent .= '<p class="text-left font-italic">Zde vidíš <strong>náhled, jak se Ti bude zobrazovat Cleopatra</strong> ve Tvém osobním Google kalendáři:</p>';
    $dialogContent .= '<img class="cleo-util is--max-w-100" src="' . HOME . '/img/kalnahl.jpg" alt="Náhled kalendáře" />';
    $dialogContent .= '</div></div></div>';

    $dialogButton = '<p class="text-right"><a href="' . GOOGLE_LOGIN . '" class="btn btn-main">Jdeme na to!</a></p>';
    break;
}
?>
<!-- Dialog modal -->
<div class="<?php echo $dialogClass;?>" id="dialog-modal" tabindex="-1" role="dialog" aria-labelledby="js-dialog-modalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title"><?php echo $dialogHeader;?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <?php echo $dialogContent;?>
  </div>
  <div class="modal-footer cleo-util is--flex-centered is--tiny-margined">
    <?php echo $dialogButton;?>
  </div>
</div>
</div>
</div>

<script>
var showDialogModal = function() {
$('#dialog-modal').modal('show');
}

if (document.readyState!='loading') showDialogModal();
else if (document.addEventListener) document.addEventListener('DOMContentLoaded', showDialogModal);
else document.attachEvent('onreadystatechange', function() {
if (document.readyState=='complete') showDialogModal();
});
</script>
