<?php
/*
  GOOGLE GENERATING EVENTS
  @Author: Martin Krotil <martin@krotil.info>
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');
require_once('../../../core/google/gauth.php');
require_once('../../../autoload.php');

$return_arr = array();

$cUsers = new cleoUsers;
if ( ($cUsers->isUserLoggedIn()) && ($cUsers->isServiceLoggedIn('google')) ) {
  $userID = $cUsers->getUserInformation('id');

  //TODO hodilodrzku refactor
  $error['error'] = false;
  $error['text'] = '';
  $error['description'] = '';

  $cEvents = new cleoEvents;
  if ($cEvents->updateFutureEvents($userID)) {
    $result = $cEvents->getFutureEvents($userID);

    // existují v CleoDB future events -> připojím se ke Google
    if ($result) {

      $gAuth = new googleAuth;
      $accessToken = $gAuth->getOwnerToken($userID);

      $client = new Google_Client();
      $pathToConfigFile = '../../../core/google/' . GOOGLE_CLIENT_SECRET_FILENAME;
      $client->setAuthConfigFile($pathToConfigFile);
      $client->setRedirectUri(GOOGLE_LOGIN);
      $client->addScope(GOOGLE_SCOPE);
      $client->setAccessType('offline');        // offline access
      $client->setIncludeGrantedScopes(true);   // incremental auth
      //$client->setApprovalPrompt('force');

      $client->setAccessToken($accessToken);

      //if ($client->isAccessTokenExpired()) {
      //  $client->refreshToken($client->getRefreshToken());
      //  $accessToken = $client->getAccessToken(); // This token has no refresh token.
      //}

      $service = new Google_Service_Calendar($client);
      $cleoCalendarID = $gAuth->getOwnerCalendarID($userID);

      if ($cleoCalendarID) {
        $calendar = $service->calendars->get($cleoCalendarID);
      }
      else {
        $newCalendar = new Google_Service_Calendar_Calendar();
        $newCalendar->setSummary('Cleopatra');
        $newCalendar->setTimeZone('Europe/Prague');
        $calendar = $service->calendars->insert($newCalendar);
        $gAuth->updateCalendarID($userID, $calendar->getId());
        $cleoCalendarID = $calendar->getId();
      }

      // Vymazání dosavadních Google eventů
      $eventsInDB = [];
      $inDbCounter = 0;

      $dataFromDB = $gAuth->getGoogleEventsIDs($userID);
      while ($row = $dataFromDB->fetch_assoc()) {
        $eventsInDB[$inDbCounter] = $row['EventID'];
        $inDbCounter++;
      }

      $optParams = array(
        'maxResults' => 999,
        'singleEvents' => true
      );
      $results = $service->events->listEvents($cleoCalendarID, $optParams);

      $eventsInCalendar = [];
      $inCalendarCounter = 0;

      if (!empty($results->getItems())) {
        foreach ($results->getItems() as $event) {
          $id = $event->getId();
          $eventsInCalendar[$inCalendarCounter] = $id;
          $inCalendarCounter++;
        }
      }

      for ($i = 0; $i < $inDbCounter; $i++) {
        for ($j = 0; $j < $inCalendarCounter; $j++) {
          if ($eventsInDB[$i] == $eventsInCalendar[$j]) {
            $service->events->delete($gAuth->getOwnerCalendarID($userID), $eventsInCalendar[$j]);
          }
        }
      }

      if (!$gAuth->clearGoogleEventsInDB($userID)) {
        $error['error'] = true;
        $error['text'] = ' Nebylo možno smazat existující události v Google kalendáři.';
        $error['description'] = 'gAuth->clearGoogleEventsInDB() fail';
      }

      while ($row = $result->fetch_assoc()) {
        //$data['id'][$counter] = $row['ID'];
        $summary = null;
        $colorID = 1;

        switch ($row['Type']) {
          case 'reflective':
            $summary = 'Reflektivní fáze';
            $colorID = 3;
          break;

          case 'dynamic':
            $summary = 'Dynamická fáze';
            $colorID = 5;
          break;

          case 'expressive':
            $summary = 'Expresivní fáze';
            $colorID = 11;
          break;

          case 'creative':
            $summary = 'Kreativní fáze';
            $colorID = 9;
          break;
        }

        //echo 'Generuji: ' . $summary . ' s colorID: ' . $colorID . ', start: ' . $row['Start'] . ', end: ' . $row['End'] . '<br />';

        $event = new Google_Service_Calendar_Event(array(
          'summary' => $summary,
          'colorId' => $colorID,
          'description' => 'Událost generovaná systémem Cleopatra',
          'start' => array(
            'date' => $row['Start'],
          ),
          'end' => array(
            'date' => $row['End'],
          )
        ));
        $event = $service->events->insert($calendar->getId(), $event);
        if($gAuth->saveGoogleEventId($userID, $event->getId())) {
            if (!$event->getId()) {
              $error['error'] = true;
              $error['text'] = 'Nebylo možné vytvořit dané události v Google kalendáři.';
              $error['description'] = 'gAuth->saveGoogleEventId() fail';
            }
        }
      }

      if (!$error['error']) {
        $row_array['status'] = 'OK';
        $row_array['error'] = 'Události v kalendáři byly úspěšně vygenerovány.';
        array_push($return_arr,$row_array);

        $cUsers->log($userID,'state','events are generated');
      }
      else {
        $row_array['status'] = 'ERROR';
        $row_array['error'] = 'Události nebyly řádně vygenerovány.' + $error['text'];
        array_push($return_arr,$row_array);

        $cUsers->log($userID,'error',$error['description']);
      }
    }
  }
  else {
    $row_array['status'] = 'ERROR';
    $row_array['error'] = 'Nepodařilo se vygenerovat budoucí eventy, buď chybí alespoň jeden záznam o periodě, a nebo je nastaven pregnancy = 1.';
    array_push($return_arr,$row_array);

    $cUsers->log($userID,'error','cEvents->updateFutureEvents() fail');
  }
}
else {
  $row_array['status'] = 'ERROR';
  $row_array['error'] = 'Uživatel není přihlášen!';
  array_push($return_arr,$row_array);

  $cUsers->log(0,'error','the user is not logged in');
}

header('Content-Type: application/json');
echo json_encode($return_arr);
?>
