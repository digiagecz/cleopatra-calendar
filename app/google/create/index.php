<?php
/*
  GOOGLE CREATING USER ACCOUNT GATE
  @Author: Martin Krotil <martin@krotil.info>

  Input variables
  $_GET['code']
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/google/gauth.php');
require_once('../../../autoload.php');

function isValidValue($value) {
  if ( (isset($value) && (strlen($value) > 0)) ) {
    return true;
  }
  return false;
}

$cUsers = new cleoUsers;
if ( !$cUsers->isUserLoggedIn() ) {

  $client = new Google_Client();
  $pathToConfigFile = '../../../core/google/' . GOOGLE_CLIENT_SECRET_FILENAME;
  $client->setAuthConfigFile($pathToConfigFile);
  $client->setRedirectUri(GOOGLE_LOGIN);
  $client->addScope(GOOGLE_SCOPE);
  $client->setAccessType('offline');        // offline access
  $client->setIncludeGrantedScopes(true);   // incremental auth
  //$client->setApprovalPrompt('force');

  $client->setAccessToken($_SESSION['google-access-token']);
  $accessToken = $client->getAccessToken();

  //if ($client->isAccessTokenExpired()) {
  //  $client->refreshToken($client->getRefreshToken());
  //  $accessToken = $client->getAccessToken(); // This token has no refresh token.
  //}

  if ($accessToken) {

    $oAuth = new Google_Service_Oauth2($client);

    $googleUserInfo = $oAuth->userinfo->get();

    $googleUserID = $googleUserInfo['id'];

    $gAuth = new googleAuth;
    $ownerID = $gAuth->getOwnerID($googleUserID);
    if ( $ownerID === false ) {

      if ($googleUserInfo && (filter_var($googleUserInfo['email'], FILTER_VALIDATE_EMAIL))) {
        $mainName = 'none';
        $mainGender = 'none';

        if ( isValidValue($googleUserInfo['name']) ) {
          $mainName = $googleUserInfo['name'];
        }
        else {
          $secondName = $googleUserInfo['givenName'] . $googleUserInfo['familyName'];
          if ( isValidValue($secondName) ) {
            $mainName = $secondName;
          }
        }

        if ( isValidValue($googleUserInfo['gender']) ) {
          $mainGender = $googleUserInfo['gender'];
        }

        $newUserID = $cUsers->createNewAccount($mainName,$mainGender,$googleUserInfo['email']);

        if ( $newUserID ) {
            if ( $gAuth->createNewRecord($googleUserID, $newUserID, $accessToken) ) {

              // Vytvoření calendáře
              $service = new Google_Service_Calendar($client);
              $newCalendar = new Google_Service_Calendar_Calendar();
              $newCalendar->setSummary('Cleopatra');
              $newCalendar->setTimeZone('Europe/Prague');
              $calendar = $service->calendars->insert($newCalendar);

              if ( $gAuth->updateCalendarID($newUserID, $calendar->getId()) ) {

                if ( $cUsers->logUserIn($newUserID, 'google') ) {
                  $cUsers->setUserInformation('picture', $googleUserInfo['picture']);

                  cleoRedirect(APPHOME);

                }
                else {
                  cleoRedirect(HOME,'err_login','Chyba při pokusu přihlásit uživatele.');
                }
              }
              else {
                cleoRedirect(HOME,'err_calendar','Chyba při pokusu vytvořit kalendář.');
              }
            }
            else {
              $cUsers->deleteAccount($newUserID);
              cleoRedirect(HOME, 'err_db_newrecord', 'Chyba při pokusu vytvořit nový záznam v CleoDB.');
            }
        }
        else {
          $pom = 'Chyba při pokusu vytvořit nového uživatele v CleoDB.';
          cleoRedirect(HOME, 'err_db_newuser', $pom);
        }
      }
      else {
        cleoRedirect(HOME, 'err_service', 'Chyba - Google API vrátil nedostatečná data (email vyžadován).');
      }
    }
    else {
      cleoRedirect(GOOGLE_LOGIN);
    }

  }
  else {
    cleoRedirect(HOME, 'err_service', 'Chyba - Nelze vytvořit nový účet na Cleopatře.');
  }

}
else {
  cleoRedirect(APPHOME);
}
?>
