<?php
/*
  GOOGLE CALENDAR RESET
  @Author: Martin Krotil <martin@krotil.info>
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');
require_once('../../../core/google/gauth.php');
require_once('../../../autoload.php');

$cUsers = new cleoUsers;
if ( ($cUsers->isUserLoggedIn()) && ($cUsers->isServiceLoggedIn('google')) ) {
  $userID = $cUsers->getUserInformation('id');

  $gAuth = new googleAuth;
  $accessToken = $gAuth->getOwnerToken($userID);

  $client = new Google_Client();
  $pathToConfigFile = '../../../core/google/' . GOOGLE_CLIENT_SECRET_FILENAME;
  $client->setAuthConfigFile($pathToConfigFile);
  $client->setRedirectUri(GOOGLE_LOGIN);
  $client->addScope(GOOGLE_SCOPE);
  $client->setAccessType('offline');        // offline access
  $client->setIncludeGrantedScopes(true);   // incremental auth
  //$client->setApprovalPrompt('force');

  $client->setAccessToken($accessToken);

  //if ($client->isAccessTokenExpired()) {
  //  $client->refreshToken($client->getRefreshToken());
  //  $accessToken = $client->getAccessToken(); // This token has no refresh token.
  //}

  $newCalendar = new Google_Service_Calendar_Calendar();
  $newCalendar->setSummary('Cleopatra (new)');
  $newCalendar->setTimeZone('Europe/Prague');

  $service = new Google_Service_Calendar($client);
  $calendar = $service->calendars->insert($newCalendar);

  if ( $gAuth->updateCalendarID($userID, $calendar->getId()) ) {
    $cUsers->log($userID,'state','Reset Google calendar.');
    cleoRedirect(APPHOME,'google_calendar_reset', 'Uživateli byl vygenerován nový Google kalendář.');
  }
  else {
    $cUsers->log($userID,'error','Google calendar reset fail');
    cleoRedirect(APPHOME,'err_google_crt_clndr', 'Nepodařilo se vytvořit nový Google kalendář.');
  }
}
else {
  cleoRedirect(HOME,'err_login', 'Uživatel není přihlášen!');
}
?>
