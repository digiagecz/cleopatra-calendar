<?php
/*
  GOOGLE LOGIN GATE
  @Author: Martin Krotil <martin@krotil.info>

  Input variables
  $_GET['code']
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/google/gauth.php');
require_once('../../../autoload.php');

$cUsers = new cleoUsers;

if ( !$cUsers->isUserLoggedIn() ) {

  $client = new Google_Client();
  $pathToConfigFile = '../../../core/google/' . GOOGLE_CLIENT_SECRET_FILENAME;
  $client->setAuthConfig($pathToConfigFile);
  $client->setRedirectUri(GOOGLE_LOGIN);
  $client->addScope(GOOGLE_SCOPE);
  $client->setAccessType('offline');        // offline access
  $client->setIncludeGrantedScopes(true);   // incremental auth
  //$client->setApprovalPrompt('force');

  // Je vrácen autentizační kód od Google serveru?
  if ( !isset($_GET['code']) ) {
    // ještě není vrácen autentizační kód, přesměrujeme na Google bránu
    $authURL = $client->createAuthUrl();
    cleoRedirect($authURL);
  }
  else {
    // Byl předán autorizační kód Google serverem, autorizujeme Google přihlášení.
    $client->authenticate($_GET['code']);
    $accessToken = $client->getAccessToken();

    //if ($client->isAccessTokenExpired()) {
    //  $client->refreshToken($client->getRefreshToken());
    //  $accessToken = $client->getAccessToken(); // This token has no refresh token.
    //}

    $oAuth = new Google_Service_Oauth2($client);
    $googleUserInfo = $oAuth->userinfo->get();
    $googleUserID = $googleUserInfo['id'];

    $gAuth = new googleAuth;
    $ownerID = $gAuth->getOwnerID($googleUserID);

    if ( $ownerID !== false ) {

      if ( $gAuth->recordUpdate($googleUserID, $ownerID, $accessToken) ) {
        if ( $cUsers->logUserIn($ownerID, 'google') ) {
          $cUsers->setUserInformation('picture', $googleUserInfo['picture']);
          cleoRedirect(APPHOME);
        }
        else {
          cleoRedirect(HOME,'err_login', 'Chyba při pokusu přihlásit uživatele.');
        }
      }
      else {
        cleoRedirect(HOME,'err_db', 'Chyba při pokusu aktualizovat záznam v CleoDB.');
      }
    }
    else {
      $_SESSION['google-access-token'] = $accessToken;
      cleoRedirect(GOOGLE_CREATE);
    }
  }
}
// TODO GOOGLE ADD
//else {
//  cleoRedirect(GOOGLE_ADD);
//}
else {
  cleoRedirect(APPHOME);
}
?>
