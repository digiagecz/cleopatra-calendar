<?php
/*
  Test login
  @Author: Martin Krotil <martin@krotil.info>
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');

$cUsers = new cleoUsers;
$cUsers->logUserIn(1,'test');

header('Location: ' . filter_var(APPHOME));
?>
