<?php
/*
  AXAJ ENDPOINT - EVENTS MANIPULATION
  @Author: Martin Krotil <martin@krotil.info>

  Input variables
  $_GET['action']
    case "new"
      $_GET['start']
      $_GET['length']
    case "delete"
      $_GET['ID']
    case "update"
      $_GET['ID']
      $_GET['start']
      $_GET['length']
    case "get"
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');

$return_arr = array();

$cUsers = new cleoUsers;
if ( $cUsers->isUserLoggedIn() ) {
  $ownerID = $cUsers->getUserInformation('id');

  if (isset($_GET['action'])) {
    switch ($_GET['action']) {
      case "create":
        if (isset($_GET['start'], $_GET['length'])) {
          $cEvents = new cleoEvents;
          if ($cEvents->newEvent($ownerID, $_GET['start'], $_GET['length'])) {
            $row_array['status'] = 'OK';
          }
          else {
            $row_array['status'] = 'ERROR';
            $row_array['error'] = 'Nepodařilo se aktualizovat záznam v CleoDB. [1]';
          }
        }
        else {
          $row_array['status'] = 'ERROR';
          $row_array['error'] = 'Nebyly předány všechny potřebné informace.';
        }
        array_push($return_arr,$row_array);
      break;
      case "update":
        if (isset($_GET['ID'], $_GET['start'], $_GET['length'])) {
          $cEvents = new cleoEvents;
          if ($cEvents->updateEvent($ownerID, $_GET['ID'], $_GET['start'], $_GET['length'])) {
            $row_array['status'] = 'OK';
          }
          else {
            $row_array['status'] = 'ERROR';
            $row_array['error'] = 'Nepodařilo se aktualizovat záznam v CleoDB. [2]';
          }
        }
        else {
          $row_array['status'] = 'ERROR';
          $row_array['error'] = 'Nebyly předány všechny potřebné informace.';
        }
        array_push($return_arr,$row_array);
      break;
      case "delete":
        if (isset($_GET['ID'])) {
          $cEvents = new cleoEvents;
          if ($cEvents->deleteEvent($ownerID, $_GET['ID'])) {
            $row_array['status'] = 'OK';
          }
          else {
            $row_array['status'] = 'ERROR';
            $row_array['error'] = 'Nepodařilo se aktualizovat záznam v CleoDB. [3]';
          }
        }
        else {
          $row_array['status'] = 'ERROR';
          $row_array['error'] = 'Nebyly předány všechny potřebné informace.';
        }

        array_push($return_arr,$row_array);
      break;
      case "get":
        $cEvents = new cleoEvents;
        $events = $cEvents->getEvents($ownerID);

        $counter = 0;

        if ($events) {
          while ($row = $events->fetch_assoc()) {
            $data['status'][$counter] = 'OK';
            $data['id'][$counter] = $row['ID'];
            $data['start'][$counter] = $row['Start'];
            $data['length'][$counter] = $row['Length'];
            $data['end'][$counter] = date('Y-m-d', strtotime($row['Start'].' +' . $row['Length'] . ' days'));

            $counter++;
          }
        }

        for ($i = 0; $i < $counter; $i++) {
          $simple_array['status'] = $data['status'][$i];
          $simple_array['id'] = $data['id'][$i];
          $simple_array['start'] = $data['start'][$i];
          $simple_array['length'] = $data['length'][$i];
          $simple_array['end'] = $data['end'][$i];

          // counting days
          if ($i > 0) {
            $from = $data['start'][$i - 1];
            $to = $data['start'][$i];
            $diff = $cEvents->dateDiff($from, $to);
            $simple_array['days'] = $diff;
          }
          else {
            $simple_array['days'] = null;
          }

          array_push($return_arr,$simple_array);
        }
      break;
    }
  }
  else {
    $row_array['status'] = 'ERROR';
    $row_array['error'] = 'Nebyly předány všechny potřebné informace.';
    array_push($return_arr,$row_array);
  }
}
else {
  $row_array['status'] = 'ERROR';
  $row_array['error'] = 'Přístup odepřen. Uživatel není přihlášen.';
  array_push($return_arr,$row_array);
}

header('Content-Type: application/json');
echo json_encode($return_arr);
?>
