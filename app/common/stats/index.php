<?php
/*
  AXAJ ENDPOINT - STATS MANIPULATION
  @Author: Martin Krotil <martin@krotil.info>

  Input variables
  $_GET['action']
    case "get"
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');

$return_arr = array();

$cUsers = new cleoUsers;
if ( $cUsers->isUserLoggedIn() ) {
  $ownerID = $cUsers->getUserInformation('id');

  if (isset($_GET['action'])) {
    switch ($_GET['action']) {
      case "get":
        $cEvents = new cleoEvents;
        $stats = $cEvents->getStats($ownerID);

        if ($stats) {
          while ($row = $stats->fetch_assoc()) {
            $data['status'] = 'OK';
            $data['agreement'] = $row['Agreement'];
            $data['auto-period'] = $row['AutoPeriod'];
            $data['length-period'] = $row['LengthPeriod'];
            $data['auto-cycle'] = $row['AutoCycle'];
            $data['length-cycle'] = $row['LengthCycle'];
            $data['pregnancy'] = $row['Pregnancy'];
          }
        }

        array_push($return_arr,$data);
      break;

      case "set":
        $cEvents = new cleoEvents;

        if (isset($ownerID, $_GET['autoMensesCount'], $_GET['mensesLength'], $_GET['autoCycleCount'], $_GET['cycleLength'], $_GET['pregnancy'])) {
          if ( $cEvents->setStats($ownerID, $_GET['autoMensesCount'], $_GET['mensesLength'], $_GET['autoCycleCount'], $_GET['cycleLength'], $_GET['pregnancy']) ) {
            if ( $cEvents->countStats($ownerID) ) {
              $data['status'] = 'OK';
              array_push($return_arr,$data);
            }
            else {
              $row_array['status'] = 'ERROR';
              $row_array['error'] = 'Nepodařilo se vypočítat statistiky.';
              array_push($return_arr,$row_array);
            }
          }
          else {
            $row_array['status'] = 'ERROR';
            $row_array['error'] = 'Předané informace nejsou ve správném formátu.';
            array_push($return_arr,$row_array);
          }
        }
        else {
          $row_array['status'] = 'ERROR';
          $row_array['error'] = 'Nebyly předány všechny potřebné informace.';
          array_push($return_arr,$row_array);
        }
      break;
    }
  }
  else {
    $row_array['status'] = 'ERROR';
    $row_array['error'] = 'Nebyly předány všechny potřebné informace.';
    array_push($return_arr,$row_array);
  }
}
else {
  $row_array['status'] = 'ERROR';
  $row_array['error'] = 'Přístup odepřen. Uživatel není přihlášen.';
  array_push($return_arr,$row_array);
}

header('Content-Type: application/json');
echo json_encode($return_arr);
?>
