<?php
// APP home page template
// Author: Martin Krotil

require_once('../core/settings.php');
require_once('../core/cleo_users.php');
require_once('../core/cleo_events.php');
require_once('../core/google/gauth.php');

header('Content-Type: text/html; charset=utf-8');

$cUsers = new cleoUsers;


if (HOST == "localhost") { // set specific user
  $userID = "101";
}else{
  if (!$cUsers->isUserLoggedIn()) {
      cleoRedirect(HOME,'err_login', 'Přístup zamítnut. Uživatel není přihlášen.');
  }
  elseif ( !$cUsers->isConsentGiven() ) {
    cleoRedirect(AGREEMENT_PAGE);
  }
  elseif ( !$cUsers->hasValidLicense() ) {
    cleoRedirect(LICENSING_PAGE);
  }
  $userID = $cUsers->getUserInformation('id');            //uživatelovo ID z databáze
}
if(isset($userID)){
  getMainHeader("Kalendář | Cleopatra",1,$cUsers->isServiceLoggedIn('google'));
  require_once('../core/tmp/navigation.php');
?>

  <section id="events" class="bg-secondary cleo-util is--top-head-padding">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h1 class="mb-1">Tvůj osobní kalendář Cleopatra</h1>
          <p class="mb-1">přehledné nastavení tvých cyklů</p>
          <hr class="dark">
        </div>
      </div>
    </div>
  </section>
  <section class="bg-primary">
    <div class="container">
      <div class="row">
        
        <div class="col-12 col-lg-6">
          <div class="inline-heading mb-1">
            <h3 class="mb-0">Přehled tvých cyklů</h3><div id="js-create-new" class="btn btn-main btn-main-shine btn-smaller"><span>+</span> Přidat cyklus</div>
          </div>

          <div class="cleo-event" id="js-event-wrapper"></div>
          <div class="cleo-event is--controls mb-lg-0">
            <a href="https://calendar.google.com" target="_blank" id="muj-kalendar" class="btn btn-info btn-smaller">Můj kalendář</a>
            <span id="js-show-more-groups" class="cleo-event__more cleo-util is--pointer btn btn-info-outline btn-smaller">
              <span class="cleo-icon is--menu3"></span> Načíst další
            </span>
            <span id="js-show-fewer-groups" class="cleo-event__more cleo-util is--pointer btn btn-info-outline btn-smaller">
              <span class="cleo-icon is--menu4"></span> Skrýt další
            </span>
          </div>


        </div>
        <div class="col-12 col-lg-6 col-xl-5 col-xxl-4 ml-auto box">
          <div class="">
            <div class="nastaveni">
              <h4>Nastavení cyklů</h4>
              <span id="js-open-setting-button" class="btn btn-info cleo-icon is--cog" title="nastavení cleopatry"></span>
            </div>
            <div class="border-top">
              <div class="cleo-stats__row">
                <span class="cleo-stats__header">Délka menstruace</span>
                <div class="cleo-stats__content" id="js-length-period">5 dnů</div>
              </div>
              <div class="cleo-stats__row">
                <span class="cleo-stats__header">Délka cyklu</span>
                <div class="cleo-stats__content" id="js-length-cycle">28 dnů</div>
              </div>
            </div>
            <div class="border-top">
              <div class="cleo-stats__row">
                <span class="cleo-stats__header">Kalendář máš k dispozici do</span>
                <div class="cleo-stats__content" ><?php if ($cUsers->whenLicenseExpires()) { echo $cUsers->whenLicenseExpires();}?></div>
              </div>
              <div class="cleo-stats__row">
                <a href="pages/licensing" class="btn btn-info-outline btn-smaller mt-1">Prodloužit platnost Cleopatry</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="calendar-preview">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <div class="inline-heading">
            <h3 class="mb-0">Náhled tvého kalendáře</h3>
            <a href="https://calendar.google.com" target="_blank" id="muj-kalendar" class="btn btn-main btn-main-shine btn-smaller mb-0">Můj kalendář</a>
          </div>
          <p class="italic-gray">zjednodušený, bez barev pro jednotlivé cykly</h3>

          <?php
            $gAuth = new googleAuth;
            $calendarID = $gAuth->getOwnerCalendarID($userID);
            if ($calendarID) {
              echo '<iframe class="cleo-google-iframe" src="https://calendar.google.com/calendar/embed?src=' . $calendarID . '&ctz=Europe%2FPrague&amp;color=%237A367A" frameborder="0" scrolling="no"></iframe>';
            }
            else {
              echo '<p class="text-center cleo-util is--tiny-margined">Náhled Google kalendáře není dostupný</p>';
            }
          ?>

        </div>
      </div>
    </div>
  </section>

  <section class="bg-primary">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">

          <h3 class="link" id="jak-zacit">Jak začít?</h3>
          <ul class="arrow">
            <li>Zadej minimálně <?php echo MIN_CYCLES_TO_CALCULATE;?> termíny své poslední menstruace. Kliknutím na <strong>+ PŘIDAT CYKLUS</strong>, výběrem data a potvrzením stiskem <strong>AKTUALIZACE KALENDÁŘE</strong>. Zároveň můžeš upravit délku své menstruace.</li>
            <li>Po třetím zadaném cyklu se ti automaticky spustí výpočet tvého osobního kalendáře Cleopatra. Aktualizace může trvat i několik desítek sekund až minut, tak prosím nezavírej aktuální okno - kalendář by se ti nevygeneroval celý.</li>
          </ul>

        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h3>Co dál?</h3>
          <ul class="arrow">
            <li>Kalendář Cleopatra se Ti propíše do Tvého osobního Google kalendáře (MŮJ KALENDÁŘ), který běžně používáš pod tímto účtem.</li>
            <li>Funguje stejně jako jiné sdílené kalendáře, to znamená, že se Ti zobrazí na všech zařízeních, kde svůj kalendář používáš (počítač, notebook, tablet, chytrý telefon apod.).</li>
            <li>Je barevný, s vyobrazením jednotlivých fází a můžeš jej skrýt či sdílet (např. s partnerem :).</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="bg-primary">
  <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h3>Cena a platba</h3>
          <ul class="arrow">
            <li>Kalendář Cleopatra máš k dispozici 30 dní zdarma.</li>
            <li>Po té Ti přijde emailem nabídka, že si jej můžeš <a href="<?php echo LICENSING_PAGE;?>">Zakoupit</a> a získat tak plán svých cyklů na 6 měsíců dopředu. </li>
            <li>Pokud nemáš zájem a neuhradíš předplatné, Tvůj účet bude zrušen a nemusíš se o něj více starat.</li>
          </ul>

        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h3>Je potřeba se vracet</h3>
            <ul class="arrow">
              <li>A přidat aktuální termíny menstruace, jinak kalendář zobrazuje zastaralé údaje a neodpovídá Tvým cyklům a to by byla opravdu škoda.</li>
              <li>Nepřihlásíš-li se, tak po 30ti dnech od poslední menstruace Ti přijde e-mail s připomenutím potřebné aktualizace kalendáře, která Ti zabere maximálně minutku :).</li>
            </ul>
            
          <p>Více v informacích o&nbsp;Cleopatře <a href="<?php echo INFO_PAGE;?>"><span class="cleo-icon is--info"></span></a>.</p>
          </div>
        </div>
      </div>
  </section>

<?php
  require_once('../core/tmp/footer.php');
?>

<?php
  if (isset($_GET['dialog'])) {
    switch ($_GET['dialog']) {
      case "STATUS_OK":
        $dialogHeader = "Vše proběhlo v pořádku";
        $dialogContent = '<h2 class="text-center">Platba proběhla v pořádku.</h2>
        <p class="text-center">Zaplacená částka: ' . $_GET['value'] . ' ' . $_GET['currency'] . '</p>';
        break;

      case "STATUS_OK2":
        $dialogHeader = "Vše proběhlo v pořádku";
        $dialogContent = '<h2 class="text-center">Platba byla již zaevidována.</h2>
        <p class="text-center">Zaplacená částka: ' . $_GET['value'] . ' ' . $_GET['currency'] . '</p>';
        break;

      case "STATUS_CANCELED":
        $dialogHeader = "Vyskytnul se problém";
        $dialogContent = '<h2 class="text-center">Vaše platba byla zrušena.</h2>';
        break;

      case "STATUS_ERROR":
        $dialogHeader = "Vyskytnul se problém";
        $dialogContent = '<h2 class="text-center">Při zpracování Vaši platby došlo k chybě.</h2>
        <p class="text-center">Konaktujte prosím technickou podporu na info@thepay.cz</p>';
        break;

      case "STATUS_UNDERPAID":
        $dialogHeader = "Vyskytnul se problém";
        $dialogContent = '<h2 class="text-center">Vaše platba byla zaplacena jen částečně.</h2>
        <p class="text-center">Nebylo proto možné prodloužit vaši licenci.</p>';
        break;

      case "STATUS_WAITING":
        $dialogHeader = "Vyskytnul se problém";
        $dialogContent = '<h2 class="text-center">Vaše platba čeká na potvrzení.</h2>
        <p class="text-center">Platba proběhla v pořádku, ale čeká na provedení a potvrzení od poskytovatele platení metody.</p>';
        break;

      default:
        $dialogHeader = "Vyskytnul se problém";
        $dialogContent = '<h2 class="text-center">Neznámý stav platby</h2>
        <p class="text-center">Konaktujte prosím naši technickou podporu.</p>';
    }
?>
  <!-- Dialog modal -->
  <div class="modal fade" id="dialog-modal" tabindex="-1" role="dialog" aria-labelledby="js-dialog-modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><?php echo $dialogHeader;?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo $dialogContent;?>
        </div>
        <div class="modal-footer cleo-util is--flex-centered is--tiny-margined">
          <span class="btn btn-success" data-dismiss="modal">OK</span>
        </div>
      </div>
    </div>
  </div>

  <script>
    var showDialogModal = function() {
      $('#dialog-modal').modal('show');
    }

    if (document.readyState!='loading') showDialogModal();
    else if (document.addEventListener) document.addEventListener('DOMContentLoaded', showDialogModal);
    else document.attachEvent('onreadystatechange', function() {
      if (document.readyState=='complete') showDialogModal();
    });
  </script>
<?php
  }
?>

  <!-- Modal with event form -->
  <div class="modal fade" id="event-modal" tabindex="-1" role="dialog" aria-labelledby="js-event-modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="js-event-modalLabel">Úprava menstruace</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body cleo-in-modal">

          <form id="js-event-form">
            <input type="hidden" id="js-input-id" name="input-id">
            <input type="hidden" id="js-input-start" name="input-start" />

            <div class="cleo-in-modal__datepicker">
              <div type="text" id="js-event-datepicker"></div>
            </div>

            <label for="js-input-length">Délka menstruace:</label><br />
            <input type="hidden" id="js-input-length" name="input-length" />

            <div id="js-slider" class="cleo-in-modal__slider">
              <div id="js-slider-handler" class="ui-slider-handle"></div>
            </div>


          </form>

        </div>
        <div class="modal-footer cleo-util is--flex-centered is--tiny-margined">
          <span id="js-button-create-new" class="btn btn-success" data-dismiss="modal">Aktualizace kalendáře</span>
          <span id="js-button-update" class="btn btn-success" data-dismiss="modal">Ulož změny</span>
          <span id="js-button-delete" class="btn btn-danger" data-dismiss="modal">Smaž termín</span>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal with settings stats form -->
  <div class="modal fade" id="settings-modal" tabindex="-1" role="dialog" aria-labelledby="settings-modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="settings-modalLabel">Nastavení kalendáře</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body cleo-in-modal">

          <form id="js-stats-form">
            <fieldset>
              <input id="js-automatic-period" type="checkbox" name="automatic-period" />
              <label for="js-automatic-period">Automatický výpočet délky menstruace</label>
            </fieldset>
            <fieldset id="js-manual-period-box">
              <input type="hidden" id="js-manual-period-field" />

              <!-- slider -->
              <div id="js-manual-period-slider" class="cleo-in-modal__slider">
                <div id="js-manual-period-slider-handler" class="ui-slider-handle"></div>
              </div>
              <!-- slider -->

            </fieldset>
            <fieldset>
              <input id="js-automatic-cycle" type="checkbox" name="automatic-cycle" />
              <label for="js-automatic-cycle">Automatický výpočet délky menstruačního cyklu</label>
            </fieldset>
            <fieldset id="js-manual-cycle-box">
              <input type="hidden" id="js-manual-cycle-field" />

              <!-- slider -->
              <div id="js-manual-cycle-slider" class="cleo-in-modal__slider">
                <div id="js-manual-cycle-slider-handler" class="ui-slider-handle"></div>
              </div>
              <!-- slider -->

            </fieldset>
            <fieldset>
              <input id="js-pregnancy-checkbox" type="checkbox" name="pregnancy" />
              <label for="js-pregnancy-checkbox">Jsem těhotná a nechci generovat události</label>
            </fieldset>
            <?php if ($cUsers->isServiceLoggedIn('google')) { ?><fieldset class="cleo-util is--tiny-margined">
              <p class="text-center">
                <span class="like--a js-update-calendars">Aktualizuj Google kalendář</span>
              </a>
            </fieldset><?php } ?>
            <fieldset class="cleo-util is--tiny-margined">
              <p class="text-center">
                <span id="js-show-confirm" class="like--a">Smazat kalendář</span>
              </p>
              <p id="js-confirm" class="text-center">
                <a href="<?php echo REMOVE_ALL_PAGE;?>" class="btn btn-danger">Opravdu smazat kalendář</a>
                <span class="btn btn-warning" id="js-hide-confirm">Storno</span>
              </p>
            </fieldset>
          </form>

        </div>
        <div class="modal-footer">
          <span id="js-stats-save-button" class="btn btn-success" data-dismiss="modal">Ulož změny</span>
          <span id="js-stats-discard-button" class="btn btn-warning" data-dismiss="modal">Zavřít nastavení</span>
        </div>
      </div>
    </div>
  </div>

<?php
getMainFooter(1,1);
}
?>
