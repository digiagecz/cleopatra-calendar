<?php
/*
  remove all data GATE
  @Author: Martin Krotil <martin@krotil.info>
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');


$cUsers = new cleoUsers;
if ( $cUsers->isUserLoggedIn() ) {
  $userID = $cUsers->getUserInformation('id');

  $cEvents = new cleoEvents;
  if ( $cUsers->deleteAccount($userID) ) {
    cleoRedirect(LOGOUT . '?after=delete');
  }
  else {
    cleoRedirect(APPHOME,'err_delete_user', 'Nepodařilo se smazat všechny záznamy o uživateli.');
  }
}
else {
  cleoRedirect(HOME,'err_login', 'Uživatel není přihlášen!');
}
?>
