<?php
/*
  logOut GATE
  @Author: Martin Krotil <martin@krotil.info>
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');

$cUsers = new cleoUsers;
$cUsers->destroyAll();

if (isset($_GET['after'])) {
  switch ($_GET['after']) {
    case 'delete':
      header('Location: ' . filter_var(HOME . '?after=delete'));
      break;
  }
} else {
  header('Location: ' . filter_var(HOME));
}
?>
