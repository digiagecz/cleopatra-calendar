<?php
/*
  PAYMENT GATE
  @Author: Martin Krotil <martin@krotil.info>
*/

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');
require_once('../../../vendor/thepay/TpReturnedPayment.php');
require_once('../../../core/productDb.php');
//require_once('../../../core/payConfig.php');

$cUsers = new cleoUsers;

$userID = $cUsers->getUserInformation('id');            //uživatelovo ID z databáze

$cUsers->getPayConfigFile($userID);                     //vložení správného pay config file

function redirectHelper ($parameters) {
  header('Location: ' . filter_var(APPHOME . $parameters, FILTER_SANITIZE_URL));
}

function redirectHelperStatus ($parameters,$redirState) {
  header('Location: ' . filter_var(APPHOME . $parameters, FILTER_SANITIZE_URL), true, $redirState);
}

//$cUsers->log($userID,'test','cleoUserClass');
/* if (!$cUsers->isUserLoggedIn()) {
  cleoRedirect(HOME,'err_login', 'Přístup zamítnut. Uživatel není přihlášen.');
}
elseif ( !$cUsers->isConsentGiven() ) {
  cleoRedirect(AGREEMENT_PAGE);
} else {
  $userID = $cUsers->getUserInformation('id');
 */
  /// Create an instance of TpReturnedPayment and give it our
  /// configuration
  $returnedPayment = new TpReturnedPayment(new PayConfig());
  /// Verify signature in try block, TpReturnedPayment throws exception
  /// if signature is wrong.

  //$cUsers->log($userID,'test','returnPayDef');

  try{
    /// Verify the payment signature
    $returnedPayment->verifySignature();

    //$cUsers->log($userID,'test','signatVerified');

    $merchantData = explode("-",$_GET['merchantData']);
    $userID = $merchantData[0];            // ID uživatele
    $coupon = $merchantData[1];            // kupon
    $quantity = $merchantData[2];            // počet let
    $price = $_GET['value'];
    switch($returnedPayment->getStatus()){
      case TpReturnedPayment::STATUS_OK:
       // header("HTTP/1.1 200 OK");

      // $cUsers->log($userID,'test','statusOK');

        if ($cUsers->addPayment($userID,$_GET['paymentId'],$coupon,$price)) {  //$cUsers->addPayment($userID,$_GET['paymentId']) 
          if ($cUsers->giveLicense($userID, (PAYMENT_DAYS_PAID*$quantity))) {       //user id = $_GET['merchantData']
            $cUsers->log($userID,'pay','STATUS_OK');
            $param = '?dialog=STATUS_OK&value=' . $_GET['value'] . '&currency=' . $_GET['currency'];
            redirectHelperStatus($param, 302);
          }
          else {
            $cUsers->log($userID,'error','cUsers->giveLicense() fail');
            cleoRedirect(APPHOME,'err', 'Platba se nezdařila.');
          }
        }
        else {
          $param = '?dialog=STATUS_OK2&value=' . $_GET['value'] . '&currency=' . $_GET['currency'];
          redirectHelper($param);
        }
        
        break;
      case TpReturnedPayment::STATUS_CANCELED:
        $cUsers->log($userID,'pay','STATUS_CANCELED');
        header('Location: ' . filter_var(APPHOME . '', FILTER_SANITIZE_URL));
        redirectHelper('?dialog=STATUS_CANCELED');
        break;
      case TpReturnedPayment::STATUS_ERROR:
        $cUsers->log($userID,'pay','STATUS_ERROR');
        redirectHelper('?dialog=STATUS_ERROR');
        break;
      case TpReturnedPayment::STATUS_UNDERPAID:
        $cUsers->log($userID,'pay','STATUS_UNDERPAID');
        redirectHelper('?dialog=STATUS_UNDERPAID');
        break;
      case TpReturnedPayment::STATUS_WAITING:
        $cUsers->log($userID,'pay','STATUS_WAITING');
        redirectHelper('?dialog=STATUS_WAITING');
        break;
      default:
        $cUsers->log($userID,'pay','STATUS_UNKNOWN');
        redirectHelper('?dialog=STATUS_UNKNOWN');
    }
  }  catch (Exception $e){
    $cUsers->log($userID,'pay','EXCEPTION');
    cleoRedirect(APPHOME,'err', 'Nepodařilo se ověřit podpis požadavku.');
  } catch(TpMissingParameterException $e){
    $cUsers->log($userID,'pay','MISSING_PARAMETERS');
    cleoRedirect(APPHOME,'err', 'Chybí některý z povinných parametrů.');
  }
//}
?>
