<?php
/*
  PAGE - LICESING PAGE
  @Author: Martin Krotil <martin@krotil.info>
  @Edit: Honza Kouba <info@digiage.cz>
*/

header('Content-Type: text/html; charset=utf-8');

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');


$cUsers = new cleoUsers;

if (HOST == "localhost") { // set specific user
  $userID = "101";
}else{
  if (!$cUsers->isUserLoggedIn()) {
    cleoRedirect(HOME,'err_login', 'Přístup zamítnut. Uživatel není přihlášen.');
  }
  elseif ( !$cUsers->isConsentGiven() ) {
    cleoRedirect(AGREEMENT_PAGE);
  } 

  $userID = $cUsers->getUserInformation('id');            //uživatelovo ID z databáze
}


require_once('../../../vendor/thepay/helpers/TpDivMerchantHelper.php');
require_once('../../../core/productDb.php');


$cUsers->getPayConfigFile($userID);                     //vložení správného pay config file


// ThePay
$payment = new TpPayment(new PayConfig());

// nastavení ceny pro validní a nevalidní/nezadaný kupon
// vložení kuponu do merchant data
//zkusit dát do funkce do cleo users?

if(isset($_POST['quantity'])){
  $quantity = $_POST['quantity'];
  $base_price = PAYMENT_NORMAL*$quantity;
  if($cUsers->checkCouponValidity($_POST['sale_coupon'])){
    $sale_constant = (100 - (PAYMENT_SALE_YEARS[$quantity-1] + PAYMENT_SALE_COUPON[$quantity-1]))/100;
    $payment->setValue($base_price*$sale_constant);      // 2 roky + kupon = 800*2
    $coupon_validity = 1;                     //kupon validní
    $payment->setMerchantData($userID."-".$_POST['sale_coupon']."-".$quantity);       // vložit přihlášeného uživatele + kupon do merchantData aby bylo spárování zákazníka s platbou
  }else{
    $sale_constant = (100 - (PAYMENT_SALE_YEARS[$quantity-1]))/100;
    $payment->setValue($base_price*$sale_constant);
    $coupon_validity = 0;                     //kupon NEvalidní
    $payment->setMerchantData($userID."-0-".$quantity);
  }
}else{
    $payment->setValue(PAYMENT_NORMAL);
    $coupon_validity = 2;                     //kupon nezadán
    $quantity = 1;
    $payment->setMerchantData($userID."-0-".$quantity);
    
}



$payment->setReturnUrl(PAYMENT.'/');                  //bez toho probiha redirect z '' -> '/' => vytváří 301 navíc
$tpHelper = new TpDivMerchantHelper($payment);

getMainHeader("Platba a Všeobecné podmínky | Cleopatra",3,$cUsers->isServiceLoggedIn('google'));

/* if (!$cUsers->isUserLoggedIn()) {
  cleoRedirect(HOME,'err_login', 'Přístup zamítnut. Uživatel není přihlášen.');
} */

require_once('../../../core/tmp/navigation.php');
?>

<section class="bg-secondary cleo-util is--top-head-padding">
  <div class="calendar-graphics calendar-right perspective">
    <img src="../../../img/calendar.svg" alt="">
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8 col-lg-6 mx-auto text-center">
        <h1 >Předplatné Cleopatry</h1>
        <hr class="dark mb-2">
        <?php if (!$cUsers->hasValidLicense()) {?>
          <div class="alert alert-warning mb-0" role="alert">
            <span class="cleo-icon is--warning"></span> Platnost tvé licence Cleopatra vypršela a je potřeba jí znovu <a href="#platba">aktivovat</a>.
          </div>
        <?php } else { ?>
          <div class="alert alert-primary alert-primary-lighter mb-0" role="alert">
          <span class="cleo-icon is--info"></span> Osobní kalendář Cleopatra máš k&nbsp;dispozici do&nbsp;<strong><?php if ($cUsers->whenLicenseExpires()) { echo $cUsers->whenLicenseExpires();}?></strong>
          </div>
        <?php } ?>

<!--         <div class="text-center">
          <a href="#platba" class="btn btn-main btn-main-shine mt-2">Předplatit si Cleopatru</a>
        </div> -->
      </div>
    </div>
  </div>
</section>
<section class="bg-primary">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-10 col-lg-7 mx-auto text-center">
<!--         <h2 class="link" id="platba">Prodloužení platnosti Cleopatry</h2>
        <p>ve dvou snadných krocích</p>
        <hr class="dark mb-3"> -->
        
        <div class="text-justify position-relative">
          <span class="circle-number">1</span> <h3 class="mb-2 link" id="platba">Předplatné a cena</h3>
        </div>
        <div class="text-justify position-relative box-left-line">
          <form action="#platba" method="post">
            <div class="mb-1 form-inline">
              <label for="quantity">Délka předplatného ( <strong>max.<?php echo PAYMENT_QUANTITY_MAX; ?> roky</strong> ):</label> &nbsp; &nbsp; <input class="form-control" type="number" name="quantity" value=<?php echo $quantity; ?> min="<?php echo PAYMENT_QUANTITY_MIN; ?>" max="<?php echo PAYMENT_QUANTITY_MAX; ?>"> &nbsp; &nbsp; <input class="form-control btn btn-info btn-smaller" type="submit" name="send" value="Přepočítat cenu" >
            </div>
            <div class="mb-2">
               Cena celkem: &nbsp; &nbsp; <h4 class="d-inline-block"><?php echo $payment->getValue(); ?> Kč  <?php if(($coupon_validity == 1) || ($coupon_validity == 0 && $quantity > 1)) { echo '&nbsp; &nbsp; <span class="before-sale">'.($quantity*PAYMENT_NORMAL).' Kč</span>'; } ?></h4>
            </div>
            <p class="italic-gray mb-1">Pokud vlastníš Slevový kupón, zadej ho zde:</p>
            <div class="mb-4 form-inline">
              <input class="form-control" type="text" placeholder="Slevový kupón" name="sale_coupon"> &nbsp; &nbsp; <input class="form-control btn btn-info-outline btn-smaller" type="submit" name="send" value="Ověřit kupón">
              <?php
                if($coupon_validity == 0 && $_POST['sale_coupon'] != "") {   //kupon NEvalidní, ale není prázdný
                  echo '<p class="alert alert-danger mt-1">Kupon <strong>'.$_POST['sale_coupon'].'</strong> není platný. Prosím překontroluj jej.</p>';
                }  
                if($coupon_validity == 1) {   //kupon NEvalidní  
                  echo '<p class="alert alert-success mt-1">Gratuluji, tvůj kupón je platný! Tvoje cena je pouze <strong>'.$payment->getValue().' Kč</strong>. Ušetříš tedy <strong style="text-decoration:underline;">'.(($quantity*PAYMENT_NORMAL) - $payment->getValue()).' Kč</strong></p>';
                }  
              ?>
            </div>
          </form>
        </div>
        <div class="text-justify">  
          <div class="position-relative"><span class="circle-number">2</span> <h3 class="mb-0">Platba</h3></div>
          <p class="italic-gray mb-2">Vyber si oblíbený způsob platby</p>
        </div>
        <div class="box-left-line" style="height: 400px;">
          <div class="payment-buttons">
            <!--<h2>Vyberte si platební metodu:</h2>-->
              <?php
                ///Do we have helper object?
                if (isset($tpHelper)) {
                  ///Now render thepay payment buttond
                  echo $tpHelper->render();
                } else {
                  /// We don't have instance of iframeHelper, there was no
                  /// product id in Get, or id was wrong
                  echo '<p>Je nám to líto, ale požadovaný produkt nebyl v našem obchodě nalezen.</p>';
                }
              ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-8 col-lg-6 mx-auto text-left"> 
        <h4 class="mb-2 mt-4">Poznámky k platbě</h4>
        <div class="text-justify">
          <ul class="arrow mt-0 mb-0">
            <li>
              Po zaplacení ti přijde emailem informace, že platba byla v pořádku uhrazena.
            </li>
            <li>
              Pokud potřebuješ <strong>doklad o zaplacení</strong> napiš na <a href="mailto:<?php echo EMAIL_INFO; ?>"><?php echo EMAIL_INFO; ?></a>, obratem ti jej zašlu na tvůj email. 
            </li>
            <li class="mb-0">
            S <strong>dalším přihlášením</strong> na <a href="<?php echo HOME; ?>"><?php echo HOME; ?></a> se ti aktualizuje kalendář a zobrazí osobní cykly na dalších 6 měsíců ve tvém Google kalendáři, abys mohla plánovat v souladu se svými cykly a jednotlivými fázemi.
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

</section>
<section>
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8 col-lg-6 mx-auto text-left">
      <h4 class="mb-2">Možnost smazat kalendář</h4>
        <div class="text-justify mb-2">
          <ul class="arrow mt-0">
            <li>
              Pokud si přeješ svůj osobní kalendář Cleopatra smazat (a již nemáš přístup do svého nastavení), můžeš to udělat kdykoli i prostřednictvím <a href="<?php echo FEEDBACK_FORM; ?>" target="_blank">formuláře</a> ke Zpětné vazbě. Tím dojde ke smazání všech osobních údajů na <?php echo HOME; ?>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- <section>
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8 col-lg-6 mx-auto text-left">
      <h4 class="mb-2">Poznámky k předplatnému</h4>
        <div class="text-justify mb-2">
          <ul class="arrow mt-0">
            <li>
              Máš-li zájem, můžeš využít <a href="#platba">předplatné</a> on-line kalendáře Cleopatra za<strong> <?php echo PAYMENT_NORMAL; ?>,- Kč</strong>/rok. 
            </li>
            <li>
              Pokud jsi si kalendář Cleopatra oblíbila, můžeš také nově využít předplatného na <strong>více let</strong> dopředu a zajistit si tak <strong>výhodnou cenu</strong>.
            </li>
          </ul>
        </div>
        <div class="text-center">
          <a href="#platba" class="btn btn-main btn-main-shine">Předplatit si Cleopatru</a>
        </div>
      </div>
    </div>
  </div>
</section> -->

<?php
  require_once('../../../core/tmp/footer.php');
  getMainFooter(3,0);
?>
