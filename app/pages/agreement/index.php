<?php
/*
  PAGE - AGREEMENT PAGE
  @Author: Martin Krotil <martin@krotil.info>
*/

header('Content-Type: text/html; charset=utf-8');

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');
require_once('../../../core/cleo_events.php');

$cUsers = new cleoUsers;

if (!$cUsers->isUserLoggedIn()) {
  cleoRedirect(HOME,'err_login', 'Přístup zamítnut. Uživatel není přihlášen.');
}

if ( $cUsers->isConsentGiven() ){
  cleoRedirect(APPHOME);
}

// TODO to POST
if ( isset($_GET["agreement"]) && ($_GET["agreement"] == 1) ) {
  if ( $cUsers->giveConsent($cUsers->getUserInformation('id')) ) {
    cleoRedirect(APPHOME);
  }
}
else {
  getMainHeader("Udělení souhlasu | Cleopatra",3,$cUsers->isServiceLoggedIn('google'));


  require_once('../../../core/tmp/navigation.php');
  require_once('../../../core/tmp/header.php');
?>

  <!-- Modal with agreement form -->
  <div class="modal fade" id="agreement-modal" tabindex="-1" role="dialog" aria-labelledby="agreement-modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog cleo-util is--info-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="agreement-modalLabel">Souhlas s pravidly</h5>
        </div>
        <div class="modal-body">

          <p>Souhlasím s <a href="<?php echo TERMS_PAGE;?>" title="Odkaz na Všeobecné podmínky">Všeobecnými obchodními podmínkami a Zásadami ochrany osobních údajů online kalendáře Cleopatra</a>. Zejména, aby údaje o mých termínech a délce menstruačního cyklu byly použity pouze pro výpočet jednotlivých fází kalendáře Cleopatra a uloženy do mého osobního Google kalendář pomocí přihlášení k mému Google účtu. Tímto účtem a jeho zabezpečením jsou chráněny mé osobní údaje.</p>

        </div>
        <div class="modal-footer">
          <form action="<?php echo AGREEMENT_PAGE;?>" method="get">
            <input type="hidden" name="agreement" id="agreement" value="1" />
            <input type="submit" class="btn btn-success" value="Souhlasím" />
          </form>
        </div>
      </div>
    </div>
  </div>

<?php
  require_once('../../../core/tmp/footer.php');
?>

  <script src="../../../js/agreements.js"></script>

<?php
getMainFooter(3,0); } ?>
