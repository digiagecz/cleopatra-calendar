<?php
/*
  PAGE - INFO PAGE
  @Author: Martin Krotil <martin@krotil.info>
*/

header('Content-Type: text/html; charset=utf-8');

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');

$cUsers = new cleoUsers;

  getMainHeader("Často kladené otázky | Cleopatra",3,$cUsers->isServiceLoggedIn('google'));

  require_once('../../../core/tmp/navigation.php');
?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-10 mx-auto text-center">
          <h1 >Často kladené otázky</h1>
          <hr class="dark">
        </div>
      </div>
    </div>
</section>
  <section class="bg-primary">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-10 mx-auto faq-flex">
					<!-- Accordion -->
					<div id="accordionExample" class="accordion">

							<?php 

                $lines = file("faq-xaopjeksalen2389U.txt",  FILE_IGNORE_NEW_LINES);
                $faqNum = 0;

                foreach($lines as $line) {
                  $parts = explode(";", $line);
                  $faqNum ++;
							?>	
										
								<div class="card same-shadow">
									<div class="card-header same-shadow">
										<strong class="mb-0" id="heading<?php echo $faqNum; ?>">
											<a href="#" data-toggle="collapse" data-target="#collapse<?php echo $faqNum; ?>" aria-expanded="<?php if(isset($_GET['q']) && $_GET['q'] == $faqNum) echo 'true'; else echo 'false'; ?>" aria-controls="collapse<?php echo $faqNum; ?>" class="d-block position-relative <?php if(isset($_GET['q']) && $_GET['q'] != $faqNum) echo 'collapsed'; ?> collapsible-link py-2"><?php echo $parts[0]; ?>
												<div class="plus-back"></div>
											</a>
                    </strong>
									</div>
									<div id="collapse<?php echo $faqNum; ?>" aria-labelledby="heading<?php echo $faqNum; ?>" data-parent="#accordionExample" class="collapse <?php if(isset($_GET['q']) && $_GET['q'] == $faqNum) echo 'show'; ?>">
									<div class="card-body">
										<p class="text-justify"><?php echo $parts[1]; ?></p>
									</div>
									</div>
								</div>
							
								
							<?php 
								}
							?>

					</div>
          <p class="mt-4 text-center"><a href="<?php echo GOOGLE_LOGIN; ?>" class="btn btn-info js--notice mb-1">Vyzkoušej měsíc ZDARMA</a> &nbsp; <a href="/#vyhody" class="btn btn-info-outline mb-1">Výhody Cleopatry</a></p>
				</div>
      
      </div>
    </div>
  </section>

<?php
  require_once('../../../core/tmp/footer.php');

  if (isset($_GET['after'])) {
    require_once('../../../core/cleo_dialog_modal.php');
  }

  getMainFooter(3,0);
?>
