<?php
/*
  TERM & CONDITIONS PAGE
  @Author: Martin Krotil <martin@krotil.info>
*/

header('Content-Type: text/html; charset=utf-8');

require_once('../../../core/settings.php');
require_once('../../../core/cleo_users.php');

$cUsers = new cleoUsers;

  getMainHeader("Obchodní podmínky | Cleopatra",3,$cUsers->isServiceLoggedIn('google'));

  require_once('../../../core/tmp/navigation.php');
?>

<section class="">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 mx-auto">
        <h1 class="text-center">Všeobecné obchodní podmínky a&nbsp;Zásady ochrany osobních údajů online kalendáře Cleopatra</h1>
      </div>
    </div>
  </div>
</section>
<section class="bg-primary">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 mx-auto">
        <h2 class="mt-0 mb-1">1 Obecně</h2>
        <p class="text-justify">
        1.1 Webové stránky <a href="<?php echo HOME; ?>"><b><?php echo HOME; ?></b></a>
        a&nbsp;on-line kalendář Cleoptara Vám poskytuje Ludmila Hobzová DiS.,
        IČO: 06813623, zapsaná v&nbsp;Živnostenském rejstříku (dále jen
        „<b>Poskytovatelka</b>“).</p>
        <p class="text-justify">
        1.2 Tyto všeobecné obchodní podmínky (dále jen &quot;<b>Uživatelské
        podmínky</b>&quot;) se použijí na smlouvy o&nbsp;poskytování Služby
        mezi Poskytovatelkou a&nbsp;fyzickou osobou, zpravidla ženou, proto dále
        jen &quot;Uživatelkou&quot;.
        </p>
        <p class="text-justify">
        1.3 Tyto Uživatelské podmínky a&nbsp;Zásady ochrany osobních údajů,
        které tvoří nedílnou součást Uživatelských podmínek (dále
        společně jen „<b>Obchodní podmínky</b>“), jsou použitelné a
        závazné pro Poskytovatelku i pro Uživatelku. Smlouvy dále
        podléhají Zvláštním obchodním podmínkám (včetně jakýchkoli
        speciálních nabídek, cen, platebních podmínek a&nbsp;způsobů, doby
        trvání smlouvy atd.), které Poskytovatelka průběžně nabízí
        ve spojení s&nbsp;uzavřením smlouvy mezi Poskytovatelkou a&nbsp;Uživatelkou.
        V&nbsp;případě rozporu mezi Zvláštními obchodními podmínkami a
        Obchodními podmínkami jsou rozhodující Zvláštní obchodní
        podmínky. Obchodní podmínky společně se Zvláštními obchodními
        podmínkami představují smlouvu mezi Uživatelkou a&nbsp;Poskytovatelkou
        (dále jen „Smlouva“). Aby Uživatelka získala přístup ke
        Službě, musí potvrdit, že si Smlouvu přečetla a&nbsp;přijímá ji.</p>
        <p class="text-justify">
        1.4 Po objednání Služby, Poskytovatelka zašle potvrzení Smlouvy
        e-mailem odeslaným na e-mailovou adresu Uživatelky poskytnutou
        Uživatelkou při registraci a&nbsp;přihlášení ke svému Google účtu
        ( podle článku 4 níže). Toto potvrzení bude obsahovat text
        Smlouvy. Smlouva je uzavřena v&nbsp;okamžiku, kdy ji Uživatelka přijme
        kliknutím na označené tlačítko na Internetových stránkách za
        účelem dokončení objednávky.</p>
        <p class="text-justify">
        1.5 Smlouva může být uzavřena pouze v&nbsp;českém jazyce. Smlouva
        (která představuje smlouvu uzavřenou elektronickými prostředky)
        bude uložena u Poskytovatelky a&nbsp;Uživatelka obdrží její znění v
        potvrzovacím e-mailu doručeném na e-mailovou adresu poskytnutou
        Uživatelkou. Uživatelské podmínky a&nbsp;Zásady ochrany osobních
        údajů v&nbsp;platném znění jsou rovněž k&nbsp;dispozici na Internetových
        stránkách.</p>

        <h2 class="mt-2 mb-1">2 Obsah Služby</h2>
        <p class="text-justify">
        2.1 Službou je myšlen osobní on-line kalendář, sestavený na
        základě zadaných termínů menstruace. Tento kalendář je
        rozšířením aplikace Google kalendář a&nbsp;Uživatelce se zobrazuje
        v&nbsp;rámci jejích Google kalendářů pod názvem “<b>Cleopatra</b>”.
        V&nbsp;kalendáři jsou vidět barevně rozlišené a&nbsp;nadepsané
        jednotlivé fáze jejího měsíčního cyklu na 6 měsíců do
        budoucna. Výpočet probíhá automaticky na základě zadaných
        parametrů Uživatelky.</p>
        <p class="text-justify">
        2.2 Za roční předplatné se Uživatelka může přihlásit
        prostřednictvím vlastního účtu u společnosti Google a&nbsp;získat
        tak přístup ke Službě.</p>
        <p class="text-justify">
        2.3 Poskytovatelka může dle svého uvážení po omezenou dobu a&nbsp;za
        podmínek stanovených v&nbsp;souvislosti s&nbsp;nabídkou a&nbsp;Smlouvou nabídnout
        Službu bez povinnosti uhradit předplatné (dále jen „<b>Zkušební
        doba</b>“). Uživatelka může Službu ukončit kdykoli během
        Zkušební doby, aniž by musel za Službu platit. Poskytovatelka
        neúčtuje žádné poplatky během Zkušební doby.</p>
        <p class="text-justify">
        2.4 Uživatelka má nárok na jednu Zkušební dobu během jakékoliv
        souvislého dvanácti (12) měsíčního období. Nárok na Zkušební
        dobu může být ze strany Poskytovatelky ověřen na základě
        emailové adresy poskytnuté Uživatelkou za účelem předcházení
        zneužívání Zkušební doby. V&nbsp;případě, že Poskytovatelka má
        podezření na zneužití Zkušební doby,  může okamžitě učinit
        opatření k&nbsp;zamezení takového zneužití, včetně ukončení
        Smlouvy.</p>
        <p class="text-justify">
        2.5 Pokud Uživatelka neukončila Smlouvu během Zkušební doby, je
        povinna uhradit roční předplatné, aby jí Služba byla plně
        zpřístupněna.</p>

        <h2 class="mt-2 mb-1">3 Registrace, osobní údaje a&nbsp;aktualizace</h2>
        <p class="text-justify">
        3.1 Registrace musí být provedena na stránce
        <a href="<?php echo HOME; ?>"><span color="#1155cc"><u><?php echo HOME; ?></u></span></a>
        přihlášením k&nbsp;vlastnímu Google účtu Uživateky a&nbsp;pod jeho
        zabezpečením. Aby Uživatelka mohla Službu užívat, musí
        souhlasit potvrzením s&nbsp;Obchodními podmínkami a&nbsp;s&nbsp;přístupem k
        vlastnímu Google kalendáři pro vytvoření a&nbsp;poskytnutí nového
        osobního kalendáře Cleopatra.</p>
        <p class="text-justify">
        3.2 Dále je potřeba uvést nejméně <?php echo MIN_CYCLES_TO_CALCULATE; ?> termíny a&nbsp;délku menstruace
        pro výpočet osobního kalendáře.</p>
        <p class="text-justify">
        3.3 Zároveň je třeba mít přístup k&nbsp;zařízení, systémům a
        připojení v&nbsp;souladu s&nbsp;Uživatelskými podmínkami (viz článek 5
        níže) a&nbsp;po skončení platnosti případné Zkušební doby uhradit
        platbu v&nbsp;souladu s&nbsp;Uživatelskými podmínkami (viz článek 6 níže).
        </p>
        <p class="text-justify">
        3.4 Pro správné fungování Služby je nezbytné vracet se
        pravidelně (zpravidla měsíčně) a&nbsp;přidávat aktuální termíny
        menstruace pro správný výpočet dalšího období. Čím více je
        uvedeno termínů, tím přesnější je výpočet dalších fází.
        Kalendář je vždy aktualizován po zadání nového termínu. Pokud
        termíny nejsou pravidelně doplňovány, tak kalendář Cleopatra
        není aktuální.</p>
        <p class="text-justify">
        3.5 Uživatelka je povinna zajistit správnost informací a&nbsp;v&nbsp;případě
        potřeby aktualizovat svůj uživatelský účet. Při platbě smí
        Uživatelka poskytnout pouze informace o&nbsp;debetní nebo kreditní
        kartě, kterou vlastní. Zjevné chyby při zadávání dat budou
        zkontrolovány a&nbsp;Uživatelka na ně bude upozorněna. V&nbsp;každém
        případě Uživatelka zkontroluje a&nbsp;ověří úplnost a&nbsp;správnost
        svých poskytnutých údajů.</p>
        <p class="text-justify">
        3.3 Uživatelka uloží Platební údaje tak, aby je ochránila před
        neoprávněným přístupem. Uživatelka je odpovědná za veškeré
        užívání Služby prostřednictvím svého účtu.</p>
        <p class="text-justify">
        3.4 Má-li Uživatelka podezření, že její Přihlašovací a
        platební údaje byly neoprávněně užity, oznámí Uživatelka
        tuto skutečnost ihned Poskytovatelce a&nbsp;změní si heslo. Pokud se
        Poskytovatelka důvodně domnívá, že Přihlašovací údaje byly
        zveřejněny nebo jsou jiným způsobem zneužity neoprávněnými
        osobami, je oprávněna Smlouvu ukončit s&nbsp;okamžitou platností v
        souladu s&nbsp;článkem 11.4 písm. c).</p>

        <h2 class="mt-2 mb-1">4 Zásady ochrany osobních údajů</h2>
        <p class="text-justify">
        4.1 Vyjádřením souhlasu Uživatelka souhlasí s&nbsp;následným
        zpracováním svých osobních a&nbsp;citlivých údajů v&nbsp;souladu s
        Nařízením Evropského parlamentu a&nbsp;Rady 2016/679 (tzv. GDPR) a
        zákona č. 101/2000 Sb. o&nbsp;ochraně osobních údajů.</p>
        <p class="text-justify">
        4.2 Jakékoliv osobní a&nbsp;citlivé údaje jsou poskytovány Správci
        dat Uživatelkou dobrovolně. Správcem dat je Poskytovatelka.</p>
        <p class="text-justify">
        4.3 Poskytovanými osobními a&nbsp;citlivými údaji je e-mail Uživatelky
        a&nbsp;údaje o&nbsp;termínech a&nbsp;délce jejího menstruačního cyklu
        (případně těhotenství). Tyto údaje jsou použity pouze pro
        výpočet jednotlivých fází cyklu a&nbsp;výpočet individuálního
        kalendáře Cleopatra a&nbsp;to pouze pro potřebu Uživatelky.</p>
        <p class="text-justify">
        4.4 Přihlášením se k&nbsp;účtu na webových stránkách
        <a href="<?php echo HOME; ?>"><?php echo HOME; ?></a>
        Uživatelkaka umožňuje propojení s&nbsp;osobním účtem a&nbsp;kalendářem,
        který má u společnosti Google, aby jí mohl být nahrán či
        aktualizován kalendář Cleopatra.</p>
        <p class="text-justify">
        4. 5 Poskytovatelka se zavazuje, že žádné z poskytnutých údajů
        nebudou použity k&nbsp;jinému účelu než pro zajištění
        bezproblémového chodu webových stránek, kalendáře a
        informovanosti Uživatelky. Údaje také nebudou předávány ani
        poskytovány třetím osobám.</p>
        <h2 class="mt-2 mb-1">5 Zařízení atd.</h2>
        <p class="text-justify">
        5.1 Aby mohla Uživatelka Službu užívat, musí mít přístup k
        zařízením podporujícím aplikaci Google kalendář.</p>
        <p class="text-justify">
        5.2 Uživatelka je oprávněn používat Službu na neomezeném
        množství zařízení.</p>
        <p class="text-justify">
        5.3 Služba vyžaduje, aby měla Uživatelka potřebné internetové
        připojení. Uživatelka hradí veškeré náklady spojené s&nbsp;tímto
        připojením.</p>
        <p class="text-justify">
        5.4 Poskytovatelka neposkytuje vlastní zařízení a&nbsp;nenese žádnou
        odpovědnost za fungování jakékoliv schváleného zařízení nebo
        jeho trvalou kompatibilitu se Službou a&nbsp;jejími aktualizacemi.</p>
        <p class="text-justify">
        5.5 V&nbsp;rozsahu dovoleném platnými právními předpisy není
        Poskytovatelka odpovědná za neschopnost Uživatelky užívat Službu
        nebo za omezený přístup ke Službě z důvodu přetížení
        internetu nebo v&nbsp;důsledku chyby nebo problémů týkajících se
        počítačů a&nbsp;jiných zařízení, sítí, elektroniky nebo
        komunikací, které jsou mimo kontrolu Poskytovatelky.</p>

        <h2 class="mt-2 mb-1">6 Předplatné Služby a&nbsp;platební podmínky</h2>
        <p class="text-justify">
        6.1 Předplatné Služby bude placeno v&nbsp;souladu s&nbsp;platnými
        aktuálními cenami uvedenými na Internetových stránkách. Výše
        příslušného předplatného bude Uživatelce k&nbsp;dispozici na
        Internetových stránkách před uzavřením Smlouvy, přičemž v
        rámci registrace musí Uživatelka výslovně potvrdit, že
        objednávaná Služba je spojena s&nbsp;povinností zaplatit příslušnou
        cenu.</p>
        <p class="text-justify">
        6.2 Platba předplatného na bezprostředně následující rok bude
        hrazena Poskytovatlce předem.</p>
        <p class="text-justify">
        6.3 Uživatelka může platbu předplatného provádět kterýmkoli
        ze způsobů platby nabízených Poskytovatelkou na Internetových
        stránkách.
        </p>
        <p class="text-justify">
        6.4 Je-li Uživatelce poskytnuta Zkušební doba, nebude za Zkušební
        dobu vyžadována Poskytovatelkou žádná platba. První platba však
        může být provedena kdykoli a&nbsp;to i během Zkušební doby (za
        předpokladu, že Uživatelka neukončila Smlouvu před koncem
        Zkušební doby).</p>
        <p class="text-justify">
        6.5 Předplatné se Uživatelce počítá 1 rok od data zaplacení
        Služby. Počet  zbývajích dní, které má Uživatelka Službu k
        dispozici zjistí na internetových stránkám pod svým profilem.</p>
        <p class="text-justify">
        6.6 Pokud platba nebude uhrazena, je Poskytovatelka oprávněna
        okamžitě přerušit nebo omezit Uživatelce přístup ke Službě.
        Poskytovatelka je rovněž oprávněna ukončit Smlouvu s&nbsp;okamžitou
        účinností v&nbsp;případě prodlení s&nbsp;platbou po dobu delší než
        dvacet (20) dní.</p>

        <h2 class="mt-2 mb-1">7 Užívání Služby</h2>
        <p class="text-justify">
        7.1 Uživatelka smí Službu užívat pouze pro osobní potřebu a&nbsp;v
        souladu s&nbsp;těmito Uživatelkými podmínkami. Uživatelka nesmí
        učinit žádná z níže uvedených jednání, ani je nesmí
        podporovat, ani umožnit ani způsobit, aby je učinila třetí
        osoba:</p>
        <p class="text-justify">
        (a) užívat Službu pro komerční nebo veřejné účely,
        </p>
        <p class="text-justify">
        (b) užívat Službu pro nezákonné nebo nevhodné účely,</p>
        <p class="text-justify">
        (c) pořizovat kopie/ rozmnoženiny, půjčovat, prodávat, vysílat
        ani jinak šířit, pozměňovat nebo jakýmkoliv jiným způsobem
        převádět nebo upravovat obsah Služby,
        </p>
        <p class="text-justify">
        (d) obcházet, upravovat, odstraňovat, měnit nebo jakýmkoliv jiným
        způsobem manipulovat se zabezpečením, šifrováním nebo jinou
        technologií nebo softwarem, který je součástí Služby, a</p>
        <p class="text-justify">
        (e) jinak užívat Službu v&nbsp;rozporu se zákony o&nbsp;právu autorském,
        jinými zákony nebo těmito Uživatelskými podmínkami.</p>
        <p class="text-justify">
        7.2 Užívání Služby v&nbsp;rozporu s&nbsp;článkem 7 vždy představuje
        podstatné porušení Smlouvy a&nbsp;může vést k&nbsp;tomu, že
        Poskytovatelka ukončí poskytování Služby s&nbsp;okamžitou platností
        (viz článek 11 níže).</p>

        <h2 class="mt-2 mb-1">8 Změny, doplňky</h2>
        <p class="text-justify">
        8.1 Uživatelka tímto výslovně uznává, že Poskytovatelka je
        oprávněna v&nbsp;přiměřeném rozsahu změnit výši předplatného a
        další ustanovení Obchodních podmínek. Uživatelka bude na takové
        změny upozorněna e-mailem odeslaným na e-mailovou adresu
        poskytnutou Uživatelkou a&nbsp;to nejpozději třicet (30) dní před
        vstupem takové změny v&nbsp;platnost.
        </p>
        <p class="text-justify">
        8.2 Uživatelka je oprávněna v&nbsp;souvislosti s&nbsp;takovou změnou
        ukončit Smlouvu s&nbsp;účinností k&nbsp;datu účinnosti předmětné
        změny. Poskytovatelka bude Uživatelce ve svém oznámení
        informovat o&nbsp;jejím právu ukončit Smlouvu. Pokud Uživatelka
        Smlouvu neukončí před tím, než změna nabude účinnosti, má se
        za to, že tuto změnu přijala.</p>
        <p class="text-justify">
        8.3 V&nbsp;případě předčasného ukončení Služby, kde Uživatelka
        provedla platbu vztahující se k&nbsp;období po ukončení, vrátí
        Poskytovatelka přeplatek do třiceti (30) dnů od data ukončení
        Smlouvy.</p>

        <h2 class="mt-2 mb-1">9 Pozbytí práva na odstoupení</h2>
        <p class="text-justify">
        9.1 Uzavřením Smlouvy Uživatelka výslovně uděluje souhlas s
        poskytováním Služby ihned po uzavření Smlouvy a&nbsp;zaplacení
        předplatného. Uživatelka bere na vědomí, že tím, že získá
        přístup ke Službě před uplynutím čtrnáctidenní (14 denní)
        lhůty pro odstoupení od Smlouvy, pozbývá právo odstoupit od
        smlouvy v&nbsp;souladu s&nbsp;§ 1829 občanského zákoníku (zákon č.
        89/2012 Sb. v&nbsp;platném znění), ve lhůtě čtrnácti (14) dnů od
        uzavření Smlouvy. Pokud Uživatelka uzavřel smlouvu a&nbsp;bylo
        zahájeno poskytování Služby, ale Uživatelka již nechce Službu
        dál užívat a&nbsp;být Smlouvou vázána, musí Smlouvu ukončit v
        souladu s&nbsp;článkem 11 níže. Proces registrace zahrnuje, že
        Uživatelka výslovně potvrdí (zaškrtnutím políčka), že
        souhlasí s&nbsp;poskytováním přístupu ke Službě ihned po uzavření
        Smlouvy a&nbsp;zaplacením předplatného a&nbsp;že bere na vědomí, že
        získáním přístupu ke Službě před vypršením lhůty čtrnácti
        (14) dnů pro odstoupení od Smlouvy pozbývá právo od Smlouvy
        odstoupit.</p>
        <p class="text-justify">
        9.2 Ustanovení článku 10 se nepoužije, pokud je poskytnuta
        Zkušební doba delší než čtrnáct (14) dnů, protože Uživatelka
        může Smlouvu ukončit během Zkušební doby, i když je Služba
        poskytována ihned po uzavření Smlouvy.</p>

        <h2 class="mt-2 mb-1">10 Informace pro zákazníky</h2>
        <p class="text-justify">
        10.1 Poskytovatelka může Uživatelce zasílat oznámení
        prostřednictvím e-mailu nebo zveřejněním na Internetových
        stránkách v&nbsp;souladu s&nbsp;platnými zákony.</p>
        <p class="text-justify">
        10.2 Uživatelka je povinen aktualizovat své kontaktní údaje,
        pokud se během doby jejího užívání Služby změní. Má se za
        to, že Poskytovatelka splnila svou oznamovací povinnost odesláním
        oznámení na poslední uvedenou e-mailovou adresu Uživatelce, a&nbsp;to
        i v&nbsp;případě, že ji Uživatelka již neužívá nebo je z jiného
        důvodu nepoužitelná.</p>

        <h2 class="mt-2 mb-1">11 Doba trvání Smlouvy a&nbsp;její ukončení</h2>
        <p class="text-justify">
        11.1 Smlouva o&nbsp;předplatném Služby se uzavírá na dobu
        předplatného v&nbsp;délce jeden (1) rok a&nbsp;je automaticky prodlužována
        vždy o&nbsp;jeden (1) rok, dokud Uživatelka nebo Poskytovatelka Smlouvu
        neukončí v&nbsp;souladu s&nbsp;těmito Uživatelskými podmínkami. Doba
        předplatného v&nbsp;délce jednoho (1) roku začíná běžet prvním
        dnem po zaplacení Služby (pokud Uživatelka neukončí Smlouvu před
        uplynutím Zkušební doby).</p>
        <p class="text-justify">
        11.2 Pokud si Uživatelka přeje ukončit Smlouvu, musí tuto
        skutečnost oznámit Poskytovatelce e-mailem nebo, pokud
        Poskytovatelka tuto metodu ukončení umožní, přes Internetové
        stránky (<?php echo HOME; ?>). V&nbsp;případě, že Uživatelka
        iniciuje ukončení Smlouvy, zašle Poskytovatelka Uživatelce
        potvrzující e-mail. Pokud si Poskytovatelka přeje ukončit
        Smlouvu, oznámí to Uživatelce e-mailem. V&nbsp;souladu s&nbsp;článkem 11.1
        bude Smlouva ukončena ke konci běžného ročního období
        platnosti Smlouvy.</p>
        <p class="text-justify">
        11.3 V&nbsp;souvislosti se změnami výše předplatného nebo
        Uživatelských podmínek vzniká právo na ukončení Smlouvy v
        souladu s&nbsp;článkem 8.</p>
        <p class="text-justify">
        11.4 V&nbsp;souladu s&nbsp;výše uvedeným je Poskytovatelka oprávněna
        ukončit Smlouvu s&nbsp;okamžitou platností a&nbsp;současně okamžitě
        znepřístupnit Službu Uživatelce v&nbsp;jakémkoliv z následujících
        případů,:</p>
        <p class="text-justify">
        (a) Uživatelka je v&nbsp;prodlení s&nbsp;platbou po dobu nejméně dvaceti
        (20) dnů,</p>
        <p class="text-justify">
        (b) lze důvodně předpokládat, že je Uživatelka v&nbsp;úpadku,
        </p>
        <p class="text-justify">
        (c) Uživatelka zneužije Zkušební dobu nebo poruší její
        podmínky;</p>
        <p class="text-justify">
        (d) Služba je užívána neoprávněně nebo existuje důvodné
        podezření, že dochází k&nbsp;neoprávněnému užívání,
        </p>
        <p class="text-justify">
        (e) Uživatelka se pokusil využít Zkušební dobu více než
        jedenkrát (1x) během souvislého dvanácti (12) měsíčního
        období, nebo</p>
        <p class="text-justify">
        (f) Uživatelka jiným způsobem podstatně porušila Smlouvu nebo
        opakovaně porušila Smlouvu.</p>

        <h2 class="mt-2 mb-1">12 Práva duševního vlastnictví</h2>
        <p class="text-justify">
        12.1 V&nbsp;souvislosti s&nbsp;uzavřením Smlouvy s&nbsp;Poskatovatelkou uděluje
        Poskytovatelka Uživatelce nevýhradní, nepřenosnou a&nbsp;omezenou
        licenci k&nbsp;užívání Služby. Uživatelka smí Službu a&nbsp;její obsah
        užívat pouze v&nbsp;souladu se Smlouvou a&nbsp;pro soukromé účely.</p>
        <p class="text-justify">
        12.2 Materiály na Internetových stránkách a&nbsp;obsah Služby jsou
        chráněny autorským právem, zákonem o&nbsp;ochranných známkách a
        dalšími zákony o&nbsp;duševním vlastnictví. Smlouva s
        Poskytovatelkou v&nbsp;žádném případě nezakládá převod
        vlastnického práva ani jiného práva duševního vlastnictví k
        obsahu Služby nebo Služby na Uživatelku.</p>

        <h2 class="mt-2 mb-1">13 Zabezpečení</h2>
        <p class="text-justify">
        13.1 Uživatelka nesmí učinit žádné jednání, které by mohlo
        zapříčinit nefunkčnost, přetížení, poškození nebo omezení
        Služby.
        </p>
        <p class="text-justify">
        13.2 Uživatelka se nesmí pokusit získat neoprávněný přístup
        do sítí, počítačových systémů, k&nbsp;obsahu nebo datům
        souvisejícím s&nbsp;Internetovými stránkami nebo Službou. Porušení
        tohoto ustanovení vždy představuje podstatné porušení Smlouvy a
        může vést mimo jiné k&nbsp;tomu, že Poskytovatelka ukončí
        poskytování Služby s&nbsp;okamžitou platností (viz článek 12 níže).</p>

        <h2 class="mt-2 mb-1">14 Omezení odpovědnosti,
        narušení a&nbsp;výpadky</h2>
        <p class="text-justify">
        14.1 Poruchy, výpadky a&nbsp;další nedostatky Služby budou
        Poskytovatelkou oznámeny co nejdříve. V&nbsp;rozsahu dovoleném
        platnými právními předpisy nenese Poskytovatelka odpovědnost za
        vady Služby, které jsou mimo kontrolu Poskytovatelky nebo jiné
        závady způsobené tím, že Uživatelka používá zařízení,
        které není schváleno, nebo za závady, které jsou způsobeny
        nedbalostí Uživatelky.</p>
        <p class="text-justify">
        14.2 Za poruchy, výpadky a&nbsp;jiné poruchy, které mají za následek,
        že Službu nelze užít, a&nbsp;za které je prokazatelně odpovědná
        Poskytovatelka podle článku 15.1, jsou Uživatelky, které o&nbsp;to
        požádají, oprávněny získat spravedlivou náhradu, která bude
        určena v&nbsp;souladu s&nbsp;platnými zákony. Uživatelky nemají nárok na
        jakoukoliv náhradu za přerušení, výpadky nebo jiné poruchy
        Služby, které jsou důsledkem údržby Služby.
        </p>

        <h2 class="mt-2 mb-1">15 Povinnost nahradit škodu</h2>
        <p class="text-justify">
          Uživatelka se zavazuje odškodnit Poskytovatelku za jakékoli ztráty, výdaje
        nebo nároky vzniklé v&nbsp;důsledku nebo v&nbsp;souvislosti s&nbsp;porušením
        Smlouvy či porušením příslušných zákonů.
        </p>

        <h2 class="mt-2 mb-1">16 Odkazy na Internetových stránkách</h2>
        <p class="text-justify">Internetové
        stránky mohou obsahovat odkazy na jiné internetové stránky
        provozované třetími stranami. Poskytovatelka nemá nad takovými
        odkazy ani obsahem takových internetových stránek kontrolu a
        nepřejímá za ně žádnou odpovědnost. Uživatelka užívá
        takové internetové stránky na vlastní nebezpečí.</p>

        <h2 class="mt-2 mb-1">17 Neexistence konkludentního schválení</h2>
        <p class="text-justify">Pokud
        Poskytovatelka nebude vyžadovat splnění povinnosti vyplývající
        z této Smlouvy nebo neučiní žádné jednání proti porušení
        Smlouvy Uživatelkou, nelze takovou skutečnost považovat za
        schválení daného porušení nebo budoucích podobných porušení
        a&nbsp;nebudou tím ani jinak dotčena práva Poskytovatelky vyplývající
        z této Smlouvy.</p>

        <h2 class="mt-2 mb-1">18 Stížnosti a&nbsp;spory</h2>
        <p class="text-justify">18.1
        Pokud bude Uživatelka z jakéhokoliv důvodu nespokojena se Službou,
        může kontaktovat Poskytovatelku prostřednictvím kontaktních
        údajů uvedených v&nbsp;článku 19 nebo jiných kontaktních údajů
        dostupných na Internetových stránkách.</p>
        <p class="text-justify">18.2
        Uživatelka a&nbsp;Poskytovatelka se nejdříve pokusí vyřešit spory ze
        Smlouvy smírně. Nepodaří-li se Smluvním stranám dosáhnout
        smírného řešení sporu, bude spor vyřešen příslušným soudem
        v&nbsp;České republice. Není-li uvedeno jinak, tato Smlouva se řídí
        právním řádem České republiky. Dále vám mohou příslušet
        některá práva na ochranu spotřebitele a&nbsp;mohou se na Vás
        vztahovat další závazná ustanovení právních předpisů České
        republiky. Před podáním žaloby k&nbsp;příslušnému soudu má
        Uživatelka nárok předložit stížnost České obchodní inspekci,
        <a href="about:blank"><u>www.coi.cz</u></a> , jakožto orgánu
        oprávněnému řešit stížnosti spotřebitelů.
        </p>
        <h2 class="mt-2 mb-1">19 Kontaktní údaje a&nbsp;příslušný regulační orgán</h2>

        <p class="text-justify">Poskytovatelku
        je možné kontaktovat na adrese Ludmila Hobzová, Jeřická 1274/3,
        Praha 20, PSČ: 193 00, Česká republika nebo e-mailové adrese
        <a href="mailto:<?php echo EMAIL_INFO; ?>"><?php echo EMAIL_INFO; ?></a>.
        </p>

      </div>
    </div>
  </div>
</section>

<?php
  require_once('../../../core/tmp/footer.php');

  if (isset($_GET['after'])) {
    require_once('../../../core/cleo_dialog_modal.php');
  }

  getMainFooter(3,0);
?>
