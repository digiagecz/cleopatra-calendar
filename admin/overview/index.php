<?php
// OVERVIEW PAGE
// Author: Martin Krotil

require_once('../../core/settings.php');
require_once('../../core/cleo_statistics.php');

// first set general PHP error reporting
// in order to be sure we will see every error in the script
ini_set('display_errors',1);
error_reporting(E_ALL);


/*** THIS! ***/
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

//header('Content-Type: text/html; charset=utf-8');

$cStats = new cleoStatistics;
$userID = $cStats->getUserInformation('id');
$userEmail = $cStats->getUserInformation('email');

//delete partner and redirect
if(isset($_GET['delPartner'])){
  $cStats -> deactivatePartner($_GET['delPartner']);
}


////// pozor, upravit podle uživatelů na produkci !!!!!!!!!!!!!!!
if (($cStats->isUserLoggedIn() || (HOST == "localhost")) && ((($userID == 1) && ($userEmail =! 'test@cleopatra-calendar.com')) || $userEmail == 'cleopatra.test.eu@gmail.com' || ($userID == 3) || ($userID == 5) || ($userID == 102) || ($userID == 104) || ($userID == 106) || (HOST == "localhost"))) {
?>
  <!DOCTYPE html>
  <html lang="cs">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Statistiky Cleopatra</title>
    <?php echo $cStats->getTableStyles(); ?>
  </head>
  <body>

<?php
  echo "Jste přihlášen (user ID: " . $userID . ", user email: " . $userEmail . ") a máte na toto místo povolený přístup.<br /><br />";


  echo '<h1>Základní statistiky</h1>';

  $basicStatistics = $cStats->getBasicStatistics();

  echo 'Počet registrovaných uživatelů: ' . $basicStatistics['numberOfUsers'] . '<br />';
  echo 'Počet uživatelů s platnou licencí (včetně zkušební verze): ' . $basicStatistics['hasValidLicense'] . '<br />';
  echo 'Počet uživatelů, kteří mají poslední událost né starší, než 31 dnů: ' . $basicStatistics['30daysActive'] . '<br />';


  //--------------- výpis parnerů ----------------
  
  echo '<h1>Aktivní partneři a slevové kódy</h1>';

  if(isset($_POST['partner_name'])){
    $cStats->createNewPartner($_POST['partner_name'],$_POST['partner_email'],$_POST['partner_account']);
  }

?>


  <form action="" method="post">
    <strong>Přidat partnera:</strong><br>
    <input type="text" placeholder="jméno partnera" name="partner_name"> <input type="text" placeholder="email partnera" name="partner_email"> <input type="text" placeholder="číslo účtu" name="partner_account"><input type="submit" name="send" value="přidat">
  </form>
  <br>

<?php
  $allPartners = $cStats->getAllActivePartners();

  echo('<table>');
  echo('<tr><th>Partner ID</th><th>Jméno</th><th>Email</th><th>Registrace od</th><th>Č. účtu</th><th>Slevový kód</th><th>Smaž</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>Celkem</th></tr>');
  if($allPartners !== false){
    foreach ($allPartners as $detail) {
        $partnerId = htmlspecialchars($detail['id']);
        echo('<tr><td>' . $partnerId . '</td>');
        echo('<td>' . htmlspecialchars($detail['name']) . '</td>');
        echo('<td>' . htmlspecialchars($detail['email']) . '</td>');
        $d = date_format(new DateTime($detail['from_date']),'d.m.Y');
        echo('<td>'. htmlspecialchars($d). '</td>');
        echo('<td>' . htmlspecialchars($detail['account']) . '</td>');
        echo('<td>' . htmlspecialchars($detail['coupon']) . '</td>');
        echo('<td><a href="" onclick="deactivatePartner('.$partnerId.',\''.$detail['name'].'\');return false;"><img src="../../img/del.png" title="deaktivovat partnera '. $detail['name'] .'"></a></td>');
        for($i=1;$i<=12;$i++){
          echo('<td>' . htmlspecialchars($detail['m'.$i]) . '</td>');
        }
        echo('<td>' . htmlspecialchars($detail['total']) . '</td></tr>');
        
    }
  }
  echo('</table>');


/*   $allPartners = $cStats->getAllActivePartners();

  echo('<table>');
  echo('<tr><th>Partner ID</th><th>Jméno</th><th>Email</th><th>Č. účtu</th><th>Slevový kód</th><th>Registrace od</th></tr>');
  if($allPartners !== false){
    foreach ($allPartners as $detail) {
        $partnerId = htmlspecialchars($detail['id']);
        echo('<tr><td>' . $partnerId . '</td>');
        echo('<td>' . htmlspecialchars($detail['name']) . '</td>');
        echo('<td>' . htmlspecialchars($detail['email']) . '</td>');
        echo('<td>' . htmlspecialchars($detail['account']) . '</td>');
        echo('<td>' . htmlspecialchars($detail['coupon']) . '</td>');

        $d = date_format(new DateTime($detail['from_date']),'d.m.Y');
        echo('<td>'. htmlspecialchars($d). '</td>');
        echo('<td><a href="" onclick="deactivatePartner('.$partnerId.',\''.$detail['name'].'\');return false;"><img src="../../img/del.png" title="deaktivovat partnera '. $detail['name'] .'"></a></td></tr>');
    }
  }
  echo('</table>'); */

  //--------------- statistiky použití kuponů ----------------

  

  $table = $cStats->getLastMonthPartnersCouponsTotals();

  //pro teď se odešle email po každém načtení statitik
  //$lastMonth = date("F", strtotime("first day of previous month"));
  //$cStats->sendEmail(EMAIL_ADMIN.', '.EMAIL_INFO,'Statistiky Cleopatra kuponů za '.$lastMonth, $cStats->formatEmailTable($table));     //.', '.EMAIL_INFO

  echo $table;




  //--------------- výpis uživatelů a jejich plateb ----------------

  $paymentDetails = $cStats->getPaymentStatistics((isset($_GET['orderby']))?$_GET['orderby']:'userID');

  echo '<h1>Všichni uživatelé</h1>';

  echo('<table>');
  echo('<tr><th><a href="?orderby=userID">ID uživatele</a></th><th><a href="?orderby=userName">Jméno</a></th><th><a href="?orderby=userEmail">Email</a></th><th><a href="?orderby=userLastEventDate">Poslední událost</a></th><th><a href="?orderby=paymentId">Payment ID</a></th><th><a href="?orderby=paymentTime">Datum zaplacení</a></th><th><a href="?orderby=userLicenseExpirateDate">Datum expirace</a></th><th><a href="?orderby=paymentCoupon">Použitý kupón</a></th></tr>');
  foreach ($paymentDetails as $detail) {
      echo('<tr><td>' . htmlspecialchars($detail['userID']) . '</td>');
      echo('<td>' . htmlspecialchars($detail['userName']) . '</td>');
      echo('<td>' . htmlspecialchars($detail['userEmail']) . '</td>');
      if($detail['userLastEventDate'] != '---') $d = date_format(new DateTime($detail['userLastEventDate']),'d.m.Y'); else $d = $detail['userLastEventDate'];
      echo('<td>' . htmlspecialchars($d) . '</td>');

      echo('<td>' . htmlspecialchars($detail['paymentId']) . '</td>');
      if($detail['paymentTime'] != '---') $d = date_format(new DateTime($detail['paymentTime']),'d.m.Y ... H:i'); else $d = $detail['paymentTime'];
      echo('<td>' . htmlspecialchars($d) . '</td>');
      if($detail['userLicenseExpirateDate'] != '---') $d = date_format(new DateTime($detail['userLicenseExpirateDate']),'d.m.Y'); else $d = $detail['userLicenseExpirateDate'];
      echo('<td>' . htmlspecialchars($d) . '</td>');
      echo('<td>' . htmlspecialchars($detail['paymentCoupon']) . '</td></tr>');
  }
  echo('</table>');

  //--------------- dlouhodobě aktivní uživatelé ----------------

  echo '<h1>Dlouhodobě aktivní uživatelé</h1>';

  $usersDetails = $cStats->getUsersActivity();

  echo '<p>V tabulce jsou uvedení uživatelé, kteří alespoň <strong>ve třech různých dnech</strong> aktualizovali své údaje. Hodnota <em>počet</em> udává počet aktualizací údajů v Cleopatře (v různých dnech).</p>';

  echo('<table border="1">');
  echo('<tr><th>ID</th><th>Email</th><th>Počet</th></tr>');
  foreach ($usersDetails as $detail) {
    if ($detail['num'] > 2) {
      echo('<tr><td>' . htmlspecialchars($detail['id']) . '</td>');
      echo('<td>' . htmlspecialchars($detail['email']) . '</td>');
      echo('<td>' . htmlspecialchars($detail['num']) . '</td></tr>');
    }
  }
  echo('</table>');



}
else {
  cleoRedirect(HOME,'err_login', 'Přístup zamítnut.');
}
?>

<script>
  function deactivatePartner(id,name){
    var pokracovat = confirm("Opravdu chcete deaktivovat partnera "+ name +" (id: "+ id +") ?");	//true or false
    if(pokracovat){ 
      window.location.href = "?delPartner="+id;
    }
  }

</script>
</body>
</html>